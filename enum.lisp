;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl)

(register-constants ((+continue+ -2)
                     (+failure+  -1)
                     (+success+  0)
                     (+edom+     1)
                     (+erange+   2)
                     (+efault+   3)
                     (+einval+   4)
                     (+efailed+  5)
                     (+efactor+  6)
                     (+esanity+  7)
                     (+enomem+   8)
                     (+ebadfunc+ 9)
                     (+erunaway+ 10)
                     (+emaxiter+ 11)
                     (+ezerodiv+ 12)
                     (+ebadtol+  13)
                     (+etol+     14)
                     (+eundrflw+ 15)
                     (+eovrflw+  16)
                     (+eloss+    17)
                     (+eround+   18)
                     (+ebadlen+  19)
                     (+enotsqr+  20)
                     (+esing+    21)
                     (+ediverge+ 22)
                     (+eunsup+   23)
                     (+eunimpl+  24)
                     (+ecache+   25)
                     (+etable+   26)
                     (+enoprog+  27)
                     (+enoprogj+ 28)
                     (+etolf+    29)
                     (+etolx+    30)
                     (+etolg+    31)
                     (+eof+      32)))

(register-constants ((+prec-double+ 0)
                     (+prec-default+ 0)
                     (+prec-single+ 1)
                     (+prec-approx+ 2)))



