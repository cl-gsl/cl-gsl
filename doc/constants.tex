
\section{Physical Constants}

 This chapter describes macros for the values of physical constants,
such as the speed of light, c, and gravitational constant, G. The
values are available in different unit systems, including the standard
MKSA system (meters, kilograms, seconds, amperes) and the CGSM system
(centimeters, grams, seconds, gauss), which is commonly used in Astronomy.

 The definitions of constants in the MKSA system are available
in the file gsl\_const\_mksa.h. The constants in the CGSM system are
defined in gsl\_const\_cgsm.h. Dimensionless constants, such as the
fine structure constant, which are pure numbers are defined in gsl\_const\_num.h.

 The full list of constants is described briefly below. Consult
the header files themselves for the values of the constants used in
the library.

\subsection{Fundamental Constants}

\begin{description}

\item [\const{+mksa-speed-of-light+}] The speed of light in vacuum, c.

\item [\const{+mksa-vacuum-permeability+}] The permeability of free
space, \(\mu_0\). This constant is defined in the MKSA system
only.

\item [\const{+mksa-vacuum-permittivity+}] The permittivity of free
space, \(\epsilon_0\). This constant is defined in the MKSA
system only.

\item [\const{+mksa-plancks-constant-h+}] Planck's constant, h.

\item [\const{+mksa-plancks-constant-hbar+}] Planck's constant
divided by \(2\pi, \hbar\).

\item [\const{+num-avogadro+}] Avogadro's number, \(N_a\).

\item [\const{+mksa-faraday+}] The molar charge of 1 Faraday.

\item [\const{+mksa-boltzmann+}] The Boltzmann constant, k.

\item [\const{+mksa-molar-mass+}] The molar gas constant, \(R_0\).

\item [\const{+mksa-standard-gas-volume+}] The standard gas volume, \(V_0\).

\item [\const{+mksa-stefan-boltzmann-constant+}] The Stefan-Boltzmann
radiation constant, \(\sigma\).

\item [\const{+mksa-gauss+}] The magnetic field of 1 Gauss.

\item [\const{+mksa-micro+}] The length of 1 micron.

\item [\const{+mksa-hectare+}] The area of 1 hectare.

\item [\const{+mksa-miles-per-hour+}] The speed of 1 mile per hour.

\item [\const{+mksa-kilometers-per-hour+}] The speed of 1 kilometer per hour.

\end{description}

\subsection{Astronomy and Astrophysics}


\begin{description}

\item [\const{+mksa-astronomical-unit+}] The length of 1 astronomical
unit (mean earth-sun distance), au.

\item [\const{+mksa-gravitational-constant+}] The gravitational constant,
G.

\item [\const{+mksa-light-year+}] The distance of 1 light-year, ly.

\item [\const{+mksa-parsec+}] The distance of 1 parsec, pc.

\item [\const{+mksa-grav-accel+}] The standard gravitational acceleration
on Earth, g.

\item [\const{+mksa-solar-mass+}] The mass of the Sun.
\end{description}

\subsection{Atomic and Nuclear Physics}

\begin{description}
\item [\const{+mksa-electron-charge+}] The charge of the electron, e.

\item [\const{+mksa-electron-volt+}] The energy of 1 electron volt, eV.

\item [\const{+mksa-unified-atomic-mass+}] The unified atomic mass,
amu.

\item [\const{+mksa-mass-electron+}] The mass of the electron, \(m_e\).

\item [\const{+mksa-mass-muon+}] The mass of the muon, \(m_\mu\).

\item [\const{+mksa-mass-proton+}] The mass of the proton, \(m_p\).

\item [\const{+mksa-mass-neutron+}] The mass of the neutron, \(m_n\).

\item [\const{+num-fine-structure+}] The electromagnetic fine structure
constant \(\alpha\).

\item [\const{+mksa-rydberg+}] The Rydberg constant, Ry, in units of
energy. This is related to the Rydberg inverse wavelength R by Ry
= h c R.

\item [\const{+mksa-bohr-radius+}] The Bohr radius, \(a_0\).

\item [\const{+mksa-angstrom+}] The length of 1 angstrom.

\item [\const{+mksa-barn+}] The area of 1 barn.

\item [\const{+mksa-bohr-magneton+}] The Bohr Magneton, \(\mu_B\).

\item [\const{+mksa-nuclear-magneton+}] The Nuclear Magneton, \(\mu_N\).

\item [\const{+mksa-electron-magnetic-moment+}] The absolute value
of the magnetic moment of the electron, \(\mu_e\). The physical
magnetic moment of the electron is negative.

\item [\const{+mksa-proton-magnetic-moment+}] The magnetic moment of
the proton, \(\mu_p\).

\item [\const{+mksa-thomson-cross-section+}] The Thomson cross section,
\(\sigma_T\).

\end{description}

\subsection{Measurement of Time}

\begin{description}

\item [\const{+mksa-minute+}] The number of seconds in 1 minute.

\item [\const{+mksa-hour+}] The number of seconds in 1 hour.

\item [\const{+mksa-day+}] The number of seconds in 1 day.

\item [\const{+mksa-week+}] The number of seconds in 1 week.

\end{description}

\subsection{Imperial Units}

\begin{description}

\item [\const{+mksa-inch+}] The length of 1 inch.

\item [\const{+mksa-foot+}] The length of 1 foot.

\item [\const{+mksa-yard+}] The length of 1 yard.

\item [\const{+mksa-mile+}] The length of 1 mile.

\item [\const{+mksa-mil+}] The length of 1 mil (1/1000th of an inch).

\end{description}

\subsection{Nautical Units}

\begin{description}

\item [\const{+mksa-nautical-mile+}] The length of 1 nautical mile.

\item [\const{+mksa-fathom+}] The length of 1 fathom.

\item [\const{+mksa-knot+}] The speed of 1 knot.

\end{description}

\subsection{Printers Units}

\begin{description}

\item [\const{+mksa-point+}] The length of 1 printer's point (1/72 inch).

\item [\const{+mksa-texpoint+}] The length of 1 \TeX{} point (1/72.27 inch).

\end{description}

\subsection{Volume}

\begin{description}

\item [\const{+mksa-acre+}] The area of 1 acre.

\item [\const{+mksa-liter+}] The volume of 1 liter.

\item [\const{+mksa-us-gallon+}] The volume of 1 US gallon.

\item [\const{+mksa-canadian-gallon+}] The volume of 1 Canadian gallon.

\item [\const{+mksa-uk-gallon+}] The volume of 1 UK gallon.

\item [\const{+mksa-quart+}] The volume of 1 quart.

\item [\const{+mksa-pint+}] The volume of 1 pint.

\end{description}

\subsection{Mass and Weight}

\begin{description}

\item [\const{+mksa-pound-mass+}] The mass of 1 pound.

\item [\const{+mksa-ounce-mass+}] The mass of 1 ounce.

\item [\const{+mksa-ton+}] The mass of 1 ton.

\item [\const{+mksa-metric-ton+}] The mass of 1 metric ton (1000 kg).

\item [\const{+mksa-uk-ton+}] The mass of 1 UK ton.

\item [\const{+mksa-troy-ounce+}] The mass of 1 troy ounce.

\item [\const{+mksa-carat+}] The mass of 1 carat.

\item [\const{+mksa-gram-force+}] The force of 1 gram weight.

\item [\const{+mksa-pound-force+}] The force of 1 pound weight.

\item [\const{+mksa-kilopound-force+}] The force of 1 kilopound weight.

\item [\const{+mksa-poundal+}] The force of 1 poundal.

\end{description}

\subsection{Thermal Energy and Power}

\begin{description}

\item [\const{+mksa-calorie+}] The energy of 1 calorie.

\item [\const{+mksa-btu+}] The energy of 1 British Thermal Unit, btu.

\item [\const{+mksa-therm+}] The energy of 1 Therm.

\item [\const{+mksa-horsepower+}] The power of 1 horsepower.

\end{description}

\subsection{Pressure}

\begin{description}

\item [\const{+mksa-bar+}] The pressure of 1 bar.

\item [\const{+mksa-std-atmosphere+}] The pressure of 1 standard atmosphere.

\item [\const{+mksa-torr+}] The pressure of 1 torr.

\item [\const{+mksa-meter-of-mercury+}] The pressure of 1 meter of mercury.

\item [\const{+mksa-inch-of-mercury+}] The pressure of 1 inch of mercury.

\item [\const{+mksa-inch-of-water+}] The pressure of 1 inch of water.

\item [\const{+mksa-psi+}] The pressure of 1 pound per square inch.

\end{description}

\subsection{Viscosity}

\begin{description}

\item [\const{+mksa-poise+}] The dynamic viscosity of 1 poise.

\item [\const{+mksa-stokes+}] The kinematic viscosity of 1 stokes.

\end{description}

\subsection{Light and Illumination}

\begin{description}

\item [\const{+mksa-stilb+}] The luminance of 1 stilb.

\item [\const{+mksa-lumen+}] The luminous flux of 1 lumen.

\item [\const{+mksa-lux+}] The illuminance of 1 lux.

\item [\const{+mksa-phot+}] The illuminance of 1 phot.

\item [\const{+mksa-footcandle+}] The illuminance of 1 footcandle.

\item [\const{+mksa-lambert+}] The luminance of 1 lambert.

\item [\const{+mksa-footlambert+}] The luminance of 1 footlambert.

\end{description}

\subsection{Radioactivity}

\begin{description}

\item [\const{+mksa-curie+}] The activity of 1 curie.

\item [\const{+mksa-roentgen+}]The exposure of 1 roentgen.

\item [\const{+mksa-rad+}] The absorbed dose of 1 rad.

\end{description}

\subsection{Force and Energy}

\begin{description}

\item [\const{+mksa-newton+}] The SI unit of force, 1 Newton.

\item [\const{+mksa-dyne+}] The force of 1 Dyne = \(10^{-5}\) Newton.

\item [\const{+mksa-joule+}] The SI unit of energy, 1 Joule.

\item [\const{+mksa-erg+}] The energy 1 erg = \(10^{-7}\) Joule.

\end{description}

\subsection{Prefixes}

These constants are dimensionless scaling factors.

\begin{description}

\item [\const{+num-yotta+}] \(10^{24}\)

\item [\const{+num-zetta+}] \(10^{21}\)

\item [\const{+num-exa+}] \(10^{18}\)

\item [\const{+num-peta+}] \(10^{15}\)

\item [\const{+num-tera+}] \(10^{12}\)

\item [\const{+num-giga+}] \(10^{9}\)

\item [\const{+num-mega+}] \(10^{6}\)

\item [\const{+num-kilo+}] \(10^{3}\)

\item [\const{+num-milli+}] \(10^{-3}\)

\item [\const{+num-micro+}] \(10^{-6}\)

\item [\const{+num-nano+}] \(10^{-9}\)

\item [\const{+num-pico+}] \(10^{-12}\)

\item [\const{+num-femto+}] \(10^{-15}\)

\item [\const{+num-atto+}] \(10^{-18}\)

\item [\const{+num-zepto+}] \(10^{-21}\)

\item [\const{+num-yocto+}] \(10^{-24}\)

\end{description}



\subsection{Examples}

 The following program demonstrates the use of the physical constants
in a calculation. In this case, the goal is to calculate the range
of light-travel times from Earth to Mars.

 The required data is the average distance of each planet from
the Sun in astronomical units (the eccentricities of the orbits will
be neglected for the purposes of this calculation). The average radius
of the orbit of Mars is 1.52 astronomical units, and for the orbit
of Earth it is 1 astronomical unit (by definition). These values are
combined with the MKSA values of the constants for the speed of light
and the length of an astronomical unit to produce a result for the
shortest and longest light-travel times in seconds. The figures are
converted into minutes before being displayed.

\#include <stdio.h> \#include <gsl/+mksa.h>

int main (void) \{ double c = GSL\_CONST\_MKSA\_SPEED\_OF\_LIGHT;
double au = GSL\_CONST\_MKSA\_ASTRONOMICAL\_UNIT; double minutes =
GSL\_CONST\_MKSA\_MINUTE;

/{*} distance stored in meters {*}/ double r\_earth = 1.00 {*} au;
double r\_mars = 1.52 {*} au;

double t\_min, t\_max;

t\_min = (r\_mars - r\_earth) / c; t\_max = (r\_mars + r\_earth) /
c;

printf (\verb|"|light travel time from Earth to Mars:$\backslash$n\verb|"|);
printf (\verb|"|minimum = \%.1f minutes$\backslash$n\verb|"|, t\_min
/ minutes); printf (\verb|"|maximum = \%.1f minutes$\backslash$n\verb|"|,
t\_max / minutes);

return 0; \}

 Here is the output from the program,

light travel time from Earth to Mars: minimum = 4.3 minutes maximum
= 21.0 minutes




\subsection{References and Further Reading}

 Further information on the values of physical constants is available
from the NIST website,

\begin{itemize}
\item http://www.physics.nist.gov/cuu/Constants/index.html
\end{itemize}
