
\section{Polynomials}

This chapter describes functions for evaluating and solving polynomials.
There are routines for finding real and complex roots of quadratic
and cubic equations using analytic methods. An iterative polynomial
solver is also available for finding the roots of general polynomials
with real coefficients (of any order).


\subsection{Polynomial Evaluation}

\func{poly-eval}{ coeff-list x => result}

This function evaluates the polynomial
\(c_0 + c_1 x + c_2 x^2 + \dots + c_{len-1} x^{len-1}\)
using Horner's method for stability. \texttt{coeff-list} is a list
of the coefficients.


\subsection{Divided Difference Representation of Polynomials}

The functions described here manipulate polynomials stored in Newton's
divided-difference representation. The use of divided-differences
is described in Abramowitz \& Stegun sections 25.1.4 and 25.2.26.


\func{dd-init}{ x-list y-list => dd-list, status}

This function computes a divided-difference representation
of the interpolating polynomial for the points (\texttt{x}, \texttt{y})
stored in the lists \texttt{x-list} and \texttt{y-list}.
The output is the divided-differences of (\texttt{x},\texttt{y})
stored in the list \texttt{dd-list}.

\func{dd-eval}{ dd-list x-list x => result}

This function evaluates the polynomial stored in divided-difference
form in the lists \texttt{dd-list} and \texttt{x-list}
at the point \texttt{x}.

\func{dd-eval}{ xp dd-list x-list => c-list, status}

This function converts the divided-difference representation
of a polynomial to a Taylor expansion. The divided-difference representation
is supplied in the lists \texttt{dd-list} and \texttt{x-list}.
On output the Taylor coefficients of the polynomial expanded about
the point \texttt{xp} are returned in the array \texttt{c-list}.




\subsection{Quadratic Equations}


\func{solve-quadratic}{ a b c => x0, x1, num-roots}

This function finds the real roots of the quadratic equation,

$$a x^2 + b x + c = 0$$

The real roots \texttt{x0}, \texttt{x1}  together
with the number of roots \texttt{num-roots} (either zero or two) are
returned as values. If no real
roots are found then \texttt{x0} and \texttt{x1} are not modified.
When two real roots are found they are stored in ascending order.
The case of coincident roots is not considered
special. For example \((x-1)^2=0\) will have two roots, which happen
to have exactly equal values.

The number of roots found depends on the sign of the discriminant
\(b^2 - 4 a c\). This will be subject to rounding and cancellation
errors when computed in double precision, and will also be subject
to errors if the coefficients of the polynomial are inexact. These
errors may cause a discrete change in the number of roots. However,
for polynomials with small integer coefficients the discriminant can
always be computed exactly.

\func{complex-solve-quadratic}{ a b c => z0, z1, num-roots}

This function finds the complex roots of the quadratic equation,

$$a z^2 + b z + c = 0$$

The complex roots \texttt{z0}, \texttt{z1}, together
with the number of roots (always two) are returned as values.
The roots are returned in ascending order, sorted first by their real
components and then by their imaginary components.


\subsection{Cubic Equations}


\func{solve-cubic}{ a b c => x0, x1, x2, num-roots}

This function finds the real roots of the cubic equation,

$$x^3 + a x^2 + b x + c = 0$$

with a leading coefficient of unity. The real roots
\texttt{x0}, \texttt{x1} and \texttt{x2}, together with the number
of roots (either one or three) are returned as values.

If one real root is found
then only \textit{x0} is modified. When three real roots are found
they are given in \texttt{x0}, \texttt{x1} and \texttt{x2} in ascending
order. The case of coincident roots is not considered special. For
example, the equation \((x-1)^3=0\) will have three roots with exactly
equal values.

\func{complex-solve-quadratic}{ a b c => z0, z1, z3, num-roots}

This function finds the complex roots of the cubic equation,

$$z^3 + a z^2 + b z + c = 0$$

The complex roots \texttt{z0}, \texttt{z1} and
\texttt{z2}, together with the number of roots (always three) are
returned as values.
The roots are returned in ascending order, sorted first
by their real components and then by their imaginary components.


\subsection{General Polynomial Equations}


The roots of polynomial equations cannot be found analytically beyond
the special cases of the quadratic, cubic and quartic equation. The
algorithm described in this section uses an iterative method to find
the approximate locations of roots of higher order polynomials.

\func{complex-solve}{ a-list => root-list, status}

This function computes the roots of the general polynomial

$$P(x) = a_0 + a_1 x + a_2 x^2 + ... + a_{n-1} x^{n-1}$$

using balanced-QR reduction of the companion matrix. The coefficients
are supplie as a list \texttt{a-list}. The coefficient
of the highest order term must be non-zero. The function
returns two values, a list of the root \texttt{root-list}, and
the status. The value of \texttt{status} is \texttt{gsl:+success+}
if all the roots are found and \texttt{gsl:+efailed+} if the QR
reduction does not converge.


\subsection{Examples}



\subsection{References and Further Reading}

The balanced-QR method and its error analysis are described
in the following papers,

\begin{itemize}
\item R.S. Martin, G. Peters and J.H. Wilkinson, {}``The QR Algorithm for
Real Hessenberg Matrices'', \textsc{Numerische Mathematik}, 14 (1970),
219--231.
\item B.N. Parlett and C. Reinsch, {}``Balancing a Matrix for Calculation
of Eigenvalues and Eigenvectors'', \textsc{Numerische Mathematik},
13 (1969), 293--304.
\item A. Edelman and H. Murakami, {}``Polynomial roots from companion matrix
eigenvalues'', \textsc{Mathematics of Computation}, Vol. 64 No. 210
(1995), 763--776.
\end{itemize}
 The formulas for divided difference are given in Abramowitz
and Stegun,

\begin{itemize}
\item Abramowitz and Stegun, \textsc{Handbook of Mathematical Functions},
Sections 25.1.4 and 25.2.26.
\end{itemize}
