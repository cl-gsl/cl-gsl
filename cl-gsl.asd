;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-user)

(defpackage #:cl-gsl-system (:use #:cl #:asdf))

(in-package :cl-gsl-system)

(defsystem cl-gsl
    :name "cl-gsl"
    :version "0.2"
    :maintainer "Edgar Denny <edgardenny@comcast.net>"
    :licence "GPL"
    :description "GSL interface"
    :depends-on (:uffi :kmrcl :cl-ppcre)
    :components
    ((:file "package")
     (:file "load-libraries" :depends-on ("package"))
     (:file "ffi" :depends-on ("load-libraries" "package"))
     (:file "util" :depends-on ("package"))
     (:file "enum" :depends-on ("util"))
     (:file "const" :depends-on ("util"))
     (:file "math" :depends-on ("util"))
     (:file "poly" :depends-on ("util" "ffi"))
     (:file "sf" :depends-on ("util" "ffi"))
     (:file "vector" :depends-on ("util" "ffi"))
     (:file "matrix" :depends-on ("util" "ffi" "vector"))
     (:file "permutation" :depends-on ("util" "ffi" "vector"))
     (:file "sort" :depends-on ("util" "ffi" "permutation"))
     (:file "random-number-generator" :depends-on ("util" "ffi"))
     ))
