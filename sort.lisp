;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl-array)

(defmacro def-vector-sort-type-funcs% (typ)
  (destructuring-bind (type-ptr type-string)
      (cond
        ((eq typ 'double-float)
         (list 'gsl-vector-ptr ""))
        ((eq typ 'single-float)
         (list 'gsl-vector-float-ptr "_float"))
        ((eq typ 'integer)
         (list 'gsl-vector-int-ptr "_int"))
        (t
         (error "no matching type.")))

    `(progn
       (defun-foreign ,(concatenate 'string "gsl_sort_vector" type-string)
           ((v ,type-ptr))
         :void)

       (defun-foreign ,(concatenate 'string
                                    "gsl_sort_vector" type-string "_index")
           ((p gsl-permutation-ptr)
            (v ,type-ptr))
         :int))))

(def-vector-sort-type-funcs% double-float)
(def-vector-sort-type-funcs% single-float)
(def-vector-sort-type-funcs% integer)

(defmacro def-vector-sort-methods% (class-string func-string)
  (let ((class-object (kmrcl:concat-symbol "gsl-vector-" class-string)))
    `(progn
       (defmethod sort-vector ((o ,class-object))
         (,(kmrcl:concat-symbol "gsl-sort-vector" func-string) (ptr o))
         o)

       (defmethod sort-vector-index ((p gsl-permutation) (o ,class-object))
         (assert (= (size o) (size p)))
         (,(kmrcl:concat-symbol "gsl-sort-vector" func-string "-index")
           (ptr p) (ptr o))
         p))))

(def-vector-sort-methods% "integer" "-int")
(def-vector-sort-methods% "single-float" "-float")
(def-vector-sort-methods% "double-float" "")

(defmacro with-sort-vector-index ((p v) &body body)
  `(let ((,p (make-permutation (size ,v))))
     (unwind-protect
          (progn
            (sort-vector-index ,p ,v)
            ,@body)
       (free ,p))))
