;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl-array)

(defclass gsl-matrix ()
  ((ptr :accessor ptr :initarg :ptr)
   (size-rows :accessor size-rows :initarg :size-rows)
   (size-cols :accessor size-cols :initarg :size-cols)
   (element-type :accessor element-type :initarg :element-type)))


(defclass gsl-matrix-double-float (gsl-matrix) ())
(defclass gsl-matrix-single-float (gsl-matrix) ())
(defclass gsl-matrix-integer (gsl-matrix) ())
(defclass gsl-matrix-complex-double-float (gsl-matrix) ())
(defclass gsl-matrix-complex-single-float (gsl-matrix) ())

(defmacro def-matrix-type-funcs% (typ)
  (let ((type-ptr)
        (type-val)
        (type-val-ptr)
        (type-vec-ptr)
        (type-string)
        (is-real (or (eq typ 'double-float)
                     (eq typ 'single-float)
                     (eq typ 'integer))))

    (cond
      ((eq typ 'double-float)
       (setq type-ptr 'gsl-matrix-ptr)
       (setq type-vec-ptr 'gsl-vector-ptr)
       (setq type-val :double)
       (setq type-val-ptr '(* :double))
       (setq type-string "matrix"))
      ((eq typ 'single-float)
       (setq type-ptr 'gsl-matrix-float-ptr)
       (setq type-vec-ptr 'gsl-vector-float-ptr)
       (setq type-val :float)
       (setq type-val-ptr '(* :float))
       (setq type-string "matrix_float"))
      ((eq typ 'integer)
       (setq type-ptr 'gsl-matrix-int-ptr)
       (setq type-vec-ptr 'gsl-vector-int-ptr)
       (setq type-val :int)
       (setq type-val-ptr '(* :int))
       (setq type-string "matrix_int"))
      ((equal typ '(complex (double-float)))
       (setq type-ptr 'gsl-matrix-complex-ptr)
       (setq type-vec-ptr 'gsl-vector-complex-ptr)
       (setq type-val 'gsl-complex)
       (setq type-val-ptr '(* gsl-complex))
       (setq type-string "matrix_complex"))
      ((equal typ '(complex (single-float)))
       (setq type-ptr 'gsl-matrix-complex-float-ptr)
       (setq type-vec-ptr 'gsl-vector-complex-float-ptr)
       (setq type-val 'gsl-complex-float)
       (setq type-val-ptr '(* gsl-complex-float))
       (setq type-string "matrix_complex_float"))
      (t
       (error "no matching type.")))

    `(progn
       (defun-foreign ,(concatenate 'string "gsl_" type-string "_alloc")
           ((size-rows size-t)
            (size-cols size-t))
         ,type-ptr)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_free")
           ((m ,type-ptr))
         :void)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_memcpy")
           ((v1 ,type-ptr)
            (v2 ,type-ptr))
         :int)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_get")
           ((m ,type-ptr)
            (i size-t)
            (j size-t))
         ,type-val)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_set")
           ((m ,type-ptr)
            (i size-t)
            (j size-t)
            (x ,type-val))
         :void)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_set_all")
           ((m ,type-ptr)
            (x ,type-val))
         :void)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_set_zero")
           ((m ,type-ptr))
         :void)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_set_identity")
           ((m ,type-ptr))
         :void)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_isnull")
           ((m ,type-ptr))
         :int)

       (defun-foreign ,(concatenate 'string "wrap_gsl_" type-string "_fwrite")
           ((fn :cstring)
            (m ,type-ptr))
         :int)

       (defun-foreign ,(concatenate 'string "wrap_gsl_" type-string "_fread")
           ((fn :cstring)
            (m ,type-ptr))
         :int)

       (defun-foreign ,(concatenate 'string "wrap_gsl_" type-string "_fprintf")
           ((fn :cstring)
            (m ,type-ptr))
         :int)

       (defun-foreign ,(concatenate 'string "wrap_gsl_" type-string "_fscanf")
           ((fn :cstring)
            (m ,type-ptr))
         :int)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_swap")
           ((m1 ,type-ptr)
            (m2 ,type-ptr))
         :int)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_get_row")
           ((v ,type-vec-ptr)
            (m ,type-ptr)
            (row size-t))
         :int)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_get_col")
           ((v ,type-vec-ptr)
            (m ,type-ptr)
            (col size-t))
         :int)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_set_row")
           ((m ,type-ptr)
            (row size-t)
            (v ,type-vec-ptr))
         :int)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_set_col")
           ((m ,type-ptr)
            (col size-t)
            (v ,type-vec-ptr))
         :int)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_swap_rows")
           ((m ,type-ptr)
            (row1 size-t)
            (row2 size-t))
         :int)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_swap_columns")
           ((m ,type-ptr)
            (col1 size-t)
            (col2 size-t))
         :int)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_swap_rowcol")
           ((m ,type-ptr)
            (row size-t)
            (col size-t))
         :int)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_transpose")
           ((m ,type-ptr))
         :int)

       (defun-foreign ,(concatenate 'string
                                    "gsl_" type-string "_transpose_memcpy")
           ((m-dest ,type-ptr)
            (m-source ,type-ptr))
         :int)

       ,(when is-real
          `(progn
             (defun-foreign ,(concatenate 'string "gsl_" type-string "_add")
                 ((ma ,type-ptr)
                  (mb ,type-ptr))
               :int)

             (defun-foreign ,(concatenate 'string "gsl_" type-string "_sub")
                 ((ma ,type-ptr)
                  (mb ,type-ptr))
               :int)

             (defun-foreign ,(concatenate 'string
                                          "gsl_" type-string "_mul_elements")
                 ((ma ,type-ptr)
                  (mb ,type-ptr))
               :int)

             (defun-foreign ,(concatenate 'string
                                          "gsl_" type-string "_div_elements")
                 ((ma ,type-ptr)
                  (mb ,type-ptr))
               :int)

             (defun-foreign ,(concatenate 'string "gsl_" type-string "_scale")
                 ((m ,type-ptr)
                  ;; seems odd that this is :double for all types
                  (x :double))
               :int)

             (defun-foreign ,(concatenate 'string
                                          "gsl_" type-string "_add_constant")
                 ((m ,type-ptr)
                  ;; and again, :double for all types
                  (x :double))
               :int)

             (defun-foreign ,(concatenate 'string "gsl_" type-string "_max")
                 ((m ,type-ptr))
               ,type-val)

             (defun-foreign ,(concatenate 'string "gsl_" type-string "_min")
                 ((m ,type-ptr))
               ,type-val)


             (defun-foreign ,(concatenate 'string
                                          "gsl_" type-string "_max_index")
                 ((m ,type-ptr)
                  (i-ptr size-t-ptr)
                  (j-ptr size-t-ptr))
               :void)


             (defun-foreign ,(concatenate 'string
                                          "gsl_" type-string "_min_index")
                 ((m ,type-ptr)
                  (i-ptr size-t-ptr)
                  (j-ptr size-t-ptr))
               :void)
             ))

       ,(when (not is-real)
          `(progn
             (defun-foreign ,(concatenate 'string "gsl_" type-string "_ptr")
                 ((m ,type-ptr)
                  (i size-t)
                  (j size-t))
               (* ,type-val))

             (defun-foreign ,(concatenate 'string "wrap_gsl_" type-string "_set")
                 ((m ,type-ptr)
                  (i size-t)
                  (j size-t)
                  (z (* ,type-val)))
               :void)

             (defun-foreign ,(concatenate 'string
                                          "wrap_gsl_" type-string "_set_all")
                 ((m ,type-ptr)
                  (z (* ,type-val)))
               :void)
             ))
       )))

(def-matrix-type-funcs% double-float)
(def-matrix-type-funcs% single-float)
(def-matrix-type-funcs% integer)
(def-matrix-type-funcs% (complex (double-float)))
(def-matrix-type-funcs% (complex (single-float)))


(defmacro def-matrix-methods% (class-string func-string)
  (let ((class-object (kmrcl:concat-symbol "gsl-matrix-" class-string))
        (is-real (or (string= class-string "integer")
                     (string= class-string "single-float")
                     (string= class-string "double-float"))))
    `(progn

       (defmethod alloc ((o ,class-object))
         (setf (ptr o) (,(kmrcl:concat-symbol "gsl-matrix-" func-string "alloc")
                         (size-rows o) (size-cols o)))
         o)

       (defmethod free ((o ,class-object))
         (,(kmrcl:concat-symbol "gsl-matrix-" func-string "free") (ptr o))
         (setf (ptr o) nil)
         (setf (size-rows o) nil)
         (setf (size-cols o) nil)
         (setf (element-type o) nil))

       (defmethod get-element ((o ,class-object) i &optional j)
         (assert (and (typep i 'integer) (>= i 0) (< i (size-rows o))))
         (assert (and (typep j 'integer) (>= j 0) (< j (size-cols o))))
         ,(if is-real
              `(,(kmrcl:concat-symbol "gsl-matrix-" func-string "get")
                 (ptr o) i j)
              `(,(kmrcl:concat-symbol "gsl-" func-string ">complex")
                 (,(kmrcl:concat-symbol "gsl-matrix-" func-string "ptr")
                   (ptr o) i j))))

       (defmethod set-element ((o ,class-object) i &optional j x)
         (assert (typep x (element-type o)))
         (assert (and (typep i 'integer) (>= i 0) (< i (size-rows o))))
         (assert (and (typep j 'integer) (>= j 0) (< j (size-cols o))))
         ,(if is-real
              `(,(kmrcl:concat-symbol "gsl-matrix-" func-string "set")
                 (ptr o) i j x)
              `(,(kmrcl:concat-symbol "with-" class-string "->gsl-" func-string
                                      "ptr") (c-ptr x)
                 (,(kmrcl:concat-symbol "wrap-gsl-matrix-" func-string "set")
                   (ptr o) i j c-ptr)))
         x)

       (defmethod set-all ((o ,class-object) x)
         (assert (typep x (element-type o)))
         ,(if is-real
              `(,(kmrcl:concat-symbol "gsl-matrix-" func-string "set-all")
                 (ptr o) x)
              `(,(kmrcl:concat-symbol "with-" class-string "->gsl-" func-string
                                      "ptr") (c-ptr x)
                 (,(kmrcl:concat-symbol "wrap-gsl-matrix-" func-string "set-all")
                   (ptr o) c-ptr)))
         o)

       (defmethod set-zero ((o ,class-object))
         (,(kmrcl:concat-symbol "gsl-matrix-" func-string "set-zero") (ptr o))
         o)

       (defmethod set-identity ((o ,class-object))
         (,(kmrcl:concat-symbol "gsl-matrix-" func-string "set-identity")
           (ptr o))
         o)


       (defmethod read-from-binary-file ((o ,class-object) file-name)
         (let ((status))
           (uffi:with-cstring (c-file-name file-name)
             (setq status
                   (,(kmrcl:concat-symbol "wrap-gsl-matrix-" func-string
                                          "fread") c-file-name (ptr o))))
           (values o status)))

       (defmethod read-from-file ((o ,class-object) file-name)
         (let ((status))
           (uffi:with-cstring (c-file-name file-name)
             (setq status
                   (,(kmrcl:concat-symbol "wrap-gsl-matrix-" func-string
                                          "fscanf") c-file-name (ptr o))))
           (values o status)))

       (defmethod write-to-binary-file (file-name (o ,class-object))
         (let ((status))
           ;; TODO: check if uffi:with-string returns a result, docs unclear.
           (uffi:with-cstring (c-file-name file-name)
             (setq status
                   (,(kmrcl:concat-symbol "wrap-gsl-matrix-" func-string
                                          "fwrite") c-file-name (ptr o))))
           status))

       (defmethod write-to-file (file-name (o ,class-object))
         (let ((status))
           (uffi:with-cstring (c-file-name file-name)
             (setq status
                   (,(kmrcl:concat-symbol "wrap-gsl-matrix-" func-string
                                          "fprintf") c-file-name (ptr o))))
           status))

       (defmethod swap ((o1 ,class-object) (o2 ,class-object))
         (assert (and (= (size-rows o1) (size-rows o2))
                      (= (size-cols o1) (size-cols o2))))
         (let ((status (,(kmrcl:concat-symbol "gsl-matrix-" func-string
                                              "swap") (ptr o1) (ptr o2))))
           (values o1 status)))


       (defmethod isnull ((o ,class-object))
         (1/0->t/nil (,(kmrcl:concat-symbol "gsl-matrix-" func-string
                                            "isnull") (ptr o))))

       (defmethod get-row ((o ,class-object) row)
         (assert (and (typep row 'integer) (>= row 0) (< row (size-rows o))))
         (let* ((vec (make-vector (size-rows o) :element-type (element-type o)))
                (status (,(kmrcl:concat-symbol "gsl-matrix-" func-string
                                               "get-row")
                          (ptr vec) (ptr o) row)))
           (values vec status)))

       (defmethod get-col ((o ,class-object) col)
         (assert (and (typep col 'integer) (>= col 0) (< col (size-cols o))))
         (let* ((vec (make-vector (size-cols o) :element-type (element-type o)))
                (status (,(kmrcl:concat-symbol "gsl-matrix-" func-string
                                               "get-col")
                          (ptr vec) (ptr o) col)))
           (values vec status)))

       (defmethod set-row ((o ,class-object) row vec)
         (assert (and (typep row 'integer) (>= row 0) (< row (size-rows o))))
         (assert (= (size vec) (size-rows o)))
         (let* ((status (,(kmrcl:concat-symbol "gsl-matrix-" func-string
                                               "set-row")
                          (ptr o) row (ptr vec))))
           (values o status)))

       (defmethod set-col ((o ,class-object) col vec)
         (assert (and (typep col 'integer) (>= col 0) (< col (size-cols o))))
         (assert (= (size vec) (size-cols o)))
         (let* ((status (,(kmrcl:concat-symbol "gsl-matrix-" func-string
                                               "set-col")
                          (ptr o) col (ptr vec))))
           (values o status)))

       (defmethod swap-rows ((o ,class-object) row1 row2)
         (assert (and (typep row1 'integer) (>= row1 0) (< row1 (size-rows o))))
         (assert (and (typep row2 'integer) (>= row2 0) (< row2 (size-rows o))))
         (let* ((status (,(kmrcl:concat-symbol "gsl-matrix-" func-string
                                               "swap-rows")
                          (ptr o) row1 row2)))
           (values o status)))

       (defmethod swap-cols ((o ,class-object) col1 col2)
         (assert (and (typep col1 'integer) (>= col1 0) (< col1 (size-cols o))))
         (assert (and (typep col2 'integer) (>= col2 0) (< col2 (size-cols o))))
         (let* ((status (,(kmrcl:concat-symbol "gsl-matrix-" func-string
                                               "swap-columns")
                          (ptr o) col1 col2)))
           (values o status)))

       (defmethod swap-rowcol ((o ,class-object) row col)
         (assert (= (size-rows o) (size-cols o)))
         (assert (and (typep row 'integer) (>= row 0) (< row (size-rows o))))
         (assert (and (typep col 'integer) (>= col 0) (< col (size-cols o))))
         (let* ((status (,(kmrcl:concat-symbol "gsl-matrix-" func-string
                                               "swap-rowcol")
                          (ptr o) row col)))
           (values o status)))

       (defmethod transpose ((o ,class-object))
         (assert (= (size-rows o) (size-cols o)))
         (let* ((status (,(kmrcl:concat-symbol "gsl-matrix-" func-string
                                               "transpose")
                          (ptr o))))
           (values o status)))

       (defmethod copy-transpose ((o-dest ,class-object) (o-src, class-object))
         (assert (and (= (size-rows o-dest) (size-rows o-src))
                      (= (size-cols o-dest) (size-cols o-src))))
         (let* ((status (,(kmrcl:concat-symbol "gsl-matrix-" func-string
                                               "transpose-memcpy")
                          (ptr o-dest) (ptr o-src))))
           (values o-src status)))

       ,(when is-real
          `(progn
             (defmethod add ((o1 ,class-object) (o2 ,class-object))
               (assert (and (= (size-rows o1) (size-rows o2))
                            (= (size-cols o1) (size-cols o2))))
               (let ((status (,(kmrcl:concat-symbol "gsl-matrix-" func-string
                                                    "add") (ptr o1) (ptr o2))))
                 (values o1 status)))

             (defmethod sub ((o1 ,class-object) (o2 ,class-object))
               (assert (and (= (size-rows o1) (size-rows o2))
                            (= (size-cols o1) (size-cols o2))))
               (let ((status (,(kmrcl:concat-symbol "gsl-matrix-" func-string
                                                    "sub") (ptr o1) (ptr o2))))
                 (values o1 status)))

             (defmethod mul-elements ((o1 ,class-object) (o2 ,class-object))
               (assert (and (= (size-rows o1) (size-rows o2))
                            (= (size-cols o1) (size-cols o2))))
               (let ((status (,(kmrcl:concat-symbol "gsl-matrix-" func-string
                                                    "mul-elements")
                               (ptr o1) (ptr o2))))
                 (values o1 status)))

             (defmethod div-elements ((o1 ,class-object) (o2 ,class-object))
               (assert (and (= (size-rows o1) (size-rows o2))
                            (= (size-cols o1) (size-cols o2))))
               (let ((status (,(kmrcl:concat-symbol "gsl-matrix-" func-string
                                                    "div-elements")
                               (ptr o1) (ptr o2))))
                 (values o1 status)))

             (defmethod scale ((o ,class-object) x)
               (assert (typep x (element-type o)))
               ;; coerce to double-float looks wrong, but isn't.
               (,(kmrcl:concat-symbol "gsl-matrix-" func-string "scale")
                 (ptr o) (coerce x 'double-float)))

             (defmethod add-constant ((o ,class-object) x)
               (assert (typep x (element-type o)))
               ;; coerce to double-float looks wrong, but isn't.
               (,(kmrcl:concat-symbol "gsl-matrix-" func-string "add-constant")
                 (ptr o) (coerce x 'double-float)))

             (defmethod max-value ((o ,class-object))
               (,(kmrcl:concat-symbol "gsl-matrix-" func-string "max") (ptr o)))

             (defmethod min-value ((o ,class-object))
               (,(kmrcl:concat-symbol "gsl-matrix-" func-string "min") (ptr o)))


             (defmethod max-index ((o ,class-object))
               (let ((i-ptr (uffi:allocate-foreign-object 'size-t))
                     (j-ptr (uffi:allocate-foreign-object 'size-t)))
                 (,(kmrcl:concat-symbol "gsl-matrix-" func-string "max-index")
                   (ptr o) i-ptr j-ptr)
                 (prog1
                     (list (uffi:deref-pointer i-ptr 'size-t)
                           (uffi:deref-pointer j-ptr 'size-t))
                   (uffi:free-foreign-object i-ptr)
                   (uffi:free-foreign-object j-ptr))))

             (defmethod min-index ((o ,class-object))
               (let ((i-ptr (uffi:allocate-foreign-object 'size-t))
                     (j-ptr (uffi:allocate-foreign-object 'size-t)))
                 (,(kmrcl:concat-symbol "gsl-matrix-" func-string "min-index")
                   (ptr o) i-ptr j-ptr)
                 (prog1
                     (list (uffi:deref-pointer i-ptr 'size-t)
                           (uffi:deref-pointer j-ptr 'size-t))
                   (uffi:free-foreign-object i-ptr)
                   (uffi:free-foreign-object j-ptr))))

             (defmethod min-max-indicies ((o ,class-object))
               (list (min-index o) (max-index o)))

             (defmethod min-max-values ((o ,class-object))
               (destructuring-bind ((i-min j-min) (i-max j-max))
                   (min-max-indicies o)
                 (list (get-element o i-min j-min)
                       (get-element o i-max j-max))))
             ))
       )))


(def-matrix-methods% "integer" "int-")
(def-matrix-methods% "single-float" "float-")
(def-matrix-methods% "double-float" "")
(def-matrix-methods% "complex-single-float" "complex-float-")
(def-matrix-methods% "complex-double-float" "complex-")


(defun make-matrix (size-rows size-cols
                    &key (element-type 'double-float) initial-element
                    initial-contents from-file from-binary-file)
  (assert (and (typep size-rows 'integer) (> size-rows 0) ))
  (assert (and (typep size-cols 'integer) (> size-cols 0) ))
  (assert (find element-type '(integer single-float double-float
                               (complex (single-float))
                               (complex (double-float))) :test #'equal))
  (let ((m (cond
             ((eq element-type 'integer)
              (make-instance 'gsl-matrix-integer
                             :size-rows size-rows :size-cols size-cols
                             :element-type element-type))
             ((eq element-type 'double-float)
              (make-instance 'gsl-matrix-double-float
                             :size-rows size-rows :size-cols size-cols
                             :element-type element-type))
             ((eq element-type 'single-float)
              (make-instance 'gsl-matrix-single-float
                             :size-rows size-rows :size-cols size-cols
                             :element-type element-type))
             ((equal element-type '(complex (double-float)))
              (make-instance 'gsl-matrix-complex-double-float
                             :size-rows size-rows :size-cols size-cols
                             :element-type element-type))
             ((equal element-type '(complex (single-float)))
              (make-instance 'gsl-matrix-complex-single-float
                             :size-rows size-rows :size-cols size-cols
                             :element-type element-type))
             (t
              (error "should never get here.")))))
    (alloc m)
    (cond
      ((and initial-element initial-contents from-file from-binary-file)
       (error "can only define one of the keys: initial-element, initial-contents, from-file, from-binary-file."))
      (initial-element
       (set-all m initial-element))
      (initial-contents
       (cond
         ((arrayp initial-contents)
          (dotimes (i size-rows)
            (dotimes (j size-cols)
              (set-element m i j (aref initial-contents i j)))))
         (t
          (error "initial-contents must be an array."))))
      (from-file
       (read-from-file m from-file))
      (from-binary-file
       (read-from-binary-file m from-binary-file)))
    m))


(defmacro with-matrix
    ((m size-rows size-cols &key element-type initial-element initial-contents
        from-file from-binary-file) &body body)
  `(let ((,m (make-matrix ,size-rows ,size-cols
                          :element-type (or ,element-type 'double-float)
                          :initial-element ,initial-element
                          :initial-contents ,initial-contents
                          :from-file ,from-file
                          :from-binary-file ,from-binary-file)))
     (unwind-protect
          (progn ,@body)
       (free ,m))))


(defmacro def-matrix-copy-method% (class-string func-string)
  (let ((class-object (kmrcl:concat-symbol "gsl-matrix-" class-string)))
    `(defmethod copy ((o ,class-object))
       (let* ((o-copy (make-matrix (size-rows o) (size-cols o)
                                   :element-type (element-type o)))
              (status (,(kmrcl:concat-symbol "gsl-matrix-" func-string
                                             "memcpy") (ptr o-copy) (ptr o))))
         (values o-copy status)))))

(def-matrix-copy-method% "integer" "int-")
(def-matrix-copy-method% "single-float" "float-")
(def-matrix-copy-method% "double-float" "")
(def-matrix-copy-method% "complex-single-float" "complex-float-")
(def-matrix-copy-method% "complex-double-float" "complex-")


(defmacro with-matrix-copy ((m-dest m-src) &body body)
  `(let ((,m-dest (copy ,m-src)))
     (unwind-protect
          ,@body
       (free ,m-dest))))

(defmacro with-copy-transpose ((m-dest m-src) &body body)
  `(gsl-array:with-matrix
       (,m-dest (size-rows ,m-src) (size-cols ,m-src)
                :element-type (element-type ,m-src))
     (copy-transpose ,m-dest ,m-src)
          ,@body))

(defmacro with-matrix-row ((v m row) &body body)
    `(let ((,v (get-row ,m ,row)))
       (unwind-protect
            ,@body
         (free ,v))))

(defmacro with-matrix-col ((v m col) &body body)
    `(let ((,v (get-col ,m ,col)))
       (unwind-protect
            ,@body
         (free ,v))))

(defun gsl-matrix->lisp-array (m)
  (let ((a (make-array (list (size-rows m) (size-cols m))
                       :element-type (element-type m))))
    (dotimes (i (size-rows m))
      (dotimes (j (size-cols m))
        (setf (aref a i j) (get-element m i j))))
    a))
