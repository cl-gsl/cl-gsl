;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl)

(defvar +null-void-pointer+ (uffi:make-null-pointer :void))

(defun t/nil->1/0 (val)
  (if val 1 0))

(defun 1/0->t/nil (val)
  (if (equal val 1) t nil))

(defun string_->string- (str)
    (cl-ppcre:regex-replace-all "_" str "-"))

;; ----------------------------------------------------------------------

(defmacro defun-foreign (name params+types ret-type)
  `(uffi:def-function
       ,name
       ,params+types
     :module "cl-gsl"
     :returning ,ret-type))

(defmacro define-foreign-type (gsl-type uffi-type)
  (let ((new-type (kmrcl:concat-symbol gsl-type "-def")))
    `(progn
       (uffi:def-foreign-type ,gsl-type ,uffi-type)
       (uffi:def-type ,new-type ,uffi-type)
       (export '(,gsl-type ,new-type)))))

(defmacro def-foreign-struct (name &rest params+types)
  (let ((new-type (kmrcl:concat-symbol name "-def")))
    `(progn
       (uffi:def-struct ,name
           ,@params+types)
     (uffi:def-type ,new-type ,name)
     (export '(,name ,new-type)))))

;; ----------------------------------------------------------------------

(define-foreign-type size-t :unsigned-long)

(def-foreign-struct gsl-complex
    (dat (:array :double 2)))

(def-foreign-struct gsl-poly-complex-workspace
    (nc size-t)
    (matrix (* :double)))

(def-foreign-struct gsl-complex-float
    (dat (:array :float 2)))

(def-foreign-struct gsl-permutation-struct
    (size size-t)
    (data (* size-t)))

;; ----------------------------------------------------------------------

(defmacro def-block-vector-matrix-struct% (struct-postfix data-type)
  `(progn
     (def-foreign-struct ,(kmrcl:concat-symbol 'gsl-block struct-postfix)
         (size :unsigned-long)
         (data (* ,data-type)))

     (def-foreign-struct ,(kmrcl:concat-symbol 'gsl-vector struct-postfix)
         (size :unsigned-long)
         (stride :unsigned-long)
         (data (* ,data-type))
         (g-block (* ,(kmrcl:concat-symbol 'gsl-block struct-postfix)))
         (owner :int))

     (def-foreign-struct ,(kmrcl:concat-symbol 'gsl-matrix struct-postfix)
         (size1 :unsigned-long)
         (size2 :unsigned-long)
         (tda :unsigned-long)
         (data (* ,data-type))
         (g-block (* ,(kmrcl:concat-symbol 'gsl-block struct-postfix)))
         (owner :int))

     ;; FIXME: is this correct?
     (def-foreign-struct ,(kmrcl:concat-symbol 'gsl-vector struct-postfix '-view)
         (vec (* ,(kmrcl:concat-symbol 'gsl-vector struct-postfix))))))


(def-block-vector-matrix-struct% "" :double)
(def-block-vector-matrix-struct% "-float" :float)
(def-block-vector-matrix-struct% "-int" :int)
(def-block-vector-matrix-struct% "-complex" :double)
(def-block-vector-matrix-struct% "-complex-float" :float)

;; ----------------------------------------------------------------------

(defmacro register-foreign-types ()
  `(progn
     ,@(mapcar #'(lambda (elm) `(define-foreign-type ,(car elm) ,(cadr elm)))
               '((double-ptr '(* :double))
                 (gsl-root-fsolver :pointer-void)
                 (gsl-root-fsolver-type :pointer-void)

                 (gsl-complex-ptr '(* gsl-complex))
                 (gsl-poly-complex-workspace-ptr '(* gsl-complex))
                 (gsl-complex-packed '(* :double))
                 (gsl-complex-packed-float '(* :float))
                 (gsl-complex-packed-array '(* :double))
                 (gsl-complex-packed-array-float '(* :float))
                 (gsl-complex-packed-ptr '(* :double))
                 (gsl-complex-packed-float-ptr '(* :float))
                 (gsl-mode-t :unsigned-int)

                 (gsl-vector-ptr '(* gsl-vector))
                 (gsl-vector-float-ptr '(* gsl-vector-float))
                 (gsl-vector-int-ptr '(* gsl-vector-int))
                 (gsl-vector-complex-ptr '(* gsl-vector-complex))
                 (gsl-vector-complex-float-ptr '(* gsl-vector-complex-float))

                 (gsl-matrix-ptr '(* gsl-matrix))
                 (gsl-matrix-float-ptr '(* gsl-matrix-float))
                 (gsl-matrix-int-ptr '(* gsl-matrix-int))
                 (gsl-matrix-complex-ptr '(* gsl-matrix-complex))
                 (gsl-matrix-complex-float-ptr '(* gsl-matrix-complex-float))

                 (size-t-ptr '(* size-t))

                 (gsl-permutation-ptr '(* gsl-permutation-struct))

                 (gsl-rng-type-ptr '(* :void))
                 (gsl-rng-ptr '(* :void))
                 ))))

(register-foreign-types)

;; typedef long double *  gsl_complex_packed_long_double ;
;; typedef long double *  gsl_complex_packed_array_long_double ;
;; typedef long double *  gsl_complex_packed_long_double_ptr ;

;; ----------------------------------------------------------------------

(defun gsl-complex->complex (z-ptr)
  "Copies the value of the foreign object pointed to by Z-PTR to a lisp object
of type (complex (double-float)). Returns the lisp object."
  (let ((dat-array (uffi:get-slot-value z-ptr '(:array :double) 'cl-gsl::dat)))
    (complex (uffi:deref-array dat-array :double 0)
             (uffi:deref-array dat-array :double 1))))

(defun gsl-complex-float->complex (z-ptr)
  "Copies the value of the foreign object pointed to by Z-PTR to a lisp object
of type (complex (single-float)). Returns the lisp object."
  (let ((dat-array (uffi:get-slot-value z-ptr '(:array :float) 'cl-gsl::dat)))
    (complex (uffi:deref-array dat-array :float 0)
             (uffi:deref-array dat-array :float 1))))


(defmacro with-complex-double-float->gsl-complex-ptr ((c-ptr complex-val)
                                                      &body body)
  "Copies the value of COMPLEX-VALUE, of type (complex (double-float)),
to a newly created foreign object of type gsl_complex. C-PTR is a pointer
to the foreign object. Returns the values of BODY and frees the memory
allocated for the foreign object."
  (let ((array (gensym)))
    `(let* ((,c-ptr (uffi:allocate-foreign-object 'gsl-complex))
            (,array (uffi:get-slot-value ,c-ptr
                                         '(:array :double)
                                         'cl-gsl::dat)))
       (unwind-protect
            (progn
              (setf (uffi:deref-array ,array :double 0) (realpart ,complex-val))
              (setf (uffi:deref-array ,array :double 1) (imagpart ,complex-val))
              ,@body)
         (uffi:free-foreign-object ,c-ptr)))))


(defmacro with-complex-single-float->gsl-complex-float-ptr ((c-ptr complex-val)
                                                      &body body)
  "Copies the value of COMPLEX-VALUE, of type (complex (single-float)),
to a newly created foreign object of type gsl_complex_float. C-PTR is a pointer
to the foreign object. Returns the values of BODY and frees the memory
allocated for the foreign object."
  (let ((array (gensym)))
    `(let* ((,c-ptr (uffi:allocate-foreign-object 'gsl-complex-float))
            (,array (uffi:get-slot-value ,c-ptr
                                         '(:array :float)
                                         'cl-gsl::dat)))
       (unwind-protect
            (progn
              (setf (uffi:deref-array ,array :float 0) (realpart ,complex-val))
              (setf (uffi:deref-array ,array :float 1) (imagpart ,complex-val))
              ,@body)
         (uffi:free-foreign-object ,c-ptr)))))


(defmacro with-lisp-vec->c-array ((c-ptr lisp-vec) &body body)
  (let ((len (gensym))
        (i (gensym)))
    `(progn
       (let* ((,len (length ,lisp-vec))
              (,c-ptr (uffi:allocate-foreign-object :double ,len)))
         (unwind-protect
              (progn
                (dotimes (,i ,len)
                  (setf (uffi:deref-array ,c-ptr :double ,i)
                        (aref ,lisp-vec ,i)))
                ,@body)
           (uffi:free-foreign-object ,c-ptr))))))


(defun c-array->lisp-vec (c-ptr len)
  (let ((lisp-vec (make-array len :element-type 'double-float)))
    (dotimes (i len)
      (setf (aref lisp-vec i) (uffi:deref-array c-ptr :double i)))
    lisp-vec))

(defun complex-packed-array->lisp-vec (z-ptr len)
  "Copies the complex values of a foreign array to a lisp array. Z-PTR is
a pointer the the foreign array of length LEN. Returns a lisp array of
complex elements, also of length LEN."
  (declare (gsl-complex-packed-def z-ptr))
  (let ((lisp-vec (make-array (/ len 2) :element-type 'complex)))
    (dotimes (i (/ len 2))
      (setf (aref lisp-vec i)
            (complex (uffi:deref-array z-ptr :double (* i 2))
                     (uffi:deref-array z-ptr :double (1+ (* i 2))))))
    lisp-vec))
