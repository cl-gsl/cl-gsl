/* gcc `gsl-config --cflags` -c gsl_test2.c
   gcc gsl_test2.o `gsl-config --libs`
*/
#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>

/*
struct gsl_function_struct
{
  double (* function) (double x, void * params);
  void * params;
};

typedef struct gsl_function_struct gsl_function ;


struct gsl_function_fdf_struct
{
  double (* f) (double x, void * params);
  double (* df) (double x, void * params);
  void (* fdf) (double x, void * params, double * f, double * df);
  void * params;
};

typedef struct gsl_function_fdf_struct gsl_function_fdf ;

*/

struct quadratic_params {
    double a, b, c;
};

double quadratic (double x, void *params)
{
    struct quadratic_params *p = (struct quadratic_params *) params;

    double a = p->a;
    double b = p->b;
    double c = p->c;

    return (a * x + b) * x + c;
}

void wrap_test_function(double (*func)(double, void *), void *ps)
{
     gsl_function F;

     F.function = func;
     F.params = ps;

     GSL_FN_EVAL(&F, 2.0);
}

int run_function(double (*func)(double, void *), void *ps,
                 const gsl_root_fsolver_type *T, double lo, double hi)
{
    int status;
    int iter = 0, max_iter = 100;
    gsl_root_fsolver *s;
    double r = 0, r_expected = sqrt (5.0);
    double x_lo, x_hi;
    gsl_function F;

    F.function = func;
    F.params = ps;

    x_lo = lo;
    x_hi = hi;

    s = gsl_root_fsolver_alloc (T);

    gsl_root_fsolver_set (s, &F, x_lo, x_hi);

    printf ("using %s method\n",
            gsl_root_fsolver_name (s));

    printf ("%5s [%9s, %9s] %9s %10s %9s\n",
            "iter", "lower", "upper", "root",
            "err", "err(est)");

    do
    {
        iter++;
        status = gsl_root_fsolver_iterate (s);
        r = gsl_root_fsolver_root (s);
        x_lo = gsl_root_fsolver_x_lower (s);
        x_hi = gsl_root_fsolver_x_upper (s);
        status = gsl_root_test_interval (x_lo, x_hi,
                                         0, 0.001);

        if (status == GSL_SUCCESS)
            printf ("Converged:\n");

        printf ("%5d [%.7f, %.7f] %.7f %+.7f %.7f\n",
                iter, x_lo, x_hi,
                r, r - r_expected,
                x_hi - x_lo);
    }
    while (status == GSL_CONTINUE && iter < max_iter);
    return status;
}

/* int main (void) */
/* { */
/*     int result; */
/*     struct quadratic_params params = {1.0, 0.0, -5.0}; */
/*     const gsl_root_fsolver_type *T; */

/*     T = gsl_root_fsolver_brent; */
/*     result = run_function(&quadratic, &params, T, 0.0, 5.0); */
/*     return result; */
/* } */

int main (void)
{
    int result;
    struct quadratic_params params = {1.0, 2.0, 3.0};

    wrap_test_function(&quadratic, &params);
    return 1;
}

