/*
 Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
 This file is part of CL-GSL.

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <string.h>

#include <gsl/gsl_blas.h>
#include <gsl/gsl_block.h>
#include <gsl/gsl_block_complex.h>
#include <gsl/gsl_cblas.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_chebyshev.h>
#include <gsl/gsl_check_range.h>
#include <gsl/gsl_combination.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_const.h>
#include <gsl/gsl_deriv.h>
#include <gsl/gsl_dft_complex.h>
#include <gsl/gsl_dft_complex_float.h>
#include <gsl/gsl_dht.h>
#include <gsl/gsl_diff.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_fft.h>
#include <gsl/gsl_fft_complex.h>
#include <gsl/gsl_fft_complex_float.h>
#include <gsl/gsl_fft_halfcomplex.h>
#include <gsl/gsl_fft_halfcomplex_float.h>
#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_real_float.h>
#include <gsl/gsl_fit.h>
#include <gsl/gsl_heapsort.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_histogram2d.h>
#include <gsl/gsl_ieee_utils.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_interp.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_machine.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_message.h>
#include <gsl/gsl_min.h>
#include <gsl/gsl_mode.h>
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_miser.h>
#include <gsl/gsl_monte_plain.h>
#include <gsl/gsl_monte_vegas.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_multiroots.h>
#include <gsl/gsl_nan.h>
#include <gsl/gsl_ntuple.h>
#include <gsl/gsl_odeiv.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_permute.h>
#include <gsl/gsl_permute_vector.h>
#include <gsl/gsl_poly.h>
#include <gsl/gsl_pow_int.h>
#include <gsl/gsl_precision.h>
#include <gsl/gsl_qrng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_sf_result.h>
#include <gsl/gsl_siman.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_sort_vector.h>
#include <gsl/gsl_specfunc.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_sum.h>
#include <gsl/gsl_sys.h>
#include <gsl/gsl_test.h>
#include <gsl/gsl_types.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_vector_complex.h>
#include <gsl/gsl_version.h>
/* #include <gsl/gsl_wavelet.h>
   #include <gsl/gsl_wavelet2d.h> */

double min_light_travel_from_earth (void)
{
    double c  = GSL_CONST_MKSA_SPEED_OF_LIGHT;
    double au = GSL_CONST_MKSA_ASTRONOMICAL_UNIT;
    double minutes = GSL_CONST_MKSA_MINUTE;

    /* distance stored in meters */
    double r_earth = 1.00 * au;
    double r_mars  = 1.52 * au;

    double t_min, t_max;

    t_min = (r_mars - r_earth) / c;
    t_max = (r_mars + r_earth) / c;

    return (t_min / minutes);
}

void wrap_test_function(double (*func)(double, void *), void *ps)
{
     gsl_function F;

     F.function = func;
     F.params = ps;

     GSL_FN_EVAL(&F, 2.0);
}

const gsl_root_fsolver_type* wrap_gsl_root_fsolver_brent ()
{
    const gsl_root_fsolver_type *T;
    T = gsl_root_fsolver_brent;
    return T;
}

/* ----------------------------------------------------------------- */

int wrap_gsl_vector_fwrite(char *fn, const gsl_vector *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "wb");
    ret = gsl_vector_fwrite(stream, v);
    fclose(stream);

    return ret;
}

int wrap_gsl_vector_fread(char *fn, gsl_vector *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "rb");
    ret = gsl_vector_fread(stream, v);
    fclose(stream);

    return ret;
}

int wrap_gsl_vector_fprintf(char *fn, const gsl_vector *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "w");
    ret = gsl_vector_fprintf(stream, v, "%lf");
    fclose(stream);

    return ret;
}

int wrap_gsl_vector_fscanf(char *fn, gsl_vector *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "r");
    ret = gsl_vector_fscanf(stream, v);
    fclose(stream);

    return ret;
}

/* ----------------------------------------------------------------- */

int wrap_gsl_vector_float_fwrite(char *fn, const gsl_vector_float *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "wb");
    ret = gsl_vector_float_fwrite(stream, v);
    fclose(stream);

    return ret;
}

int wrap_gsl_vector_float_fread(char *fn, gsl_vector_float *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "rb");
    ret = gsl_vector_float_fread(stream, v);
    fclose(stream);

    return ret;
}

int wrap_gsl_vector_float_fprintf(char *fn, const gsl_vector_float *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "w");
    ret = gsl_vector_float_fprintf(stream, v, "%f");
    fclose(stream);

    return ret;
}

int wrap_gsl_vector_float_fscanf(char *fn, gsl_vector_float *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "r");
    ret = gsl_vector_float_fscanf(stream, v);
    fclose(stream);

    return ret;
}

/* ----------------------------------------------------------------- */

int wrap_gsl_vector_int_fwrite(char *fn, const gsl_vector_int *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "wb");
    ret = gsl_vector_int_fwrite(stream, v);
    fclose(stream);

    return ret;
}

int wrap_gsl_vector_int_fread(char *fn, gsl_vector_int *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "rb");
    ret = gsl_vector_int_fread(stream, v);
    fclose(stream);

    return ret;
}

int wrap_gsl_vector_int_fprintf(char *fn, const gsl_vector_int *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "w");
    ret = gsl_vector_int_fprintf(stream, v, "%d");
    fclose(stream);

    return ret;
}

int wrap_gsl_vector_int_fscanf(char *fn, gsl_vector_int *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "r");
    ret = gsl_vector_int_fscanf(stream, v);
    fclose(stream);

    return ret;
}

/* ----------------------------------------------------------------- */

int wrap_gsl_vector_complex_fwrite(char *fn, const gsl_vector_complex *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "wb");
    ret = gsl_vector_complex_fwrite(stream, v);
    fclose(stream);

    return ret;
}

int wrap_gsl_vector_complex_fread(char *fn, gsl_vector_complex *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "rb");
    ret = gsl_vector_complex_fread(stream, v);
    fclose(stream);

    return ret;
}

int wrap_gsl_vector_complex_fprintf(char *fn, const gsl_vector_complex *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "w");
    ret = gsl_vector_complex_fprintf(stream, v, "%lf");
    fclose(stream);

    return ret;
}

int wrap_gsl_vector_complex_fscanf(char *fn, gsl_vector_complex *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "r");
    ret = gsl_vector_complex_fscanf(stream, v);
    fclose(stream);

    return ret;
}

/* ----------------------------------------------------------------- */

int wrap_gsl_vector_complex_float_fwrite(char *fn,
                                         const gsl_vector_complex_float *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "wb");
    ret = gsl_vector_complex_float_fwrite(stream, v);
    fclose(stream);

    return ret;
}

int wrap_gsl_vector_complex_float_fread(char *fn, gsl_vector_complex_float *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "rb");
    ret = gsl_vector_complex_float_fread(stream, v);
    fclose(stream);

    return ret;
}

int wrap_gsl_vector_complex_float_fprintf(char *fn,
                                          const gsl_vector_complex_float *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "w");
    ret = gsl_vector_complex_float_fprintf(stream, v, "%lf");
    fclose(stream);

    return ret;
}

int wrap_gsl_vector_complex_float_fscanf(char *fn, gsl_vector_complex_float *v)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "r");
    ret = gsl_vector_complex_float_fscanf(stream, v);
    fclose(stream);

    return ret;
}

/* ----------------------------------------------------------------- */

gsl_vector *wrap_gsl_vector_subvector(gsl_vector *v, size_t offset, size_t n)
{
    gsl_vector *ret;
    gsl_vector_view v_view = gsl_vector_subvector(v, offset, n);
    ret = &v_view.vector;
    return ret;
}

gsl_vector_float *wrap_gsl_vector_float_subvector(gsl_vector_float *v,
                                                  size_t offset,
                                                  size_t n)
{
    gsl_vector_float *ret;
    gsl_vector_float_view v_view = gsl_vector_float_subvector(v, offset, n);
    ret = &v_view.vector;
    return ret;
}

gsl_vector_int *wrap_gsl_vector_int_subvector(gsl_vector_int *v,
                                              size_t offset,
                                              size_t n)
{
    gsl_vector_int *ret;
    gsl_vector_int_view v_view = gsl_vector_int_subvector(v, offset, n);
    ret = &v_view.vector;
    return ret;
}

gsl_vector_complex *wrap_gsl_vector_complex_subvector(gsl_vector_complex *v,
                                                      size_t offset,
                                                      size_t n)
{
    gsl_vector_complex *ret;
    gsl_vector_complex_view v_view = gsl_vector_complex_subvector(v, offset, n);
    ret = &v_view.vector;
    return ret;
}

gsl_vector_complex_float *wrap_gsl_vector_complex_float_subvector(
        gsl_vector_complex_float *v,
        size_t offset,
        size_t n)
{
    gsl_vector_complex_float *ret;
    gsl_vector_complex_float_view v_view =
        gsl_vector_complex_float_subvector(v, offset, n);
    ret = &v_view.vector;
    return ret;
}

/* ----------------------------------------------------------------- */

gsl_vector *wrap_gsl_vector_subvector_with_stride(
        gsl_vector *v,
        size_t offset,
        size_t stride,
        size_t n)
{
    gsl_vector *ret;
    gsl_vector_view v_view =
        gsl_vector_subvector_with_stride(v, offset, stride, n);
    ret = &v_view.vector;
    return ret;
}

gsl_vector_float *wrap_gsl_vector_float_subvector_with_stride(
        gsl_vector_float *v,
        size_t offset,
        size_t stride,
        size_t n)
{
    gsl_vector_float *ret;
    gsl_vector_float_view v_view =
        gsl_vector_float_subvector_with_stride(v, offset, stride, n);
    ret = &v_view.vector;
    return ret;
}

gsl_vector_int *wrap_gsl_vector_int_subvector_with_stride(
        gsl_vector_int *v,
        size_t offset,
        size_t stride,
        size_t n)
{
    gsl_vector_int *ret;
    gsl_vector_int_view v_view =
        gsl_vector_int_subvector_with_stride(v, offset, stride, n);
    ret = &v_view.vector;
    return ret;
}

gsl_vector_complex *wrap_gsl_vector_complex_subvector_with_stride(
        gsl_vector_complex *v,
        size_t offset,
        size_t stride,
        size_t n)
{
    gsl_vector_complex *ret;
    gsl_vector_complex_view v_view =
        gsl_vector_complex_subvector_with_stride(v, offset, stride, n);
    ret = &v_view.vector;
    return ret;
}

gsl_vector_complex_float *wrap_gsl_vector_complex_float_subvector_with_stride(
        gsl_vector_complex_float *v,
        size_t offset,
        size_t stride,
        size_t n)
{
    gsl_vector_complex_float *ret;
    gsl_vector_complex_float_view v_view =
        gsl_vector_complex_float_subvector_with_stride(v, offset, stride, n);
    ret = &v_view.vector;
    return ret;
}

/* ----------------------------------------------------------------- */

void wrap_gsl_vector_complex_float_set(gsl_vector_complex_float *v,
                                       const size_t i,
                                       gsl_complex_float *z)
{
    v->data[2 * i * v->stride] = z->dat[0];
    v->data[(2 * i * v->stride) + 1] = z->dat[1];
}

void wrap_gsl_vector_complex_set(gsl_vector_complex *v,
                                 const size_t i,
                                 gsl_complex *z)
{
    v->data[2 * i * v->stride] = z->dat[0];
    v->data[(2 * i * v->stride) + 1] = z->dat[1];
}

void wrap_gsl_vector_complex_set_all(gsl_vector_complex *v,
                                 gsl_complex *z)
{
    gsl_vector_complex_set_all(v , *z);
}

void wrap_gsl_vector_complex_float_set_all(gsl_vector_complex_float *v,
                                           gsl_complex_float *z)
{
    gsl_vector_complex_float_set_all(v , *z);
}


/* ----------------------------------------------------------------- */

void wrap_gsl_matrix_complex_float_set(gsl_matrix_complex_float *m,
                                       const size_t i,
                                       const size_t j,
                                       gsl_complex_float *z)
{
    m->data[2 * (i * m->tda + j)] = z->dat[0];
    m->data[(2 * (i * m->tda + j)) + 1] = z->dat[1];
}

void wrap_gsl_matrix_complex_set(gsl_matrix_complex *m,
                                 const size_t i,
                                 const size_t j,
                                 gsl_complex *z)
{
    m->data[2 * (i * m->tda + j)] = z->dat[0];
    m->data[(2 * (i * m->tda + j)) + 1] = z->dat[1];
}

void wrap_gsl_matrix_complex_set_all(gsl_matrix_complex *m,
                                     gsl_complex *z)
{
    gsl_matrix_complex_set_all(m , *z);
}

void wrap_gsl_matrix_complex_float_set_all(gsl_matrix_complex_float *m,
                                           gsl_complex_float *z)
{
    gsl_matrix_complex_float_set_all(m , *z);
}

/* ----------------------------------------------------------------- */

int wrap_gsl_matrix_fwrite(char *fn, const gsl_matrix *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "wb");
    ret = gsl_matrix_fwrite(stream, m);
    fclose(stream);

    return ret;
}

int wrap_gsl_matrix_fread(char *fn, gsl_matrix *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "rb");
    ret = gsl_matrix_fread(stream, m);
    fclose(stream);

    return ret;
}

int wrap_gsl_matrix_fprintf(char *fn, const gsl_matrix *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "w");
    ret = gsl_matrix_fprintf(stream, m, "%lf");
    fclose(stream);

    return ret;
}

int wrap_gsl_matrix_fscanf(char *fn, gsl_matrix *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "r");
    ret = gsl_matrix_fscanf(stream, m);
    fclose(stream);

    return ret;
}

/* ----------------------------------------------------------------- */

int wrap_gsl_matrix_float_fwrite(char *fn, const gsl_matrix_float *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "wb");
    ret = gsl_matrix_float_fwrite(stream, m);
    fclose(stream);

    return ret;
}

int wrap_gsl_matrix_float_fread(char *fn, gsl_matrix_float *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "rb");
    ret = gsl_matrix_float_fread(stream, m);
    fclose(stream);

    return ret;
}

int wrap_gsl_matrix_float_fprintf(char *fn, const gsl_matrix_float *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "w");
    ret = gsl_matrix_float_fprintf(stream, m, "%f");
    fclose(stream);

    return ret;
}

int wrap_gsl_matrix_float_fscanf(char *fn, gsl_matrix_float *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "r");
    ret = gsl_matrix_float_fscanf(stream, m);
    fclose(stream);

    return ret;
}

/* ----------------------------------------------------------------- */

int wrap_gsl_matrix_int_fwrite(char *fn, const gsl_matrix_int *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "wb");
    ret = gsl_matrix_int_fwrite(stream, m);
    fclose(stream);

    return ret;
}

int wrap_gsl_matrix_int_fread(char *fn, gsl_matrix_int *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "rb");
    ret = gsl_matrix_int_fread(stream, m);
    fclose(stream);

    return ret;
}

int wrap_gsl_matrix_int_fprintf(char *fn, const gsl_matrix_int *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "w");
    ret = gsl_matrix_int_fprintf(stream, m, "%d");
    fclose(stream);

    return ret;
}

int wrap_gsl_matrix_int_fscanf(char *fn, gsl_matrix_int *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "r");
    ret = gsl_matrix_int_fscanf(stream, m);
    fclose(stream);

    return ret;
}

/* ----------------------------------------------------------------- */

int wrap_gsl_matrix_complex_fwrite(char *fn, const gsl_matrix_complex *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "wb");
    ret = gsl_matrix_complex_fwrite(stream, m);
    fclose(stream);

    return ret;
}

int wrap_gsl_matrix_complex_fread(char *fn, gsl_matrix_complex *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "rb");
    ret = gsl_matrix_complex_fread(stream, m);
    fclose(stream);

    return ret;
}

int wrap_gsl_matrix_complex_fprintf(char *fn, const gsl_matrix_complex *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "w");
    ret = gsl_matrix_complex_fprintf(stream, m, "%lf");
    fclose(stream);

    return ret;
}

int wrap_gsl_matrix_complex_fscanf(char *fn, gsl_matrix_complex *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "r");
    ret = gsl_matrix_complex_fscanf(stream, m);
    fclose(stream);

    return ret;
}

/* ----------------------------------------------------------------- */

int wrap_gsl_matrix_complex_float_fwrite(char *fn,
                                         const gsl_matrix_complex_float *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "wb");
    ret = gsl_matrix_complex_float_fwrite(stream, m);
    fclose(stream);

    return ret;
}

int wrap_gsl_matrix_complex_float_fread(char *fn, gsl_matrix_complex_float *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "rb");
    ret = gsl_matrix_complex_float_fread(stream, m);
    fclose(stream);

    return ret;
}

int wrap_gsl_matrix_complex_float_fprintf(char *fn,
                                          const gsl_matrix_complex_float *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "w");
    ret = gsl_matrix_complex_float_fprintf(stream, m, "%lf");
    fclose(stream);

    return ret;
}

int wrap_gsl_matrix_complex_float_fscanf(char *fn, gsl_matrix_complex_float *m)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "r");
    ret = gsl_matrix_complex_float_fscanf(stream, m);
    fclose(stream);

    return ret;
}

/* ----------------------------------------------------------------- */

int wrap_gsl_permutation_fwrite(char *fn, const gsl_permutation *p)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "wb");
    ret = gsl_permutation_fwrite(stream, p);
    fclose(stream);

    return ret;
}

int wrap_gsl_permutation_fread(char *fn, gsl_permutation *p)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "rb");
    ret = gsl_permutation_fread(stream, p);
    fclose(stream);

    return ret;
}

int wrap_gsl_permutation_fprintf(char *fn, const gsl_permutation *p)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "w");
    ret = gsl_permutation_fprintf(stream, p, "%u\n");
    fclose(stream);

    return ret;
}

int wrap_gsl_permutation_fscanf(char *fn, gsl_permutation *p)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "r");
    ret = gsl_permutation_fscanf(stream, p);
    fclose(stream);

    return ret;
}

gsl_rng *wrap_gsl_rng_set_type(char *tp)
{
    gsl_rng *r;

    if (strncmp( "borosh13", tp, 8) == 0)
        r = gsl_rng_alloc (gsl_rng_borosh13);
    else if (strncmp( "coveyou", tp, 7) == 0)
        r = gsl_rng_alloc(gsl_rng_coveyou);
    else if (strncmp( "cmrg", tp, 4) == 0)
        r = gsl_rng_alloc(gsl_rng_cmrg);
    else if (strncmp( "fishman18", tp, 9) == 0)
        r = gsl_rng_alloc(gsl_rng_fishman18);
    else if (strncmp( "fishman20", tp, 9) == 0)
        r = gsl_rng_alloc(gsl_rng_fishman20);
    else if (strncmp( "fishman2x", tp, 9) == 0)
        r = gsl_rng_alloc(gsl_rng_fishman2x);
    else if (strncmp( "gfsr4", tp, 5) == 0)
        r = gsl_rng_alloc(gsl_rng_gfsr4);
    else if (strncmp( "knuthran2", tp, 9) == 0)
        r = gsl_rng_alloc(gsl_rng_knuthran2);
    else if (strncmp( "knuthran", tp, 8) == 0)
        r = gsl_rng_alloc(gsl_rng_knuthran);
    else if (strncmp( "lecuyer21", tp, 9) == 0)
        r = gsl_rng_alloc(gsl_rng_lecuyer21);
    else if (strncmp( "minstd", tp, 6) == 0)
        r = gsl_rng_alloc(gsl_rng_minstd);
    else if (strncmp( "mrg", tp, 3) == 0)
        r = gsl_rng_alloc(gsl_rng_mrg);
    else if (strncmp( "mt19937-1999", tp, 12) == 0)
        r = gsl_rng_alloc(gsl_rng_mt19937_1999);
    else if (strncmp( "mt19937-1998", tp, 12) == 0)
        r = gsl_rng_alloc(gsl_rng_mt19937_1998);
    else if (strncmp( "mt19937", tp, 7) == 0)
        r = gsl_rng_alloc(gsl_rng_mt19937);
    else if (strncmp( "r250", tp, 4) == 0)
        r = gsl_rng_alloc(gsl_rng_r250);
    else if (strncmp( "ran0", tp, 4) == 0)
        r = gsl_rng_alloc(gsl_rng_ran0);
    else if (strncmp( "ran1", tp, 4) == 0)
        r = gsl_rng_alloc(gsl_rng_ran1);
    else if (strncmp( "ran2", tp, 4) == 0)
        r = gsl_rng_alloc(gsl_rng_ran2);
    else if (strncmp( "ran3", tp, 4) == 0)
        r = gsl_rng_alloc(gsl_rng_ran3);
    else if (strncmp( "randu", tp, 5) == 0)
        r = gsl_rng_alloc(gsl_rng_randu);
    else if (strncmp( "rand48", tp, 6) == 0)
        r = gsl_rng_alloc(gsl_rng_rand48);
    else if (strncmp( "random128-bsd", tp, 13) == 0)
        r = gsl_rng_alloc(gsl_rng_random128_bsd);
    else if (strncmp( "random128-glibc2", tp, 16) == 0)
        r = gsl_rng_alloc(gsl_rng_random128_glibc2);
    else if (strncmp( "random128-libc5", tp, 15) == 0)
        r = gsl_rng_alloc(gsl_rng_random128_libc5);
    else if (strncmp( "random256-bsd", tp, 13) == 0)
        r = gsl_rng_alloc(gsl_rng_random256_bsd);
    else if (strncmp( "random256-glibc2", tp, 16) == 0)
        r = gsl_rng_alloc(gsl_rng_random256_glibc2);
    else if (strncmp( "random256-libc5", tp, 15) == 0)
        r = gsl_rng_alloc(gsl_rng_random256_libc5);
    else if (strncmp( "random32-bsd", tp, 12) == 0)
        r = gsl_rng_alloc(gsl_rng_random32_bsd);
    else if (strncmp( "random32-glibc2", tp, 15) == 0)
        r = gsl_rng_alloc(gsl_rng_random32_glibc2);
    else if (strncmp( "random32-libc5", tp, 14) == 0)
        r = gsl_rng_alloc(gsl_rng_random32_libc5);
    else if (strncmp( "random64-bsd", tp, 12) == 0)
        r = gsl_rng_alloc(gsl_rng_random64_bsd);
    else if (strncmp( "random64-glibc2", tp, 15) == 0)
        r = gsl_rng_alloc(gsl_rng_random64_glibc2);
    else if (strncmp( "random64-libc5", tp, 14) == 0)
        r = gsl_rng_alloc(gsl_rng_random64_libc5);
    else if (strncmp( "random8-bsd", tp, 11) == 0)
        r = gsl_rng_alloc(gsl_rng_random8_bsd);
    else if (strncmp( "random8-glibc2", tp, 14) == 0)
        r = gsl_rng_alloc(gsl_rng_random8_glibc2);
    else if (strncmp( "random8-libc5", tp, 13) == 0)
        r = gsl_rng_alloc(gsl_rng_random8_libc5);
    else if (strncmp( "random-bsd", tp, 10) == 0)
        r = gsl_rng_alloc(gsl_rng_random_bsd);
    else if (strncmp( "random-glibc2", tp, 13) == 0)
        r = gsl_rng_alloc(gsl_rng_random_glibc2);
    else if (strncmp( "random-libc5", tp, 12) == 0)
        r = gsl_rng_alloc(gsl_rng_random_libc5);
    else if (strncmp( "rand", tp, 4) == 0)
        r = gsl_rng_alloc(gsl_rng_rand);
    else if (strncmp( "ranf", tp, 4) == 0)
        r = gsl_rng_alloc(gsl_rng_ranf);
    else if (strncmp( "ranlux389", tp, 9) == 0)
        r = gsl_rng_alloc(gsl_rng_ranlux389);
    else if (strncmp( "ranlux", tp, 6) == 0)
        r = gsl_rng_alloc(gsl_rng_ranlux);
    else if (strncmp( "ranlxd1", tp, 7) == 0)
        r = gsl_rng_alloc(gsl_rng_ranlxd1);
    else if (strncmp( "ranlxd2", tp, 7) == 0)
        r = gsl_rng_alloc(gsl_rng_ranlxd2);
    else if (strncmp( "ranlxs0", tp, 7) == 0)
        r = gsl_rng_alloc(gsl_rng_ranlxs0);
    else if (strncmp( "ranlxs1", tp, 7) == 0)
        r = gsl_rng_alloc(gsl_rng_ranlxs1);
    else if (strncmp( "ranlxs2", tp, 7) == 0)
        r = gsl_rng_alloc(gsl_rng_ranlxs2);
    else if (strncmp( "ranmar", tp, 6) == 0)
        r = gsl_rng_alloc(gsl_rng_ranmar);
    else if (strncmp( "slatec", tp, 6) == 0)
        r = gsl_rng_alloc(gsl_rng_slatec);
    else if (strncmp( "taus113", tp, 7) == 0)
        r = gsl_rng_alloc(gsl_rng_taus113);
    else if (strncmp( "taus2", tp, 5) == 0)
        r = gsl_rng_alloc(gsl_rng_taus2);
    else if (strncmp( "taus", tp, 4) == 0)
        r = gsl_rng_alloc(gsl_rng_taus);
    else if (strncmp( "transputer", tp, 10) == 0)
        r = gsl_rng_alloc(gsl_rng_transputer);
    else if (strncmp( "tt800", tp, 5) == 0)
        r = gsl_rng_alloc(gsl_rng_tt800);
    else if (strncmp( "uni32", tp, 5) == 0)
        r = gsl_rng_alloc(gsl_rng_uni32);
    else if (strncmp( "uni", tp, 3) == 0)
        r = gsl_rng_alloc(gsl_rng_uni);
    else if (strncmp( "vax", tp, 3) == 0)
        r = gsl_rng_alloc(gsl_rng_vax);
    else if (strncmp( "waterman14", tp, 10) == 0)
        r = gsl_rng_alloc(gsl_rng_waterman14);
    else if (strncmp( "zuf", tp, 3) == 0)
        r = gsl_rng_alloc(gsl_rng_zuf);
    else
        r = gsl_rng_alloc(gsl_rng_default);

    return r;
}

/* ----------------------------------------------------------------- */

int wrap_gsl_rng_fwrite(char *fn, const gsl_rng *r)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "wb");
    ret = gsl_rng_fwrite(stream, r);
    fclose(stream);

    return ret;
}

int wrap_gsl_rng_fread(char *fn, gsl_rng *r)
{
    FILE* stream;
    int ret;

    stream = fopen(fn, "rb");
    ret = gsl_rng_fread(stream, r);
    fclose(stream);

    return ret;
}
