;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl-array)

(defclass gsl-permutation ()
  ((ptr :accessor ptr :initarg :ptr)
   (size :accessor size :initarg :size)
   (element-type :accessor element-type :initform 'integer)))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_permutation_alloc"
    ((size size-t))
  gsl-permutation-ptr)

(defmethod alloc ((o gsl-permutation))
  (setf (ptr o) (gsl-permutation-alloc (size o)))
  o)

;; ----------------------------------------------------------------------

(defun-foreign "gsl_permutation_init"
    ((p gsl-permutation-ptr))
  :void)

(defmethod permutation-init ((o gsl-permutation))
  (gsl-permutation-init (ptr o))
  o)

;; ----------------------------------------------------------------------

(defun-foreign "gsl_permutation_free"
    ((p gsl-permutation-ptr))
  :void)

(defmethod free ((o gsl-permutation))
  (gsl-permutation-free (ptr o))
  (setf (ptr o) nil)
  (setf (size o) nil))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_permutation_get"
    ((p gsl-permutation-ptr)
     (i size-t))
  size-t)

(defmethod get-element ((o gsl-permutation) i &optional j)
  (assert (and (typep i 'integer) (>= i 0) (< i (size o))))
  (gsl-permutation-get (ptr o) i))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_permutation_swap"
    ((p gsl-permutation-ptr)
     (i size-t)
     (j size-t))
  size-t)

(defmethod swap-elements ((o gsl-permutation) i j)
  (assert (and (typep i 'integer) (>= i 0) (< i (size o))))
  (assert (and (typep j 'integer) (>= j 0) (< j (size o))))
  (let ((status (gsl-permutation-swap (ptr o) i j)))
    (values o status)))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_permutation_valid"
    ((p gsl-permutation-ptr))
  :int)

(defmethod isvalid ((o gsl-permutation))
  ;; The C function gsl_permutation_valid does not return when the
  ;; permutation is invalid - instead it calls GSL_ERROR.
  ;; It only returns a value when the permutation is valid.
  (ignore-errors
    (= (gsl-permutation-valid (ptr o)) +success+)))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_permutation_reverse"
    ((p gsl-permutation-ptr))
  :void)

(defmethod reverse-permutation ((o gsl-permutation))
  (gsl-permutation-reverse (ptr o)))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_permutation_next"
    ((p gsl-permutation-ptr))
  :int)

(defmethod next ((o gsl-permutation))
  (let ((status (gsl-permutation-next (ptr o))))
    (values o status)))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_permutation_prev"
    ((p gsl-permutation-ptr))
  :int)

(defmethod prev ((o gsl-permutation))
  (let ((status (gsl-permutation-prev (ptr o))))
    (values o status)))

;; ----------------------------------------------------------------------

(defun-foreign "wrap_gsl_permutation_fwrite"
    ((fn :cstring)
     (p gsl-permutation-ptr))
  :int)

(defmethod write-to-binary-file (file-name (o gsl-permutation))
  (let ((status))
    (uffi:with-cstring (c-file-name file-name)
      (setq status
            (wrap-gsl-permutation-fwrite c-file-name (ptr o))))
    status))

;; ----------------------------------------------------------------------

(defun-foreign "wrap_gsl_permutation_fread"
    ((fn :cstring)
     (p gsl-permutation-ptr))
  :int)

(defmethod read-from-binary-file ((o gsl-permutation) file-name)
  (let ((status))
    (uffi:with-cstring (c-file-name file-name)
      (setq status
            (wrap-gsl-permutation-fread c-file-name (ptr o))))
    (values o status)))

;; ----------------------------------------------------------------------

(defun-foreign "wrap_gsl_permutation_fprintf"
    ((fn :cstring)
     (p gsl-permutation-ptr))
  :int)

(defmethod write-to-file (file-name (o gsl-permutation))
  (let ((status))
    (uffi:with-cstring (c-file-name file-name)
      (setq status
            (wrap-gsl-permutation-fprintf c-file-name (ptr o))))
    status))

;; ----------------------------------------------------------------------

(defun-foreign "wrap_gsl_permutation_fscanf"
    ((fn :cstring)
     (p gsl-permutation-ptr))
  :int)

(defmethod read-from-file ((o gsl-permutation) file-name)
  (let ((status))
    (uffi:with-cstring (c-file-name file-name)
      (setq status
            (wrap-gsl-permutation-fscanf c-file-name (ptr o))))
    (values o status)))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_permutation_inverse"
    ((inv gsl-permutation-ptr)
     (p gsl-permutation-ptr))
  :int)

(defmethod inverse ((p gsl-permutation))
  (let* ((inv (make-permutation (size p)))
         (status (gsl-permutation-inverse (ptr inv) (ptr p))))
    (values inv status)))

(defmacro with-permutation-inverse ((p-inv p-src) &body body)
  `(let ((,p-inv (inverse ,p-src)))
     (unwind-protect
          ,@body
       (free ,p-inv))))

;; ----------------------------------------------------------------------

(defmacro def-vector-permutation-type-funcs% (typ)
  (destructuring-bind (type-ptr type-string)
      (cond
        ((eq typ 'double-float)
         (list 'gsl-vector-ptr ""))
        ((eq typ 'single-float)
         (list 'gsl-vector-float-ptr "_float"))
        ((eq typ 'integer)
         (list 'gsl-vector-int-ptr "_int"))
        ((equal typ '(complex (double-float)))
         (list 'gsl-vector-complex-ptr "_complex"))
        ((equal typ '(complex (single-float)))
         (list 'gsl-vector-complex-float-ptr "_complex_float"))
        (t
         (error "no matching type.")))

    `(progn
       (defun-foreign ,(concatenate 'string "gsl_permute_vector" type-string)
           ((p gsl-permutation-ptr)
            (v ,type-ptr))
         :int)

       (defun-foreign ,(concatenate 'string
                                    "gsl_permute_vector" type-string "_inverse")
           ((p gsl-permutation-ptr)
            (v ,type-ptr))
         :int))))

(def-vector-permutation-type-funcs% double-float)
(def-vector-permutation-type-funcs% single-float)
(def-vector-permutation-type-funcs% integer)
(def-vector-permutation-type-funcs% (complex (double-float)))
(def-vector-permutation-type-funcs% (complex (single-float)))

(defmacro def-vector-methods% (class-string func-string)
  (let ((class-object (kmrcl:concat-symbol "gsl-vector-" class-string)))
    `(progn

       (defmethod permute-vector ((p gsl-permutation) (v ,class-object))
         (let ((status (,(kmrcl:concat-symbol "gsl-permute-vector" func-string)
                         (ptr p) (ptr v))))
           (values v status)))

       (defmethod permute-vector-inverse ((p gsl-permutation)
                                          (v ,class-object))
         (let ((status (,(kmrcl:concat-symbol
                          "gsl-permute-vector" func-string "-inverse")
                         (ptr p) (ptr v))))
           (values v status)))
       )))

(def-vector-methods% "integer" "-int")
(def-vector-methods% "single-float" "-float")
(def-vector-methods% "double-float" "")
(def-vector-methods% "complex-single-float" "-complex-float")
(def-vector-methods% "complex-double-float" "-complex")

;; ----------------------------------------------------------------------

(defun make-permutation (size &key initial-contents from-file from-binary-file)
  (assert (and (typep size 'integer) (> size 0)))
  (let ((p (make-instance 'gsl-permutation :size size)))
    (alloc p)
    (cond
      ((and initial-contents from-file from-binary-file)
       (error "can only define one of the keys: initial-contents, from-file, from-binary-file."))
      (initial-contents
       (cond
         ((listp initial-contents)
          (do ((x initial-contents (cdr x))
               (i 0 (1+ i)))
              ((= i size))
            (set-element p i (car x)))
          (unless (isvalid p)
            (error "intitial contents are not a valid permutation.")))
         ((vectorp initial-contents)
          (do ((i 0 (1+ i)))
              ((= i size))
            (set-element p i (aref initial-contents i)))
          (unless (isvalid p)
            (error "intitial contents are not a valid permutation.")))
         (t
          (error "initial-contents must be either a list or a vector."))))
      (from-file
       (read-from-file p from-file)
       (unless (isvalid p)
         (error "file contents are not a valid permutation.")))
      (from-binary-file
       (read-from-binary-file p from-binary-file)
       (unless (isvalid p)
         (error "file contents are not a valid permutation.")))
      (t
       (permutation-init p)))
    p))

(defmacro with-permutation ((p size &key initial-contents from-file
                               from-binary-file)
                            &body body)
  `(let ((,p (make-permutation ,size :initial-contents ,initial-contents
                               :from-file ,from-file
                               :from-binary-file ,from-binary-file)))
     (unwind-protect
          (progn ,@body)
       (free ,p))))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_permutation_memcpy"
    ((p-dest gsl-permutation-ptr)
     (p-src gsl-permutation-ptr))
  :int)

(defmethod copy ((o gsl-permutation))
  (let* ((p-copy (make-permutation (size o)))
         (status (gsl-permutation-memcpy (ptr p-copy) (ptr o))))
    (values p-copy status)))

(defmacro with-permutation-copy ((p-dest p-src) &body body)
  `(let ((,p-dest (copy ,p-src)))
     (unwind-protect
          ,@body
       (free ,p-dest))))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_permutation_mul"
    ((p gsl-permutation-ptr)
     (pa gsl-permutation-ptr)
     (pb gsl-permutation-ptr))
  :int)

(defmethod mul ((pa gsl-permutation) (pb gsl-permutation))
  (let* ((p (make-permutation (size pa)))
         (status (gsl-permutation-mul (ptr p) (ptr pa) (ptr pb))))
    (values p status)))

(defmacro with-permutation-mul ((p-new pa pb) &body body)
  `(let ((,p-new (mul ,pa ,pb)))
     (unwind-protect
          ,@body
       (free ,p-new))))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_permutation_linear_to_canonical"
    ((q gsl-permutation-ptr)
     (p gsl-permutation-ptr))
  :int)

(defmethod linear->canonical ((p-can gsl-permutation) (p-lin gsl-permutation))
  (let ((status (gsl-permutation-linear-to-canonical (ptr p-can) (ptr p-lin))))
    (values p-can status)))

(defmacro with-permutation-linear->canonical ((p-can p-lin) &body body)
  (let ((p (gensym)))
    `(let* ((,p ,p-lin)
            (,p-can (make-permutation (size ,p))))
     (linear->canonical ,p-can ,p)
     (unwind-protect
          ,@body
       (free ,p-can)))))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_permutation_canonical_to_linear"
    ((p gsl-permutation-ptr)
     (q gsl-permutation-ptr))
  :int)

(defmethod canonical->linear ((p-lin gsl-permutation) (p-can gsl-permutation))
  (let ((status (gsl-permutation-canonical-to-linear (ptr p-lin) (ptr p-can))))
    (values p-lin status)))

(defmacro with-permutation-canonical->linear ((p-lin p-can) &body body)
  (let ((p (gensym)))
    `(let* ((,p ,p-can)
            (,p-lin (make-permutation (size ,p))))
       (canonical->linear ,p-lin ,p)
       (unwind-protect
            ,@body
         (free ,p-lin)))))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_permutation_inversions"
    ((p gsl-permutation-ptr))
  size-t)

(defmethod inversions ((o gsl-permutation))
  (gsl-permutation-inversions (ptr o)))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_permutation_linear_cycles"
    ((p gsl-permutation-ptr))
  size-t)

(defmethod linear-cycles ((o gsl-permutation))
  (gsl-permutation-linear-cycles (ptr o)))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_permutation_canonical_cycles"
    ((p gsl-permutation-ptr))
  size-t)

(defmethod canonical-cycles ((o gsl-permutation))
  (gsl-permutation-linear-cycles (ptr o)))

;; ----------------------------------------------------------------------

(defmethod set-element ((p gsl-permutation) i &optional x dummy)
  (assert (typep x 'integer))
  (assert (and (typep i 'integer) (>= i 0) (< i (size p))))
  (let ((data-ptr (uffi:get-slot-pointer (ptr p) '(* size-t) 'cl-gsl::data)))
    (setf (uffi:deref-array data-ptr 'size-t i) x)))
