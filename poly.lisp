;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl-poly)

(defun-foreign "gsl_poly_eval"
    ((c (* :double))
     (len :int)
     (x :double))
  :double)

(defun poly-eval (coefficients x)
  "Returns the value of the polynomial
c[0] + c[1] X + c[2] X^2 + ... + c[n-1] X^{n-1}
where COEFFICIENTS is a vector of the coefficients of length n."
  (with-lisp-vec->c-array (c-ptr coefficients)
    (gsl-poly-eval c-ptr (length coefficients) x)))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_poly_solve_quadratic"
    ((a :double)
     (b :double)
     (c :double)
     (x0 double-ptr)
     (x1 double-ptr))
  :int)

(defun solve-quadratic (a b c)
  "Computes the real roots of the quadratic equation A x^2 + B x + C = 0.
Returns three values. The first two values are the real roots of the equation.
The third value is the number of roots (either 2 or 0).
If there are 0 real roots, the first two values are 0.0d0. When there are
2 real roots, their values are returned in ascending order."
  (declare (double-float a) (double-float b) (double-float c))
  (uffi:with-foreign-object (x0-ptr :double)
    (uffi:with-foreign-object (x1-ptr :double)
      (setf (uffi:deref-pointer x0-ptr :double) 0.0d0)
      (setf (uffi:deref-pointer x1-ptr :double) 0.0d0)
      (let ((num-roots (gsl-poly-solve-quadratic a b c x0-ptr x1-ptr)))
        (values (uffi:deref-pointer x0-ptr :double)
                (uffi:deref-pointer x1-ptr :double)
                num-roots)))))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_poly_solve_cubic"
    ((a :double)
     (b :double)
     (c :double)
     (x0 double-ptr)
     (x1 double-ptr)
     (x2 double-ptr))
  :int)

(defun solve-cubic (a b c)
  "Computes the real roots of the cubic equation, x^3 + A x^2 + B x + C = 0
with a leading coefficient of unity.
Returns four values. The first 3 values are the real roots of the equation.
The fourth value is the number of real roots (either 1 or 3).
If 1 real root is found, the other two roots are 0.0d0. When 3 real
roots are found, they are returned in ascending order."
  (declare (double-float a) (double-float b) (double-float c))
  (uffi:with-foreign-object (x0-ptr :double)
    (uffi:with-foreign-object (x1-ptr :double)
      (uffi:with-foreign-object (x2-ptr :double)
        (setf (uffi:deref-pointer x0-ptr :double) 0.0d0)
        (setf (uffi:deref-pointer x1-ptr :double) 0.0d0)
        (setf (uffi:deref-pointer x2-ptr :double) 0.0d0)
        (let ((num-roots (gsl-poly-solve-cubic a b c x0-ptr x1-ptr x2-ptr)))
          (values (uffi:deref-pointer x0-ptr :double)
                  (uffi:deref-pointer x1-ptr :double)
                  (uffi:deref-pointer x2-ptr :double)
                  num-roots))))))


;; ----------------------------------------------------------------------

(defun-foreign "gsl_poly_complex_solve_quadratic"
    ((a :double)
     (b :double)
     (c :double)
     (z0 gsl-complex-ptr)
     (z1 gsl-complex-ptr))
  :int)

(defun complex-solve-quadratic (a b c)
  (declare (double-float a) (double-float b) (double-float c))
  (uffi:with-foreign-object (z0-ptr 'gsl-complex)
    (uffi:with-foreign-object (z1-ptr 'gsl-complex)
      (let ((num-roots (gsl-poly-complex-solve-quadratic a b c z0-ptr z1-ptr)))
        (values (gsl-complex->complex (uffi:deref-pointer z0-ptr 'gsl-complex))
                (gsl-complex->complex (uffi:deref-pointer z1-ptr 'gsl-complex))
                num-roots)))))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_poly_complex_solve_cubic"
    ((a :double)
     (b :double)
     (c :double)
     (z0 gsl-complex-ptr)
     (z1 gsl-complex-ptr)
     (z2 gsl-complex-ptr))
  :int)

(defun complex-solve-cubic (a b c)
  (declare (double-float a) (double-float b) (double-float c))
  (uffi:with-foreign-object (z0-ptr 'gsl-complex)
    (uffi:with-foreign-object (z1-ptr 'gsl-complex)
      (uffi:with-foreign-object (z2-ptr 'gsl-complex)
        (let ((num-roots (gsl-poly-complex-solve-cubic a b c
                                                       z0-ptr z1-ptr z2-ptr)))
          (values (gsl-complex->complex (uffi:deref-pointer z0-ptr 'gsl-complex))
                  (gsl-complex->complex (uffi:deref-pointer z1-ptr 'gsl-complex))
                  (gsl-complex->complex (uffi:deref-pointer z2-ptr 'gsl-complex))
                  num-roots))))))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_poly_complex_workspace_alloc"
    ((size :unsigned-long))
  gsl-poly-complex-workspace-ptr)

(defun-foreign "gsl_poly_complex_workspace_free"
    ((w gsl-poly-complex-workspace-ptr))
  :void)

(defun-foreign "gsl_poly_complex_solve"
    ((a double-ptr)
     (size :unsigned-long)
     (w gsl-poly-complex-workspace-ptr)
     (z gsl-complex-packed-ptr))
  :int)

(defun complex-solve (a)
  (with-lisp-vec->c-array (a-ptr a)
    (let* ((len (length a))
           (w (gsl-poly-complex-workspace-alloc len))
           (z-ptr (uffi:allocate-foreign-object :double (* 2 (1- len))))
           (ret-val (gsl-poly-complex-solve a-ptr len w z-ptr)))
      (gsl-poly-complex-workspace-free w)
      (multiple-value-prog1
          (values (complex-packed-array->lisp-vec z-ptr (* 2 (1- len))) ret-val)
        (uffi:free-foreign-object z-ptr)))))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_poly_dd_init"
    ((dd double-ptr)
     (xa double-ptr)
     (ya double-ptr)
     (size :unsigned-long))
  :int)


(defun dd-init (xa ya)
  "Computes a divided-difference representation of the interpolating polynomial
for the points (xa, ya) stored in the vectors XA and YA of equal length.
Returns two values: the divided differences as a vector of length equal to XA,
and the status, indicating the success of the computation."
  (with-lisp-vec->c-array (xa-ptr xa)
    (with-lisp-vec->c-array (ya-ptr ya)
      (let* ((len (length xa))
             (dd-ptr (uffi:allocate-foreign-object :double len))
             (ret-val (gsl-poly-dd-init dd-ptr xa-ptr ya-ptr len)))
        (multiple-value-prog1
            (values (c-array->lisp-vec dd-ptr len) ret-val)
          (uffi:free-foreign-object dd-ptr))))))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_poly_dd_eval"
    ((dd double-ptr)
     (xa double-ptr)
     (size :unsigned-long)
     (x :double))
  :double)


(defun dd-eval (dd xa x)
  "Returns the value of the polynomial at point X. The vectors DD and XA,
of equal length, store the divided difference representation of the polynomial."
  (with-lisp-vec->c-array (dd-ptr dd)
    (with-lisp-vec->c-array (xa-ptr xa)
      (gsl-poly-dd-eval dd-ptr xa-ptr (length dd) x))))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_poly_dd_taylor"
    ((c double-ptr)
     (xp :double)
     (dd double-ptr)
     (xa double-ptr)
     (size :unsigned-long)
     (w double-ptr))
  :int)


(defun dd-taylor (xp dd xa)
  "Converts the divided-difference representation of a polynomial to
a Taylor expansion. The divided-difference representation is supplied in the
vectors DD and XA of equal length. Returns a vector of the Taylor coefficients
of the polynomial expanded about the point XP."
  (with-lisp-vec->c-array (dd-ptr dd)
    (with-lisp-vec->c-array (xa-ptr xa)
      (let* ((len (length dd))
             (w-ptr (uffi:allocate-foreign-object :double len))
             (c-ptr (uffi:allocate-foreign-object :double len))
             (ret-val (gsl-poly-dd-taylor c-ptr xp dd-ptr xa-ptr len w-ptr)))
        (multiple-value-prog1
            (values (c-array->lisp-vec c-ptr len) ret-val)
          (uffi:free-foreign-object w-ptr)
          (uffi:free-foreign-object c-ptr))))))
