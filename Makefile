#### -*- Mode: Makefile -*-
#### Author: Edgar Denny <edgardenny@comcast.net>
#### Main GNU Makefile

DESTDIR=/usr/local

all: Makefile.opts
	(cd c; ${MAKE})

clean: Makefile.opts
	(cd build; ${MAKE} clean)
	$(RM) Makefile.opts

lispclean:
	find . -name '*.x86f' | xargs rm -f
	find . -name '*.fasla16' | xargs rm -f
	find . -name '*.fasl' | xargs rm -f
	find . -name '*.ufsl' | xargs rm -f

Makefile.opts:
	(sh select_platform.sh)

install_cwrapper:
	(cd build; ${MAKE} install_cwrapper DESTDIR=$(DESTDIR))

install: install_cwrapper
