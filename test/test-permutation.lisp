;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl-test)

(defconstant +permutation+ "permutation")

(defconstant +perm-vector-init+ (vector 0 1 2 3 4))

(defconstant +perm-vector-next+ (vector 0 1 2 4 3))

(defconstant +perm-vector+ (vector 3 4 0 2 1))

(defconstant +inversion-vector+
  (vector 0 1 1 2 2 3 1 2 2 3
          3 4 2 3 3 4 4 5 3 4
          4 5 5 6 1 2 2 3 3 4
          2 3 3 4 4 5 3 4 4 5
          5 6 4 5 5 6 6 7 2 3
          3 4 4 5 3 4 4 5 5 6
          4 5 5 6 6 7 5 6 6 7
          7 8 3 4 4 5 5 6 4 5
          5 6 6 7 5 6 6 7 7 8
          6 7 7 8 8 9 4 5 5 6
          6 7 5 6 6 7 7 8 6 7
          7 8 8 9 7 8 8 9 9 10))

(defconstant +cycles-vector+
  (vector 5 4 4 3 3 4 4 3 3 2
          2 3 3 2 4 3 3 2 2 3
          3 4 2 3 4 3 3 2 2 3
          3 2 2 1 1 2 2 1 3 2
          2 1 1 2 2 3 1 2 3 2
          2 1 1 2 4 3 3 2 2 3
          3 2 2 1 1 2 2 3 1 2
          2 1 2 1 3 2 2 1 3 2
          4 3 3 2 2 1 3 2 2 1
          1 2 2 1 3 2 1 2 2 3
          1 2 2 3 3 4 2 3 1 2
          2 3 1 2 2 1 1 2 2 3))

(deftest "make-permutation-init" :category +permutation+
         :test-fn
         #'(lambda ()
             (let ((p (gsl-array:make-permutation 5)))
               (prog1
                   (and (= (gsl-array:size p) 5)
                        (equalp (gsl-array:gsl->lisp-vector p)
                                +perm-vector-init+))
                 (gsl-array:free p)))))


(deftest "make-permutation-initial-contents" :category +permutation+
         :test-fn
         #'(lambda ()
             (let ((p (gsl-array:make-permutation
                       5 :initial-contents +perm-vector+)))
               (prog1
                   (equalp (gsl-array:gsl->lisp-vector p) +perm-vector+)
                 (gsl-array:free p)))))


(deftest "valid-t" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation (p 5 :initial-contents +perm-vector+)
               (gsl-array:isvalid p))))


(deftest "valid-nil" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation (p 5 :initial-contents +perm-vector+)
               (progn
                 (gsl-array:set-element p 0 0)
                 (not (gsl-array:isvalid p))))))


(deftest "read-write" :category +permutation+
         :test-fn
         #'(lambda ()
             (let ((v1 +perm-vector+))
               (gsl-array:with-permutation (p1 5 :initial-contents v1)
                 (progn
                   (gsl-array:write-to-file "/tmp/test.txt" p1)
                   (gsl-array:with-permutation (p2 5 :from-file "/tmp/test.txt")
                     (and (equalp v1 (gsl-array:gsl->lisp-vector p1))
                          (equalp v1 (gsl-array:gsl->lisp-vector p2)))))))))


(deftest "read-write-binary" :category +permutation+
         :test-fn
         #'(lambda ()
             (let ((v1 +perm-vector+))
               (gsl-array:with-permutation (p1 5 :initial-contents v1)
                 (progn
                   (gsl-array:write-to-binary-file "/tmp/test.bin" p1)
                   (gsl-array:with-permutation
                       (p2 5 :from-binary-file "/tmp/test.bin")
                     (and (equalp v1 (gsl-array:gsl->lisp-vector p1))
                          (equalp v1 (gsl-array:gsl->lisp-vector p2)))))))))


(deftest "get-element" :category +permutation+
         :test-fn
         #'(lambda ()
             (let ((v1 +perm-vector+))
               (gsl-array:with-permutation (p1 5 :initial-contents v1)
                 (dotimes (i 5 t)
                   (unless (= (gsl-array:get-element p1 i) (aref v1 i))
                     (return nil)))))))


(deftest "swap-elements" :category +permutation+
         :test-fn
         #'(lambda ()
             (let ((v1 +perm-vector+))
               (gsl-array:with-permutation (p1 5 :initial-contents v1)
                 (progn
                   (gsl-array:swap-elements p1 1 3)
                   (dotimes (i 5 t)
                     (cond
                       ((= i 1)
                        (unless (= (gsl-array:get-element p1 i) (aref v1 3))
                          (return nil)))
                       ((= i 3)
                        (unless (= (gsl-array:get-element p1 i) (aref v1 1))
                          (return nil)))
                       (t
                        (unless (= (gsl-array:get-element p1 i) (aref v1 i))
                          (return nil))))))))))


(deftest "reverse-permutation" :category +permutation+
         :test-fn
         #'(lambda ()
             (let ((v1 +perm-vector+))
               (gsl-array:with-permutation (p1 5 :initial-contents v1)
                 (progn
                   (gsl-array:reverse-permutation p1)
                   (dotimes (i 5 t)
                     (unless (= (gsl-array:get-element p1 i) (aref v1 (- 4 i)))
                       (return nil))))))))

(deftest "next" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation (p1 5)
               (gsl-array:with-permutation
                   (p2 5 :initial-contents +perm-vector-next+)
                 (let ((p3 (gsl-array:next p1)))
                   (equalp (gsl-array:gsl->lisp-vector p2)
                           (gsl-array:gsl->lisp-vector p3)))))))

(deftest "prev" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation
                 (p1 5 :initial-contents +perm-vector-next+)
               (gsl-array:with-permutation (p2 5)
                 (let ((p3 (gsl-array:prev p1)))
                   (equalp (gsl-array:gsl->lisp-vector p2)
                           (gsl-array:gsl->lisp-vector p3)))))))


(deftest "inverse" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation (p1 5 :initial-contents +perm-vector+)
               (gsl-array:with-permutation-inverse (p2 p1)
                 (dotimes (i 5 t)
                   (unless (= (gsl-array:get-element
                               p1 (gsl-array:get-element p2 i))
                              i)
                     (return nil)))))))


;; ----------------------------------------------------------------------

(deftest "permute-vector-integer" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation (p1 5 :initial-contents +perm-vector+)
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer
                       :initial-contents (vector 10 11 12 13 14))
                 (gsl-array:with-vector-copy (v2 v1)
                   (progn
                     (gsl-array:permute-vector p1 v1)
                     (dotimes (i 5 t)
                       (unless (= (gsl-array:get-element
                                   v2 (gsl-array:get-element p1 i))
                                  (gsl-array:get-element v1 i))
                         (return nil)))))))))

(deftest "permute-vector-double-float" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation (p1 5 :initial-contents +perm-vector+)
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float
                       :initial-contents
                       (vector 10.0d0 11.0d0 12.0d0 13.0d0 14.0d0))
                 (gsl-array:with-vector-copy (v2 v1)
                   (progn
                     (gsl-array:permute-vector p1 v1)
                     (dotimes (i 5 t)
                       (unless (= (gsl-array:get-element
                                   v2 (gsl-array:get-element p1 i))
                                  (gsl-array:get-element v1 i))
                         (return nil)))))))))

(deftest "permute-vector-single-float" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation (p1 5 :initial-contents +perm-vector+)
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float
                       :initial-contents (vector 10.0 11.0 12.0 13.0 14.0))
                 (gsl-array:with-vector-copy (v2 v1)
                   (progn
                     (gsl-array:permute-vector p1 v1)
                     (dotimes (i 5 t)
                       (unless (= (gsl-array:get-element
                                   v2 (gsl-array:get-element p1 i))
                                  (gsl-array:get-element v1 i))
                         (return nil)))))))))


(deftest "permute-vector-complex-double-float" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation (p1 5 :initial-contents +perm-vector+)
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (double-float))
                       :initial-contents
                       (vector (complex 10.0d0 1.0d0) (complex 11.0d0 2.0d0)
                               (complex 12.0d0 3.0d0) (complex 13.0d0 4.0d0)
                               (complex 14.0d0 5.0d0)))
                 (gsl-array:with-vector-copy (v2 v1)
                   (progn
                     (gsl-array:permute-vector p1 v1)
                     (dotimes (i 5 t)
                       (unless (= (gsl-array:get-element
                                   v2 (gsl-array:get-element p1 i))
                                  (gsl-array:get-element v1 i))
                         (return nil)))))))))

(deftest "permute-vector-complex-single-float" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation (p1 5 :initial-contents +perm-vector+)
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (single-float))
                       :initial-contents
                       (vector (complex 10.0 1.0) (complex 11.0 2.0)
                               (complex 12.0 3.0) (complex 13.0 4.0)
                               (complex 14.0 5.0)))
                 (gsl-array:with-vector-copy (v2 v1)
                   (progn
                     (gsl-array:permute-vector p1 v1)
                     (dotimes (i 5 t)
                       (unless (= (gsl-array:get-element
                                   v2 (gsl-array:get-element p1 i))
                                  (gsl-array:get-element v1 i))
                         (return nil)))))))))

;; ----------------------------------------------------------------------

(deftest "permute-vector-inverse-integer" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation (p1 5 :initial-contents +perm-vector+)
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer
                       :initial-contents (vector 10 11 12 13 14))
                 (gsl-array:with-vector-copy (v2 v1)
                   (progn
                     (gsl-array:permute-vector-inverse p1 v1)
                     (dotimes (i 5 t)
                       (unless (= (gsl-array:get-element
                                   v1 (gsl-array:get-element p1 i))
                                  (gsl-array:get-element v2 i))
                         (return nil)))))))))

(deftest "permute-vector-inverse-double-float" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation (p1 5 :initial-contents +perm-vector+)
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float
                       :initial-contents
                       (vector 10.0d0 11.0d0 12.0d0 13.0d0 14.0d0))
                 (gsl-array:with-vector-copy (v2 v1)
                   (progn
                     (gsl-array:permute-vector-inverse p1 v1)
                     (dotimes (i 5 t)
                       (unless (= (gsl-array:get-element
                                   v1 (gsl-array:get-element p1 i))
                                  (gsl-array:get-element v2 i))
                         (return nil)))))))))

(deftest "permute-vector-inverse-single-float" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation (p1 5 :initial-contents +perm-vector+)
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float
                       :initial-contents (vector 10.0 11.0 12.0 13.0 14.0))
                 (gsl-array:with-vector-copy (v2 v1)
                   (progn
                     (gsl-array:permute-vector-inverse p1 v1)
                     (dotimes (i 5 t)
                       (unless (= (gsl-array:get-element
                                   v1 (gsl-array:get-element p1 i))
                                  (gsl-array:get-element v2 i))
                         (return nil)))))))))


(deftest "permute-vector-inverse-complex-double-float" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation (p1 5 :initial-contents +perm-vector+)
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (double-float))
                       :initial-contents
                       (vector (complex 10.0d0 1.0d0) (complex 11.0d0 2.0d0)
                               (complex 12.0d0 3.0d0) (complex 13.0d0 4.0d0)
                               (complex 14.0d0 5.0d0)))
                 (gsl-array:with-vector-copy (v2 v1)
                   (progn
                     (gsl-array:permute-vector-inverse p1 v1)
                     (dotimes (i 5 t)
                       (unless (= (gsl-array:get-element
                                   v1 (gsl-array:get-element p1 i))
                                  (gsl-array:get-element v2 i))
                         (return nil)))))))))

(deftest "permute-vector-inverse-complex-single-float" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation (p1 5 :initial-contents +perm-vector+)
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (single-float))
                       :initial-contents
                       (vector (complex 10.0 1.0) (complex 11.0 2.0)
                               (complex 12.0 3.0) (complex 13.0 4.0)
                               (complex 14.0 5.0)))
                 (gsl-array:with-vector-copy (v2 v1)
                   (progn
                     (gsl-array:permute-vector-inverse p1 v1)
                     (dotimes (i 5 t)
                       (unless (= (gsl-array:get-element
                                   v1 (gsl-array:get-element p1 i))
                                  (gsl-array:get-element v2 i))
                         (return nil)))))))))

;; ----------------------------------------------------------------------

(deftest "copy" :category +permutation+
         :test-fn
         #'(lambda ()
             (let ((v1 +perm-vector+))
               (gsl-array:with-permutation (p1 5 :initial-contents v1)
                 (gsl-array:with-permutation-copy (p2 p1)
                   (and (equalp v1 (gsl-array:gsl->lisp-vector p1))
                        (equalp v1 (gsl-array:gsl->lisp-vector p2))))))))


(deftest "mul" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation (p1 5 :initial-contents +perm-vector+)
               (gsl-array:with-permutation
                   (p2 5 :initial-contents +perm-vector-next+)
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer
                       :initial-contents (vector 10 11 12 13 14))
                 (gsl-array:with-vector-copy (v2 v1)
                   (progn
                     (gsl-array:permute-vector p1 v1)
                     (gsl-array:permute-vector p2 v1)
                     (gsl-array:with-permutation-mul (p3 p2 p1)
                       (progn
                         (gsl-array:permute-vector p3 v2)
                         (and (equalp (gsl-array:gsl->lisp-vector v1)
                                      (gsl-array:gsl->lisp-vector v2))))))))))))

(deftest "linear-to-canonical" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation
                 (p1 5 :initial-contents (vector 2 4 3 0 1))
               (gsl-array:with-permutation-linear->canonical (p2 p1)
                   (and (equalp (gsl-array:gsl->lisp-vector p2)
                                (vector 1 4 0 2 3)))))))

(deftest "canonical-to-linear" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation
                 (p1 5 :initial-contents (vector 1 4 0 2 3))
               (gsl-array:with-permutation-canonical->linear (p2 p1)
                   (and (equalp (gsl-array:gsl->lisp-vector p2)
                                (vector 2 4 3 0 1)))))))


(deftest "inversions" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation (p1 5)
               (do ((j 0 (1+ j)))
                   ((> j 119) nil)
                 (let ((i (gsl-array:inversions p1))
                       (status (nth-value 1 (gsl-array:next p1))))
                   (when (/= i (aref +inversion-vector+ j))
                     (return nil))
                   (when (/= status gsl:+success+)
                     (if (/= j 119)
                         (return nil)
                         (return t))))))))


(deftest "cycles" :category +permutation+
         :test-fn
         #'(lambda ()
             (gsl-array:with-permutation (p-lin 5)
               (gsl-array:with-permutation (p-can 5)
               (do ((j 0 (1+ j)))
                   ((> j 119) nil)
                 (gsl-array:linear->canonical p-can p-lin)
                 (when (/= (gsl-array:canonical-cycles p-can)
                           (gsl-array:linear-cycles p-lin)
                           (aref +cycles-vector+ j))
                   (return nil))
                 (when (/= (nth-value 1 (gsl-array:next p-lin))
                           gsl:+success+)
                   (if (/= j 119)
                       (return nil)
                       (return t))))))))
