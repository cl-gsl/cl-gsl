;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl-test)

(defconstant +sort+ "sort")

(defconstant +vector-unsorted-double-float+
  (vector 6.0d0 3.0d0 1.0d0 5.0d0 7.0d0))

(defconstant +vector-sorted-double-float+
  (vector 1.0d0 3.0d0 5.0d0 6.0d0 7.0d0))

(defconstant +vector-unsorted-single-float+
  (vector 6.0 3.0 1.0 5.0 7.0))

(defconstant +vector-sorted-single-float+
  (vector 1.0 3.0 5.0 6.0 7.0))

(defconstant +vector-unsorted-integer+
  (vector 6 3 1 5 7))

(defconstant +vector-sorted-integer+
  (vector 1 3 5 6 7))

(defconstant +vector-sorted-permutation+
  (vector 2 1 3 0 4))

(deftest "sort-vector-double-float" :category +sort+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector
                 (v1 5 :element-type 'double-float
                     :initial-contents +vector-unsorted-double-float+)
               (equalp (gsl-array:gsl->lisp-vector (gsl-array:sort-vector v1))
                       +vector-sorted-double-float+))))

(deftest "sort-vector-single-float" :category +sort+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector
                 (v1 5 :element-type 'single-float
                     :initial-contents +vector-unsorted-single-float+)
               (equalp (gsl-array:gsl->lisp-vector (gsl-array:sort-vector v1))
                       +vector-sorted-single-float+))))

(deftest "sort-vector-integer" :category +sort+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector
                 (v1 5 :element-type 'integer
                     :initial-contents +vector-unsorted-integer+)
               (equalp (gsl-array:gsl->lisp-vector (gsl-array:sort-vector v1))
                       +vector-sorted-integer+))))

(deftest "sort-vector-index-double-float" :category +sort+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector
                 (v1 5 :element-type 'double-float
                     :initial-contents +vector-unsorted-double-float+)
               (gsl-array:with-sort-vector-index (p1 v1)
                 (equalp (gsl-array:gsl->lisp-vector p1)
                         +vector-sorted-permutation+)))))

(deftest "sort-vector-index-single-float" :category +sort+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector
                 (v1 5 :element-type 'single-float
                     :initial-contents +vector-unsorted-single-float+)
               (gsl-array:with-sort-vector-index (p1 v1)
                 (equalp (gsl-array:gsl->lisp-vector p1)
                         +vector-sorted-permutation+)))))

(deftest "sort-vector-index-integer" :category +sort+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector
                 (v1 5 :element-type 'integer
                     :initial-contents +vector-unsorted-integer+)
               (gsl-array:with-sort-vector-index (p1 v1)
                 (equalp (gsl-array:gsl->lisp-vector p1)
                         +vector-sorted-permutation+)))))
