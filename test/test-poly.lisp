;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl-test)

(defconstant +poly+ "poly")

(deftest "poly-eval" :category +poly+
         :test-fn
         #'(lambda ()
             (tol< (gsl-poly:poly-eval (vector 1.0d0 0.5d0 0.3d0) 0.5d0)
                   (+ 1.0d0 (* 0.5d0 0.5d0) (* 0.3d0 0.5d0 0.5d0))
                   +tol+2+)))

(deftest "solve-quadratic-no-roots" :category +poly+
         :test-fn
         #'(lambda () (multiple-value-bind (r1 r2 n)
                          (gsl-poly:solve-quadratic 4.0d0 -20.0d0 26.0d0)
                        (declare (ignore r1 r2))
                        (= n 0))))

(deftest "solve-quadratic-one-root" :category +poly+
         :test-fn #'(lambda ()
                      (multiple-value-bind (r1 r2 n)
                          (gsl-poly:solve-quadratic 4.0d0 -20.0d0 25.0d0)
                        (and (= n 2)
                             (tol< r1 2.5d0 1.0d-9)
                             (tol< r2 2.5d0 1.0d-9)))))

(deftest "solve-quadratic-two-roots" :category +poly+
         :test-fn #'(lambda ()
                      (multiple-value-bind (r1 r2 n)
                          (gsl-poly:solve-quadratic 4.0d0 -20.0d0 21.0d0)
                        (and (= n 2)
                             (tol< r1 1.5d0 1.0d-9)
                             (tol< r2 3.5d0 1.0d-9)))))

(deftest "solve-cubic-one-root" :category +poly+
         :test-fn #'(lambda ()
                      (multiple-value-bind (r1 r2 r3 n)
                          (gsl-poly:solve-cubic 0.0d0 0.0d0 27.0d0)
                        (declare (ignore r2 r3))
                        (and (= n 1)
                             (tol< r1 -3.0d0 1.0d-9)))))

(deftest "solve-cubic-three-roots" :category +poly+
         :test-fn #'(lambda ()
                      (multiple-value-bind (r1 r2 r3 n)
                          (gsl-poly:solve-cubic -143.0d0 5087.0d0 -50065.0d0)
                        (and (= n 3)
                             (tol< r1 17.0d0 1.0d-9)
                             (tol< r2 31.0d0 1.0d-9)
                             (tol< r3 95.0d0 1.0d-9)))))

(deftest "complex-solve-quadratic-two-roots" :category +poly+
         :test-fn
         #'(lambda ()
             (multiple-value-bind (r1 r2 n)
                 (gsl-poly:complex-solve-quadratic 4.0d0 -20.0d0 26.0d0)
               (and (= n 2)
                    (tol< (realpart r1) 2.5d0 1.0d-7)
                    (tol< (imagpart r1) -0.5d0 1.0d-7)
                    (tol< (realpart r2) 2.5d0 1.0d-7)
                    (tol< (imagpart r2) 0.5d0 1.0d-7)))))

(deftest "complex-solve-quadratic-one-root" :category +poly+
         :test-fn
         #'(lambda ()
             (multiple-value-bind (r1 r2 n)
                 (gsl-poly:complex-solve-quadratic 4.0d0 -20.0d0 25.0d0)
               (and (= n 2)
                    (tol< (realpart r1) 2.5d0 1.0d-7)
                    (tol< (imagpart r1) 0.0d0 1.0d-7)
                    (tol< (realpart r2) 2.5d0 1.0d-7)
                    (tol< (imagpart r2) 0.0d0 1.0d-7)))))

(deftest "complex-solve-cubic-three-roots" :category +poly+
         :test-fn
         #'(lambda ()
             (multiple-value-bind (r1 r2 r3 n)
                 (gsl-poly:complex-solve-cubic 0.0d0 0.0d0 -27.0d0)
               (and (= n 3)
                    (tol< (realpart r1) -1.5d0 1.0d-7)
                    (tol< (imagpart r1) (* -1.5d0 (sqrt 3.0d0)) 1.0d-7)
                    (tol< (realpart r2) -1.5d0 1.0d-7)
                    (tol< (imagpart r2) (* 1.5d0 (sqrt 3.0d0)) 1.0d-7)
                    (tol< (realpart r3) 3.0d0 1.0d-7)
                    (tol< (imagpart r3) 0.0d0 1.0d-7)))))

(deftest "complex-solve" :category +poly+
         :test-fn
         #'(lambda ()
             (multiple-value-bind (r status)
                 (gsl-poly:complex-solve (vector -120.0d0 274.0d0 -225.0d0
                                                 85.0d0 -15.0d0 1.0d0))
               (and (= status 0)
                    (tol< (realpart (aref r 0)) 1.0d0 1.0d-9)
                    (tol< (imagpart (aref r 0)) 0.0d0 1.0d-9)
                    (tol< (realpart (aref r 1)) 2.0d0 1.0d-9)
                    (tol< (imagpart (aref r 1)) 0.0d0 1.0d-9)
                    (tol< (realpart (aref r 2)) 3.0d0 1.0d-9)
                    (tol< (imagpart (aref r 2)) 0.0d0 1.0d-9)
                    (tol< (realpart (aref r 3)) 4.0d0 1.0d-9)
                    (tol< (imagpart (aref r 3)) 0.0d0 1.0d-9)
                    (tol< (realpart (aref r 4)) 5.0d0 1.0d-9)
                    (tol< (imagpart (aref r 4)) 0.0d0 1.0d-9)))))


(deftest "dd-init" :category +poly+
         :test-fn
         #'(lambda ()
             (multiple-value-bind (r status)
                 (gsl-poly::dd-init
                  (vector 0.16d0 0.97d0 1.94d0 2.74d0 3.58d0 3.73d0 4.70d0)
                  (vector 0.73d0 1.11d0 1.49d0 1.84d0 2.30d0 2.41d0 3.07d0))
               (and (= status 0)
                    (tol< (aref r 0) 0.73d0 1.0d-9)
                    (tol< (aref r 1) 4.69135802469136d-01 1.0d-9)
                    (tol< (aref r 2) -4.34737219941284d-02 1.0d-9)
                    (tol< (aref r 3) 2.68681098870099d-02 1.0d-9)
                    (tol< (aref r 4) -3.22937056934996d-03 1.0d-9)
                    (tol< (aref r 5) 6.12763259971375d-03 1.0d-9)
                    (tol< (aref r 6) -6.45402453527083d-03 1.0d-9)))))

(deftest "dd-eval" :category +poly+
         :test-fn
         #'(lambda ()
             (tol<
              (gsl-poly::dd-eval
               (vector 0.73d0 4.69135802469136d-01 -4.34737219941284d-02
                       2.68681098870099d-02 -3.22937056934996d-03
                       6.12763259971375d-03 -6.45402453527083d-03)
               (vector 0.16d0 0.97d0 1.94d0 2.74d0 3.58d0 3.73d0 4.70d0) 0.16d0)
              0.73d0 1.0d-9)))

(deftest "dd-taylor" :category +poly+
         :test-fn
         #'(lambda ()
             (multiple-value-bind (r status)
                 (gsl-poly::dd-taylor
                  1.5d0
                  (vector 0.73d0 4.69135802469136d-01 -4.34737219941284d-02
                          2.68681098870099d-02 -3.22937056934996d-03
                          6.12763259971375d-03 -6.45402453527083d-03)
                  (vector 0.16d0 0.97d0 1.94d0 2.74d0 3.58d0 3.73d0 4.70d0))
               (and
                (= status 0)
                (tol< (gsl-poly:poly-eval r (- 0.16d0 1.5d0)) 0.73d0 1.0d-9)))))
