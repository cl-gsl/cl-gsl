;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl-test)

(defconstant +sf+ "sf")

(defmacro sf-deftest (func-name-str param-list result tolerance)
  `(deftest ,func-name-str :category +sf+
            :test-fn #'(lambda ()
                         (tol< (,(kmrcl:concat-symbol-pkg 'gsl-sf
                                                          func-name-str)
                                 ,@param-list)
                               ,result ,tolerance))))

(defmacro sf-deftest-e (func-name-str param-list result tolerance status)
  `(deftest ,func-name-str :category +sf+
            :test-fn #'(lambda ()
                         (multiple-value-bind (result error status)
                             (,(kmrcl:concat-symbol-pkg 'gsl-sf
                                                        func-name-str)
                               ,@param-list)
                           (declare (ignore error))
                           (and (tol< result ,result ,tolerance)
                                (= status ,status))))))

;; airy-ai
(sf-deftest "airy-ai" (-5.0d0 gsl:+prec-default+) 0.3507610090241142d0 +tol0+)

;; airy-ai-e
(sf-deftest-e "airy-ai-e" (-5.0d0 gsl:+prec-default+)
              0.3507610090241142d0 +tol0+ gsl:+success+)

;; airy-bi
(sf-deftest "airy-bi" (-5.0d0 gsl:+prec-default+) -0.1383691349016005d0 +tol0+)

;; airy-bi-e
(sf-deftest-e "airy-bi-e" (-5.0d0 gsl:+prec-default+)
              -0.1383691349016005d0 +tol0+ gsl:+success+)

;; airy-ai-scaled
(sf-deftest "airy-ai-scaled" (-5.0d0 gsl:+prec-default+)
            0.3507610090241142d0 +tol0+)

;; airy-ai-scaled-e
(sf-deftest-e "airy-ai-scaled-e" (-5.0d0 gsl:+prec-default+)
              0.3507610090241142d0 +tol0+ gsl:+success+)

;; airy-bi-scaled
(sf-deftest "airy-bi-scaled" (-5.0d0 gsl:+prec-default+)
            -0.1383691349016005d0 +tol0+)

;; airy-bi-scaled-e
(sf-deftest-e "airy-bi-scaled-e" (-5.0d0 gsl:+prec-default+)
              -0.1383691349016005d0 +tol0+ gsl:+success+)

;; airy-ai-deriv
(sf-deftest "airy-ai-deriv" (-5.0d0 gsl:+prec-default+)
            0.3271928185544435d0 +tol+1+)

;; airy-ai-deriv-e
(sf-deftest-e "airy-ai-deriv-e" (-5.0d0 gsl:+prec-default+)
              0.3271928185544435d0 +tol+1+ gsl:+success+)

;; airy-bi-deriv
(sf-deftest "airy-bi-deriv" (-5.0d0 gsl:+prec-default+)
            0.778411773001899d0 +tol+1+)

;; airy-bi-deriv-e
(sf-deftest-e "airy-bi-deriv-e" (-5.0d0 gsl:+prec-default+)
              0.778411773001899d0 +tol+1+ gsl:+success+)

;; airy-ai-deriv-scaled
(sf-deftest "airy-ai-deriv-scaled" (-5.0d0 gsl:+prec-default+)
            0.3271928185544435d0 +tol+1+)

;; airy-ai-deriv-scaled-e
(sf-deftest-e "airy-ai-deriv-scaled-e" (-5.0d0 gsl:+prec-default+)
              0.3271928185544435d0 +tol+1+ gsl:+success+)

;; airy-bi-deriv-scaled
(sf-deftest "airy-bi-deriv-scaled" (-5.0d0 gsl:+prec-default+)
            0.778411773001899d0 +tol+1+)

;; airy-bi-deriv-scaled-e
(sf-deftest-e "airy-bi-deriv-scaled-e" (-5.0d0 gsl:+prec-default+)
            0.778411773001899d0 +tol+1+ gsl:+success+)

;; airy-zero-ai
(sf-deftest "airy-zero-ai" (2) -4.087949444130970617d0 +tol0+)

;; airy-zero-ai-e
(sf-deftest-e "airy-zero-ai-e" (2)
              -4.087949444130970617d0 +tol0+ gsl:+success+)

;; airy-zero-bi
(sf-deftest "airy-zero-bi" (2) -3.271093302836352716d0 +tol0+)

;; airy-zero-bi-e
(sf-deftest-e "airy-zero-bi-e" (2)
              -3.271093302836352716d0 +tol0+ gsl:+success+)

;; airy-zero-ai-deriv
(sf-deftest "airy-zero-ai-deriv" (2) -3.248197582179836561d0 +tol0+)

;; airy-zero-ai-deriv-e
(sf-deftest-e "airy-zero-ai-deriv-e" (2)
              -3.248197582179836561d0 +tol0+ gsl:+success+)

;; airy-zero-bi-deriv
(sf-deftest "airy-zero-bi-deriv" (2) -4.073155089071828216d0 +tol0+)

;; airy-zero-bi-deriv-e
(sf-deftest-e "airy-zero-bi-deriv-e" (2)
              -4.073155089071828216d0 +tol0+ gsl:+success+)

;; bessel-c-j0
(sf-deftest "bessel-c-j0" (0.1d0) 0.99750156206604003230d0 +tol0+)

;; bessel-c-j0-e
(sf-deftest-e "bessel-c-j0-e" (0.1d0)
              0.99750156206604003230d0 +tol0+ gsl:+success+)

;; bessel-c-j1
(sf-deftest "bessel-c-j1" (0.1d0) 0.04993752603624199756d0 +tol0+)

;; bessel-c-j1-e
(sf-deftest-e "bessel-c-j1-e" (0.1d0)
              0.04993752603624199756d0 +tol0+ gsl:+success+)

;; bessel-c-jn
(sf-deftest "bessel-c-jn" (10 20.0d0) 0.18648255802394508321d0 +tol+1+)

;; bessel-c-jn-e
(sf-deftest-e "bessel-c-jn-e" (10 20.0d0)
              0.18648255802394508321d0 +tol+1+ gsl:+success+)

;; bessel-c-jn-array
;; bessel-c-y0
(sf-deftest "bessel-c-y0" (0.1d0) -1.5342386513503668441d0 +tol0+)

;; bessel-c-y0-e
(sf-deftest-e "bessel-c-y0-e" (0.1d0)
              -1.5342386513503668441d0 +tol0+ gsl:+success+)

;; bessel-c-y1
(sf-deftest "bessel-c-y1" (0.1d0) -6.45895109470202698800d0 +tol0+)

;; bessel-c-y1-e
(sf-deftest-e "bessel-c-y1-e" (0.1d0)
              -6.45895109470202698800d0 +tol0+ gsl:+success+)

;; bessel-c-yn
(sf-deftest "bessel-c-jn" (10 20.0d0) 0.18648255802394508321d0 +tol0+)

;; bessel-c-yn-e
(sf-deftest-e "bessel-c-jn-e" (10 20.0d0)
              0.18648255802394508321d0 +tol0+ gsl:+success+)

;; bessel-c-yn-array
;; bessel-c-i0
(sf-deftest "bessel-c-i0" (0.1d0) 1.0025015629340956014d0 +tol0+)

;; bessel-c-i0-e
(sf-deftest-e "bessel-c-i0-e" (0.1d0)
              1.0025015629340956014d0 +tol0+ gsl:+success+)

;; bessel-c-i1
(sf-deftest "bessel-c-i1" (0.1d0) 0.05006252604709269211d0 +tol0+)

;; bessel-c-i1-e
(sf-deftest-e "bessel-c-i1-e" (0.1d0)
              0.05006252604709269211d0 +tol0+ gsl:+success+)

;; bessel-c-in
(sf-deftest "bessel-c-in" (5 2.0d0) 0.009825679323131702321d0 +tol0+)

;; bessel-c-in-e
(sf-deftest-e "bessel-c-in-e" (5 2.0d0)
              0.009825679323131702321d0 +tol0+ gsl:+success+)

;; bessel-c-in-array
;; bessel-c-i0-scaled
(sf-deftest "bessel-c-i0-scaled" (0.1d0) 0.90710092578230109640d0 +tol0+)

;; bessel-c-i0-scaled-e
(sf-deftest-e "bessel-c-i0-scaled-e" (0.1d0)
              0.90710092578230109640d0 +tol0+ gsl:+success+)

;; bessel-c-i1-scaled
(sf-deftest "bessel-c-i1-scaled" (0.1d0) 0.04529844680880932501d0 +tol0+)

;; bessel-c-i1-scaled-e
(sf-deftest-e "bessel-c-i1-scaled-e" (0.1d0)
              0.04529844680880932501d0 +tol0+ gsl:+success+)

;; bessel-c-in-scaled
(sf-deftest "bessel-c-in-scaled" (5 2.0d0) 0.0013297610941881578142d0 +tol0+)

;; bessel-c-in-scaled-e
(sf-deftest-e "bessel-c-in-scaled-e" (5 2.0d0)
              0.0013297610941881578142d0 +tol0+ gsl:+success+)

;; bessel-c-in-scaled-array
;; bessel-c-k0
(sf-deftest "bessel-c-k0" (2.0d0) 0.11389387274953343565d0 +tol0+)

;; bessel-c-k0-e
(sf-deftest-e "bessel-c-k0-e" (2.0d0)
              0.11389387274953343565d0 +tol0+ gsl:+success+)

;; bessel-c-k1
(sf-deftest "bessel-c-k1" (2.0d0) 0.13986588181652242728d0 +tol0+)

;; bessel-c-k1-e
(sf-deftest-e "bessel-c-k1-e" (2.0d0)
              0.13986588181652242728d0 +tol0+ gsl:+success+)

;; bessel-c-kn
(sf-deftest "bessel-c-kn" (5 2.0d0) 9.431049100596467443d0 +tol+2+)

;; bessel-c-kn-e
(sf-deftest-e "bessel-c-kn-e" (5 2.0d0)
              9.431049100596467443d0 +tol+2+ gsl:+success+)

;; bessel-c-kn-array
;; bessel-c-k0-scaled
(sf-deftest "bessel-c-k0-scaled" (2.0d0) 0.8415682150707714179d0 +tol+1+)

;; bessel-c-k0-scaled-e
(sf-deftest-e "bessel-c-k0-scaled-e" (2.0d0)
              0.8415682150707714179d0 +tol+1+ gsl:+success+)

;; bessel-c-k1-scaled
(sf-deftest "bessel-c-k1-scaled" (2.0d0) 1.0334768470686885732d0 +tol0+)

;; bessel-c-k1-scaled-e
(sf-deftest-e "bessel-c-k1-scaled-e" (2.0d0)
              1.0334768470686885732d0 +tol0+ gsl:+success+)

;; bessel-c-kn-scaled
(sf-deftest "bessel-c-kn-scaled" (5 2.0d0) 69.68655087607675118d0 +tol+3+)

;; bessel-c-kn-scaled-e
(sf-deftest-e "bessel-c-kn-scaled-e" (5 2.0d0)
              69.68655087607675118d0 +tol+3+ gsl:+success+)

;; bessel-c-kn-scaled-array
;; bessel-s-j0
(sf-deftest "bessel-s-j0" (1.0d0) 0.84147098480789650670d0 +tol0+)

;; bessel-s-j0-e
(sf-deftest-e "bessel-s-j0-e" (1.0d0)
              0.84147098480789650670d0 +tol0+ gsl:+success+)

;; bessel-s-j1
(sf-deftest "bessel-s-j1" (1.0d0) 0.30116867893975678925d0 +tol0+)

;; bessel-s-j1-e
(sf-deftest-e "bessel-s-j1-e" (1.0d0)
              0.30116867893975678925d0 +tol0+ gsl:+success+)

;; bessel-s-j2
(sf-deftest "bessel-s-j2" (1.0d0) 0.06203505201137386110d0 +tol0+)

;; bessel-s-j2-e
(sf-deftest-e "bessel-s-j2-e" (1.0d0)
              0.06203505201137386110d0 +tol0+ gsl:+success+)

;; bessel-s-jl
(sf-deftest "bessel-s-jl" (10 10.0d0) 0.06460515449256426427d0 +tol0+)

;; bessel-s-jl-e
(sf-deftest-e "bessel-s-jl-e" (10 10.0d0)
              0.06460515449256426427d0 +tol0+ gsl:+success+)

;; bessel-s-jl-array
;; bessel-s-y0
(sf-deftest "bessel-s-y0" (1.0d0) -0.5403023058681397174d0 +tol0+)

;; bessel-s-y0-e
(sf-deftest-e "bessel-s-y0-e" (1.0d0)
              -0.5403023058681397174d0 +tol0+ gsl:+success+)

;; bessel-s-y1
(sf-deftest "bessel-s-y1" (1.0d0) -1.3817732906760362241d0 +tol0+)

;; bessel-s-y1-e
(sf-deftest-e "bessel-s-y1-e" (1.0d0)
              -1.3817732906760362241d0 +tol0+ gsl:+success+)

;; bessel-s-y2
(sf-deftest "bessel-s-y2" (1.0d0) -3.605017566159968955d0 +tol0+)

;; bessel-s-y2-e
(sf-deftest-e "bessel-s-y2-e" (1.0d0)
              -3.605017566159968955d0 +tol0+ gsl:+success+)

;; bessel-s-yl
(sf-deftest "bessel-s-yl" (0 1.0d0) -0.54030230586813972d0 +tol0+)

;; bessel-s-yl-e
(sf-deftest-e "bessel-s-yl-e" (0 1.0d0)
              -0.54030230586813972d0 +tol0+ gsl:+success+)

;; bessel-s-yl-array
;; bessel-s-i0-scaled
(sf-deftest "bessel-s-i0-scaled" (2.0d0) 0.24542109027781645493d0 +tol0+)

;; bessel-s-i0-scaled-e
(sf-deftest-e "bessel-s-i0-scaled-e" (2.0d0)
              0.24542109027781645493d0 +tol0+ gsl:+success+)

;; bessel-s-i1-scaled
(sf-deftest "bessel-s-i1-scaled" (2.0d0) 0.131868364583275317610d0 +tol0+)

;; bessel-s-i1-scaled-e
(sf-deftest-e "bessel-s-i1-scaled-e" (2.0d0)
              0.131868364583275317610d0 +tol0+ gsl:+success+)

;; bessel-s-i2-scaled
(sf-deftest "bessel-s-i2-scaled" (2.0d0) 0.0476185434029034785100d0 +tol0+)

;; bessel-s-i2-scaled-e
(sf-deftest-e "bessel-s-i2-scaled-e" (2.0d0)
              0.0476185434029034785100d0 +tol0+ gsl:+success+)

;; bessel-s-il-scaled
(sf-deftest "bessel-s-il-scaled" (5 2.0d0) 0.0004851564602127540059d0 +tol0+)

;; bessel-s-il-scaled-e
(sf-deftest-e "bessel-s-il-scaled-e" (5 2.0d0)
              0.0004851564602127540059d0 +tol0+ gsl:+success+)

;; bessel-s-il-scaled-array
;; bessel-s-k0-scaled
(sf-deftest "bessel-s-k0-scaled" (2.0d0) 0.7853981633974483096d0 +tol0+)

;; bessel-s-k0-scaled-e
(sf-deftest-e "bessel-s-k0-scaled-e" (2.0d0)
              0.7853981633974483096d0 +tol0+ gsl:+success+)

;; bessel-s-k1-scaled
(sf-deftest "bessel-s-k1-scaled" (2.0d0) 1.1780972450961724644d0 +tol0+)

;; bessel-s-k1-scaled-e
(sf-deftest-e "bessel-s-k1-scaled-e" (2.0d0)
              1.1780972450961724644d0 +tol0+ gsl:+success+)

;; bessel-s-k2-scaled
(sf-deftest "bessel-s-k2-scaled" (2.0d0) 2.5525440310417070063d0 +tol0+)

;; bessel-s-k2-scaled-e
(sf-deftest-e "bessel-s-k2-scaled-e" (2.0d0)
              2.5525440310417070063d0 +tol0+ gsl:+success+)

;; bessel-s-kl-scaled
(sf-deftest "bessel-s-kl-scaled" (5 2.0d0) 138.10735829492005119d0 +tol0+)

;; bessel-s-kl-scaled-e
(sf-deftest-e "bessel-s-kl-scaled-e" (5 2.0d0)
              138.10735829492005119d0 +tol0+ gsl:+success+)

;; bessel-s-kl-scaled-array
;; bessel-c-jnu
(sf-deftest "bessel-c-jnu" (1.0d0 1.0d0) 0.4400505857449335160d0 +tol0+)

;; bessel-c-jnu-e
(sf-deftest-e "bessel-c-jnu-e" (1.0d0 1.0d0)
              0.4400505857449335160d0 +tol0+ gsl:+success+)

;; bessel-c-ynu
(sf-deftest "bessel-c-ynu" (1.0d0 1.0d0) -0.7812128213002887165d0 +tol+1+)

;; bessel-c-ynu-e
(sf-deftest-e "bessel-c-ynu-e" (1.0d0 1.0d0)
              -0.7812128213002887165d0 +tol+1+ gsl:+success+)

;; bessel-c-inu
(sf-deftest "bessel-c-inu" (1.0d0 1.0d0) 0.5651591039924850272d0 +tol0+)

;; bessel-c-inu-e
(sf-deftest-e "bessel-c-inu-e" (1.0d0 1.0d0)
              0.5651591039924850272d0 +tol0+ gsl:+success+)

;; bessel-c-inu-scaled
(sf-deftest "bessel-c-inu-scaled" (1.0d0 1.0d0) 0.20791041534970844887d0 +tol0+)

;; bessel-c-inu-scaled-e
(sf-deftest-e "bessel-c-inu-scaled-e" (1.0d0 1.0d0)
              0.20791041534970844887d0 +tol0+ gsl:+success+)

;; bessel-c-knu
(sf-deftest "bessel-c-knu" (1.0d0 1.0d0) 0.6019072301972345747d0 +tol0+)

;; bessel-c-knu-e
(sf-deftest-e "bessel-c-knu-e" (1.0d0 1.0d0)
              0.6019072301972345747d0 +tol0+ gsl:+success+)

;; bessel-c-lnknu
(sf-deftest "bessel-c-lnknu" (1.0d0 1.0d0) -0.5076519482107523309d0 +tol0+)

;; bessel-c-lnknu-e
(sf-deftest-e "bessel-c-lnknu-e" (1.0d0 1.0d0)
              -0.5076519482107523309d0 +tol0+ gsl:+success+)

;; bessel-c-knu-scaled
(sf-deftest "bessel-c-knu-scaled" (1.0d0 1.0d0) 1.6361534862632582465d0 +tol0+)

;; bessel-c-knu-scaled-e
(sf-deftest-e "bessel-c-knu-scaled-e" (1.0d0 1.0d0)
              1.6361534862632582465d0 +tol0+ gsl:+success+)

;; bessel-c-zero-j0
(sf-deftest "bessel-c-zero-j0" (1) 2.404825557695771d0 +tol+1+)

;; bessel-c-zero-j0-e
(sf-deftest-e "bessel-c-zero-j0-e" (1)
              2.404825557695771d0 +tol+1+ gsl:+success+)

;; bessel-c-zero-j1
(sf-deftest "bessel-c-zero-j1" (1) 3.831705970207512d0 +tol+2+)

;; bessel-c-zero-j1-e
(sf-deftest-e "bessel-c-zero-j1-e" (1)
              3.831705970207512d0 +tol+2+ gsl:+success+)

;; bessel-c-zero-jnu
(sf-deftest "bessel-c-zero-jnu" (0.0d0 1) 2.404825557695771d0 +tol+2+)

;; bessel-c-zero-jnu-e
(sf-deftest-e "bessel-c-zero-jnu-e" (0.0d0 1)
              2.404825557695771d0 +tol+2+ gsl:+success+)

;; clausen
(sf-deftest "clausen" ((/ gsl-math:+pi+ 20.0d0)) 0.4478882448133546d0 +tol0+)

;; clausen-e
(sf-deftest-e "clausen-e" ((/ gsl-math:+pi+ 6.0d0))
              0.8643791310538927d0 +tol0+ gsl:+success+)

;; hydrogenic-r-1
(sf-deftest "hydrogenic-r-1" (3.0d0 2.0d0) 0.025759948256148471036d0 +tol0+)

;; hydrogenic-r-1-e
(sf-deftest-e "hydrogenic-r-1-e" (3.0d0 2.0d0)
              0.025759948256148471036d0 +tol0+ gsl:+success+)

;; hydrogenic-r
(sf-deftest "hydrogenic-r" (4 2 3.0d0 2.0d0) 0.14583027278668431009d0 +tol0+)

;; hydrogenic-r-e
(sf-deftest-e "hydrogenic-r-e" (4 2 3.0d0 2.0d0)
              0.14583027278668431009d0 +tol0+ gsl:+success+)

;; coulomb-CL-e
;; coupling-3j
(sf-deftest "coupling-3j" (0 1 1 0 1 -1) (sqrt (/ 1.0d0 2.0d0)) +tol0+)

;; coupling-3j-e
(sf-deftest-e "coupling-3j-e" (0 1 1 0 1 -1)
            (sqrt (/ 1.0d0 2.0d0)) +tol0+ gsl:+success+)

;; coupling-6j
(sf-deftest "coupling-6j" (2 2 4 2 2 2) (/ 1.0d0 6.0d0) +tol0+)

;; coupling-6j-e
(sf-deftest-e "coupling-6j-e" (2 2 4 2 2 2) (/ 1.0d0 6.0d0) +tol0+ gsl:+success+)

;; coupling-9j
(sf-deftest "coupling-9j" (4 2 4 3 3 2 1 1 2)
            (/ (sqrt (/ 1.0d0 6.0d0)) -10.0d0) +tol0+)

;; coupling-9j-e
(sf-deftest-e "coupling-9j-e" (4 2 4 3 3 2 1 1 2)
            (/ (sqrt (/ 1.0d0 6.0d0)) -10.0d0) +tol0+ gsl:+success+)

;; dawson
(sf-deftest "dawson" (0.5d0) 0.4244363835020222959d0 +tol0+)

;; dawson-e
(sf-deftest-e "dawson-e" (0.5d0) 0.4244363835020222959d0 +tol0+ gsl:+success+)

;; debye-1
(sf-deftest "debye-1" (0.1d0) 0.975277750004723276d0 +tol0+)

;; debye-1-e"
(sf-deftest-e "debye-1-e" (0.1d0) 0.975277750004723276d0 +tol0+ gsl:+success+)

;; debye-2
(sf-deftest "debye-2" (0.1d0) 0.967083287045302664d0 +tol0+)

;; debye-2-e
(sf-deftest-e "debye-2-e" (0.1d0) 0.967083287045302664d0 +tol0+ gsl:+success+)

;; debye-3
(sf-deftest "debye-3" (0.1d0) 0.962999940487211048d0 +tol0+)

;; debye-3-e
(sf-deftest-e "debye-3-e" (0.1d0) 0.962999940487211048d0 +tol0+ gsl:+success+)

;; debye-4
(sf-deftest "debye-4" (0.1d0) 0.960555486124335944d0 +tol0+)

;; debye-4-e
(sf-deftest-e "debye-4-e" (0.1d0) 0.960555486124335944d0 +tol0+ gsl:+success+)

;; dilog
(sf-deftest "dilog" (0.1d0) 0.1026177910993911d0 +tol0+)

;; dilog-e
(sf-deftest-e "dilog-e" (0.1d0) 0.1026177910993911d0 +tol0+ gsl:+success+)

;; multiply-e
;; multiply-err-e
;; ellint-kcomp
(sf-deftest "ellint-kcomp" (0.99d0 gsl:+prec-default+)
            3.3566005233611923760d0 +tol+1+)

;; ellint-kcomp-e
(sf-deftest-e "ellint-kcomp-e" (0.99d0 gsl:+prec-default+)
              3.3566005233611923760d0 +tol+1+ gsl:+success+)

;; ellint-ecomp
(sf-deftest "ellint-ecomp" (0.99d0 gsl:+prec-default+)
            1.0284758090288040010d0 +tol+1+)

;; ellint-ecomp-e
(sf-deftest-e "ellint-ecomp-e" (0.99d0 gsl:+prec-default+)
              1.0284758090288040010d0 +tol+1+ gsl:+success+)

;; ellint-f
(sf-deftest "ellint-f" ((/ gsl-math:+pi+ 3.0d0) 0.99d0 gsl:+prec-default+)
            1.3065333392738763d0 +tol0+)

;; ellint-f-e
(sf-deftest-e "ellint-f-e" ((/ gsl-math:+pi+ 3.0d0) 0.99d0 gsl:+prec-default+)
              1.3065333392738763d0 +tol0+ gsl:+success+)

;; ellint-e
(sf-deftest "ellint-e" ((/ gsl-math:+pi+ 3.0d0) 0.99d0 gsl:+prec-default+)
            0.8704819220377943536d0 +tol+1+)

;; ellint-e-e
(sf-deftest-e "ellint-e-e" ((/ gsl-math:+pi+ 3.0d0) 0.99d0 gsl:+prec-default+)
              0.8704819220377943536d0 +tol+1+ gsl:+success+)

;; ellint-p
(sf-deftest "ellint-p" ((/ gsl-math:+pi+ 3.0d0) 0.99d0 0.5d0 gsl:+prec-default+)
            1.1288726598764096d0 +tol0+)

;; ellint-p-e
(sf-deftest-e "ellint-p-e"
              ((/ gsl-math:+pi+ 3.0d0) 0.99d0 0.5d0 gsl:+prec-default+)
              1.1288726598764096d0 +tol0+ gsl:+success+)

;; ellint-D
;; ellint-D-e
;; ellint-rc
(sf-deftest "ellint-rc" (1.0d0 2.0d0 gsl:+prec-default+)
            0.7853981633974482d0 +tol+1+)

;; ellint-rc-e
(sf-deftest-e "ellint-rc-e" (1.0d0 2.0d0 gsl:+prec-default+)
              0.7853981633974482d0 +tol+1+ gsl:+success+)

;; ellint-rd
(sf-deftest "ellint-rd" (5.0d-11 1.0d-10 1.0d0 gsl:+prec-default+)
            34.0932594919337362d0 +tol0+)

;; ellint-rd-e
(sf-deftest-e "ellint-rd-e" (5.0d-11 1.0d-10 1.0d0 gsl:+prec-default+)
              34.0932594919337362d0 +tol0+ gsl:+success+)

;; ellint-rf
(sf-deftest "ellint-rf" (5.0d-11 1.0d-10 1.0d0 gsl:+prec-default+)
            12.364419829794393d0 +tol0+)

;; ellint-rf-e
(sf-deftest-e "ellint-rf-e" (5.0d-11 1.0d-10 1.0d0 gsl:+prec-default+)
              12.364419829794393d0 +tol0+ gsl:+success+)

;; ellint-RJ
;; ellint-RJ-e
;; erf
(sf-deftest "erf" (-10.0d0) -1.0d0 +tol0+)

;; erf-e
(sf-deftest-e "erf-e" (-10.0d0) -1.0d0 +tol0+ gsl:+success+)

;; erfc
(sf-deftest "erfc" (-10.0d0) 2.0d0 +tol0+)

;; erfc-e
(sf-deftest-e "erfc-e" (-10.0d0) 2.0d0 +tol0+ gsl:+success+)

;; log-erfc
(sf-deftest "log-erfc" (-1.0d0) (log 1.842700792949714869d0) +tol+1+)

;; log-erfc-e
(sf-deftest-e "log-erfc-e" (-1.0d0)
              (log 1.842700792949714869d0) +tol+1+ gsl:+success+)

;; erf-z
(sf-deftest "erf-z" (1.0d0) 0.24197072451914334980d0 +tol0+)

;; erf-z-e
(sf-deftest-e "erf-z-e" (1.0d0) 0.24197072451914334980d0 +tol0+ gsl:+success+)

;; erf-q
(sf-deftest "erf-q" (10.0d0) 7.619853024160526066d-24 +tol-2+)

;; erf-q-e
(sf-deftest-e "erf-q-e" (10.0d0) 7.619853024160526066d-24 +tol-2+ gsl:+success+)

;; hazard
(sf-deftest "hazard" (1.0d0) 1.5251352761609812091d0 +tol0+)

;; hazard-e
(sf-deftest-e "hazard-e" (1.0d0) 1.5251352761609812091d0 +tol0+ gsl:+success+)

;; exp-e
;; exp-mult
(sf-deftest "exp-mult" (-10.0d0 1.0d-6) (* 1.0d-6 (exp -10.0d0)) +tol0+)

;; exp-mult-e
(sf-deftest-e "exp-mult-e" (-10.0d0 1.0d-6)
              (* 1.0d-6 (exp -10.0d0)) +tol0+ gsl:+success+)

;; expm1
(sf-deftest "expm1" (-0.001d0) -0.00099950016662500845d0 +tol0+)

;; expm1-e
(sf-deftest-e "expm1-e" (-0.001d0)
              -0.00099950016662500845d0 +tol0+ gsl:+success+)

;; exprel
(sf-deftest "exprel" (-0.001d0) 0.9995001666250084d0 +tol+1+)

;; exprel-e
(sf-deftest-e "exprel-e" (-0.001d0)
              0.9995001666250084d0 +tol+1+ gsl:+success+)

;; exprel-2
(sf-deftest "exprel-2" (-10.0d0) 0.18000090799859524970d0 +tol0+)

;; exprel-2-e
(sf-deftest-e "exprel-2-e" (-10.0d0)
              0.18000090799859524970d0 +tol0+ gsl:+success+)

;; exprel-n
(sf-deftest "exprel-n" (3 -1000.0d0) 0.00299400600000000000d0 +tol0+)

;; exprel-n-e
(sf-deftest-e "exprel-n-e" (3 -1000.0d0)
              0.00299400600000000000d0 +tol0+ gsl:+success+)

;; exp-err-e
;; exp-mult-err-e
;; expint-e1
(sf-deftest "expint-e1" (-1.0d0) -1.8951178163559367555d0 +tol+1+)

;; expint-e1-e
(sf-deftest-e "expint-e1-e" (-1.0d0)
              -1.8951178163559367555d0 +tol+1+ gsl:+success+)

;; expint-e2
(sf-deftest "expint-e2" (-1.0d0) 0.8231640121031084799d0 +tol+1+)

;; expint-e2-e
(sf-deftest-e "expint-e2-e" (-1.0d0)
              0.8231640121031084799d0 +tol+1+ gsl:+success+)

;; expint-ei
(sf-deftest "expint-ei" (-1.0d0) -0.21938393439552027368d0 +tol0+)

;; expint-ei-e
(sf-deftest-e "expint-ei-e" (-1.0d0)
              -0.21938393439552027368d0 +tol0+ gsl:+success+)

;; shi
(sf-deftest "shi" (-1.0d0) -1.0572508753757285146d0 +tol+1+)

;; shi-e
(sf-deftest-e "shi-e" (-1.0d0) -1.0572508753757285146d0 +tol+1+ gsl:+success+)

;; chi
(sf-deftest "chi" (-1.0d0) 0.8378669409802082409d0 +tol+1+)

;; chi-e
(sf-deftest-e "chi-e" (-1.0d0) 0.8378669409802082409d0 +tol+1+ gsl:+success+)

;; expint-3
(sf-deftest "expint-3" (0.1d0) 0.09997500714119079665122d0 +tol0+)

;; expint-3-e
(sf-deftest-e "expint-3-e" (0.1d0)
              0.09997500714119079665122d0 +tol0+ gsl:+success+)

;; si
(sf-deftest "si" (-1.0d0) -0.9460830703671830149d0 +tol0+)

;; si-e
(sf-deftest-e "si-e" (-1.0d0) -0.9460830703671830149d0 +tol0+ gsl:+success+)

;; ci
(sf-deftest "ci" ((/ 1.0d0 4294967296.0d0)) -21.603494113016717041d0 +tol0+)

;; ci-e
(sf-deftest-e "ci-e" ((/ 1.0d0 4294967296.0d0))
              -21.603494113016717041d0 +tol0+ gsl:+success+)

;; atanint
(sf-deftest "atanint" (2.0d0) 1.57601540344632342236d0 +tol0+)

;; atanint-e
(sf-deftest-e "atanint-e" (2.0d0)
              1.57601540344632342236d0 +tol0+ gsl:+success+)

;; fermi-dirac-m1
(sf-deftest "fermi-dirac-m1" (-1.0d0) 0.26894142136999512075d0 +tol0+)

;; fermi-dirac-m1-e
(sf-deftest-e "fermi-dirac-m1-e" (-1.0d0)
              0.26894142136999512075d0 +tol0+ gsl:+success+)

;; fermi-dirac-0
(sf-deftest "fermi-dirac-0" (-1.0d0) 0.313261687518222834055d0 +tol0+)

;; fermi-dirac-0-e
(sf-deftest-e "fermi-dirac-0-e" (-1.0d0)
              0.31326168751822283405d0 +tol0+ gsl:+success+)

;; fermi-dirac-1
(sf-deftest "fermi-dirac-1" (-2.0d0) 0.13101248471442377127d0 +tol0+)

;; fermi-dirac-1-e
(sf-deftest-e "fermi-dirac-1-e" (-2.0d0)
              0.13101248471442377127d0 +tol0+ gsl:+success+)

;; fermi-dirac-2
(sf-deftest "fermi-dirac-2" (-1.0d0) 0.3525648792978077590d0 +tol0+)

;; fermi-dirac-2-e
(sf-deftest-e "fermi-dirac-2-e" (-1.0d0)
              0.3525648792978077590d0 +tol0+ gsl:+success+)

;; fermi-dirac-int
(sf-deftest "fermi-dirac-int" (3 0.1d0) 1.0414170610956165759d0 +tol+1+)

;; fermi-dirac-int-e
(sf-deftest-e "fermi-dirac-int-e" (3 0.1d0)
              1.0414170610956165759d0 +tol+1+ gsl:+success+)

;; fermi-dirac-mhalf
(sf-deftest "fermi-dirac-mhalf" (-2.0d0) 0.12366562180120994266d0 +tol0+)

;; fermi-dirac-mhalf-e
(sf-deftest-e "fermi-dirac-mhalf-e" (-2.0d0)
              0.12366562180120994266d0 +tol0+ gsl:+success+)

;; fermi-dirac-half
(sf-deftest "fermi-dirac-half" (-2.0d0) 0.12929851332007559106d0 +tol0+)

;; fermi-dirac-half-e
(sf-deftest-e "fermi-dirac-half-e" (-2.0d0)
              0.12929851332007559106d0 +tol0+ gsl:+success+)

;; fermi-dirac-3half
(sf-deftest "fermi-dirac-3half" (-1.0d0) 0.3466747947990574170d0 +tol0+)

;; fermi-dirac-3half-e
(sf-deftest-e "fermi-dirac-3half-e" (-1.0d0)
              0.3466747947990574170d0 +tol0+ gsl:+success+)

;; fermi-dirac-inc-0
;; fermi-dirac-inc-0-e
;; gamma
(sf-deftest "gamma" ((+ 1.0d0 (/ 1.0d0 4096.0d0)))
            0.9998591371459403421d0 +tol0+)

;; gamma-e
(sf-deftest-e "gamma-e" ((+ 1.0d0 (/ 1.0d0 4096.0d0)))
              0.9998591371459403421d0 +tol0+ gsl:+success+)

;; lngamma
(sf-deftest "lngamma" (-0.1d0) 2.368961332728788655d0 +tol+1+)

;; lngamma-e
(sf-deftest-e "lngamma-e" (-0.1d0)
              2.368961332728788655d0 +tol+1+ gsl:+success+)

;; gammastar
(sf-deftest "gammastar" (1.5d0) 1.0563442442685598666d0 +tol0+)

;; gammastar-e
(sf-deftest-e "gammastar-e" (1.5d0)
              1.0563442442685598666d0 +tol0+ gsl:+success+)

;; gammainv
(sf-deftest "gammainv" (1.0d0) 1.0d0 +tol0+)

;; gammainv-e
(sf-deftest-e "gammainv-e" (1.0d0) 1.0d0 +tol0+ gsl:+success+)

;; taylorcoeff
(sf-deftest "taylorcoeff" (10 5.0d0) 2.6911444554673721340d0 +tol0+)

;; taylorcoeff-e
(sf-deftest-e "taylorcoeff-e" (10 5.0d0)
              2.6911444554673721340d0 +tol0+ gsl:+success+)

;; fact
(sf-deftest "fact" (0) 1.0d0 +tol0+)

;; fact-e
(sf-deftest-e "fact-e" (0) 1.0d0 +tol0+ gsl:+success+)

;; doublefact
(sf-deftest "doublefact" (0) 1.0d0 +tol0+)

;; doublefact-e
(sf-deftest-e "doublefact-e" (0) 1.0d0 +tol0+ gsl:+success+)

;; lnfact
(sf-deftest "lnfact" (7) 8.525161361065414300d0 +tol0+)

;; lnfact-e
(sf-deftest-e "lnfact-e" (7) 8.525161361065414300d0 +tol0+ gsl:+success+)

;; lndoublefact
(sf-deftest "lndoublefact" (7) 4.653960350157523371d0 +tol0+)

;; lndoublefact-e
(sf-deftest-e "lndoublefact-e" (7) 4.653960350157523371d0 +tol0+ gsl:+success+)

;; choose
(sf-deftest "choose" (7 3) 35.0d0 +tol0+)

;; choose-e
(sf-deftest-e "choose-e" (7 3) 35.0d0 +tol0+ gsl:+success+)

;; lnchoose
(sf-deftest "lnchoose" (7 3) 3.555348061489413680d0 +tol+1+)

;; lnchoose-e
(sf-deftest-e "lnchoose-e" (7 3) 3.555348061489413680d0 +tol+1+ gsl:+success+)

;; poch
(sf-deftest "poch" (5.0d0 2.0d0) 30.0d0 +tol+2+)

;; poch-e
(sf-deftest-e "poch-e" (5.0d0 2.0d0) 30.0d0 +tol+2+ gsl:+success+)

;; lnpoch
(sf-deftest "lnpoch" (5.0d0 2.0d0) 3.401197381662155375d0 +tol+1+)

;; lnpoch-e
(sf-deftest-e "lnpoch-e" (5.0d0 2.0d0)
              3.401197381662155375d0 +tol+1+ gsl:+success+)

;; pochrel
(sf-deftest "pochrel" (5.0d0 2.0d0) (/ 29.0d0 2.0d0) +tol+2+)

;; pochrel-e
(sf-deftest-e "pochrel-e" (5.0d0 2.0d0) (/ 29.0d0 2.0d0) +tol+2+ gsl:+success+)

;; gamma-inc-q
(sf-deftest "gamma-inc-q" (1.0d0 1.01d0) 0.3642189795715233198d0 +tol0+)

;; gamma-inc-q-e
(sf-deftest-e "gamma-inc-q-e" (1.0d0 1.01d0)
              0.3642189795715233198d0 +tol0+ gsl:+success+)

;; gamma-inc-p
(sf-deftest "gamma-inc-p" (1.0d0 1.01d0) 0.6357810204284766802d0 +tol0+)

;; gamma-inc-p-e
(sf-deftest-e "gamma-inc-p-e" (1.0d0 1.01d0)
              0.6357810204284766802d0 +tol0+ gsl:+success+)

;; gamma-inc
(sf-deftest "gamma-inc" (-0.5d0 0.1d0) 3.4017693366916154163d0 +tol+2+)

;; gamma-inc-e
(sf-deftest-e "gamma-inc-e" (-0.5d0 0.1d0)
              3.4017693366916154163d0 +tol+2+ gsl:+success+)

;; beta
(sf-deftest "beta" (1.0d0 5.0d0) 0.2d0 +tol0+)

;; beta-e
(sf-deftest-e "beta-e" (1.0d0 5.0d0) 0.2d0 +tol0+ gsl:+success+)

;; lnbeta
(sf-deftest "lnbeta" (0.1d0 0.1d0) 2.9813614810376273949d0 +tol+1+)

;; lnbeta-e
(sf-deftest-e "lnbeta-e" (0.1d0 0.1d0)
              2.9813614810376273949d0 +tol+1+ gsl:+success+)

;; beta-inc
(sf-deftest "beta-inc" (0.1d0 1.0d0 0.5d0) 0.9330329915368074160d0 +tol0+)

;; beta-inc-e
(sf-deftest-e "beta-inc-e" (0.1d0 1.0d0 0.5d0)
              0.9330329915368074160d0 +tol0+ gsl:+success+)

;; gegenpoly-1
(sf-deftest "gegenpoly-1" (-0.2d0 1.0d0) -0.4d0 +tol0+)

;; gegenpoly-2
(sf-deftest "gegenpoly-2" (-0.2d0 0.5d0) 0.12d0 +tol0+)

;; gegenpoly-3
(sf-deftest "gegenpoly-3" (-0.2d0 0.5d0) 0.112d0 +tol0+)

;; gegenpoly-1-e
(sf-deftest-e "gegenpoly-1-e" (-0.2d0 1.0d0) -0.4d0 +tol0+ gsl:+success+)

;; gegenpoly-2-e
(sf-deftest-e "gegenpoly-2-e" (-0.2d0 0.5d0) 0.12d0 +tol0+ gsl:+success+)

;; gegenpoly-3-e
(sf-deftest-e "gegenpoly-3-e" (-0.2d0 0.5d0) 0.112d0 +tol0+ gsl:+success+)

;; gegenpoly-n
(sf-deftest "gegenpoly-n" (10 1.0d0 1.0d0) 11.0d0 +tol0+)

;; gegenpoly-n-e
(sf-deftest-e "gegenpoly-n-e" (10 1.0d0 1.0d0) 11.0d0 +tol0+ gsl:+success+)

;; gegenpoly-array
;; hyperg-0f1
(sf-deftest "hyperg-0f1" (1.0d0 0.5d0) 1.5660829297563505373d0 +tol+1+)

;; hyperg-0f1-e
(sf-deftest-e "hyperg-0f1-e" (1.0d0 0.5d0)
              1.5660829297563505373d0 +tol+1+ gsl:+success+)

;; hyperg-1f1-int
(sf-deftest "hyperg-1f1-int" (1 1 0.5d0) 1.6487212707001281468d0 +tol0+)

;; hyperg-1f1-int-e
(sf-deftest-e "hyperg-1f1-int-e" (1 1 0.5d0)
              1.6487212707001281468d0 +tol0+ gsl:+success+)

;; hyperg-1f1
(sf-deftest "hyperg-1f1" (1.5d0 2.5d0 1.0d0) 1.8834451238277954398d0 +tol0+)

;; hyperg-1f1-e
(sf-deftest-e "hyperg-1f1-e" (1.5d0 2.5d0 1.0d0)
              1.8834451238277954398d0 +tol0+ gsl:+success+)

;; hyperg-u-int
(sf-deftest "hyperg-u-int" (1 1 2.0d0) 0.3613286168882225847d0 +tol0+)

;; hyperg-u-int-e
(sf-deftest-e "hyperg-u-int-e" (1 1 2.0d0)
              0.3613286168882225847d0 +tol0+ gsl:+success+)

;; hyperg-u-int-e10-e
;; hyperg-u
(sf-deftest "hyperg-u" (0.0001d0 1.0d0 1.0d0) 0.9999999925484179084d0 +tol+2+)

;; hyperg-u-e
(sf-deftest-e "hyperg-u-e" (0.0001d0 1.0d0 1.0d0)
              0.9999999925484179084d0 +tol+2+ gsl:+success+)

;; hyperg-u-e10-e
;; hyperg-2f1
(sf-deftest "hyperg-2f1" (8.0d0 -8.0d0 1.0d0 0.5d0)
            0.13671875d0 +tol0+)

;; hyperg-2f1-e
(sf-deftest-e "hyperg-2f1-e" (8.0d0 -8.0d0 1.0d0 0.5d0)
              0.13671875d0 +tol0+ gsl:+success+)

;; hyperg-2f1-conj
(sf-deftest "hyperg-2f1-conj" (1.0d0 1.0d0 1.0d0 0.5d0)
            3.352857095662929028d0 +tol+2+)

;; hyperg-2f1-conj-e
(sf-deftest-e "hyperg-2f1-conj-e" (1.0d0 1.0d0 1.0d0 0.5d0)
              3.352857095662929028d0 +tol+2+ gsl:+success+)

;; hyperg-2f1-renorm
(sf-deftest "hyperg-2f1-renorm" (8.0d0 -8.0d0 1.0d0 0.5d0)
            0.13671875d0 +tol0+)

;; hyperg-2f1-renorm-e
(sf-deftest-e "hyperg-2f1-renorm-e" (8.0d0 -8.0d0 1.0d0 0.5d0)
              0.13671875d0 +tol0+ gsl:+success+)

;; hyperg-2f1-conj-renorm
(sf-deftest "hyperg-2f1-conj-renorm" (9.0d0 9.0d0 -1.5d0 -0.99d0)
            0.10834020229476124874d0 +tol0+)

;; hyperg-2f1-conj-renorm-e
(sf-deftest-e "hyperg-2f1-conj-renorm-e" (9.0d0 9.0d0 -1.5d0 -0.99d0)
              0.10834020229476124874d0 +tol0+ gsl:+success+)

;; hyperg-2f0
;; hyperg-2f0-e
;; laguerre-1
(sf-deftest "laguerre-1" (0.5d0 -1.0d0) 2.5d0 +tol0+)

;; laguerre-2
(sf-deftest "laguerre-2" (0.5d0 -1.0d0) 4.875d0 +tol0+)

;; laguerre-3
(sf-deftest "laguerre-3" (0.5d0 -1.0d0) 8.479166666666666667d0 +tol0+)

;; laguerre-1-e
(sf-deftest-e "laguerre-1-e" (0.5d0 -1.0d0) 2.5d0 +tol0+ gsl:+success+)

;; laguerre-2-e
(sf-deftest-e "laguerre-2-e" (0.5d0 -1.0d0) 4.875d0 +tol0+ gsl:+success+)

;; laguerre-3-e
(sf-deftest-e "laguerre-3-e" (0.5d0 -1.0d0)
              8.479166666666666667d0 +tol0+ gsl:+success+)

;; laguerre-n
(sf-deftest "laguerre-n" (4 2.0d0 0.5d0) 6.752604166666666667d0 +tol+2+)

;; laguerre-n-e
(sf-deftest-e "laguerre-n-e" (4 2.0d0 0.5d0)
              6.752604166666666667d0 +tol+2+ gsl:+success+)

;; lambert-w0
(sf-deftest "lambert-w0" (1.0d0) 0.567143290409783872999969d0 +tol0+)

;; lambert-w0-e
(sf-deftest-e "lambert-w0-e" (1.0d0)
              0.567143290409783872999969d0 +tol0+ gsl:+success+)

;; lambert-wm1
(sf-deftest "lambert-wm1" (1.0d0) 0.567143290409783872999969d0 +tol0+)

;; lambert-wm1-e
(sf-deftest-e "lambert-wm1-e" (1.0d0)
              0.567143290409783872999969d0 +tol0+ gsl:+success+)

;; legendre-p1
(sf-deftest "legendre-p1" (0.5d0) 0.5d0 +tol0+)

;; legendre-p2
(sf-deftest "legendre-p2" (0.5d0) -0.125d0 +tol0+)

;; legendre-p3
(sf-deftest "legendre-p3" (0.5d0) -0.4375d0 +tol0+)

;; legendre-p1-e
(sf-deftest-e "legendre-p1-e" (0.5d0) 0.5d0 +tol0+ gsl:+success+)

;; legendre-p2-e
(sf-deftest-e "legendre-p2-e" (0.5d0) -0.125d0 +tol0+ gsl:+success+)

;; legendre-p3-e
(sf-deftest-e "legendre-p3-e" (0.5d0) -0.4375d0 +tol0+ gsl:+success+)

;; legendre-pl
(sf-deftest "legendre-pl" (1 0.5d0) 0.5d0 +tol0+)

;; legendre-pl-e
(sf-deftest-e "legendre-pl-e" (1 0.5d0) 0.5d0 +tol0+ gsl:+success+)

;; legendre-Pl-array
;; legendre-Q0
(sf-deftest "legendre-q0" (-0.5d0) -0.5493061443340548457d0 +tol0+)

;; legendre-Q0-e
(sf-deftest-e "legendre-q0-e" (-0.5d0)
              -0.5493061443340548457d0 +tol0+ gsl:+success+)

;; legendre-Q1
(sf-deftest "legendre-q1" (-0.5d0) -0.7253469278329725772d0 +tol+1+)

;; legendre-Q1-e
(sf-deftest-e "legendre-q1-e" (-0.5d0)
              -0.7253469278329725772d0 +tol+1+ gsl:+success+)

;; legendre-Ql
(sf-deftest "legendre-ql" (10 -0.5d0) -0.29165813966586752393d0 +tol0+)

;; legendre-Ql-e
(sf-deftest-e "legendre-ql-e" (10 -0.5d0)
              -0.29165813966586752393d0 +tol0+ gsl:+success+)

;; legendre-plm
(sf-deftest "legendre-plm" (10 0 0.5d0) -0.18822860717773437500d0 +tol0+)

;; legendre-plm-e
(sf-deftest-e "legendre-plm-e" (10 0 0.5d0)
              -0.18822860717773437500d0 +tol0+ gsl:+success+)

;; legendre-sphplm
(sf-deftest "legendre-sphplm" (10 0 0.5d0) -0.24332702369300133776d0 +tol0+)

;; legendre-sphplm-e
(sf-deftest-e "legendre-sphplm-e" (10 0 0.5d0)
              -0.24332702369300133776d0 +tol0+ gsl:+success+)

;; legendre-array-size
;; conicalp-half
(sf-deftest "conicalp-half" (0.0d0 0.5d0) 0.8573827581049917129d0 +tol0+)

;; conicalp-half-e
(sf-deftest-e "conicalp-half-e" (0.0d0 0.5d0)
              0.8573827581049917129d0 +tol0+ gsl:+success+)

;; conicalp-mhalf
(sf-deftest "conicalp-mhalf" (0.0d0 0.5d0) 0.8978491247257322404d0 +tol+1+)

;; conicalp-mhalf-e
(sf-deftest-e "conicalp-mhalf-e" (0.0d0 0.5d0)
              0.8978491247257322404d0 +tol+1+ gsl:+success+)

;; conicalp-0
(sf-deftest "conicalp-0" (0.0d0 0.5d0) 1.0731820071493643751d0 +tol+1+)

;; conicalp-0-e
(sf-deftest-e "conicalp-0-e" (0.0d0 0.5d0)
              1.0731820071493643751d0 +tol+1+ gsl:+success+)

;; conicalp-1
(sf-deftest "conicalp-1" (0.0d0 0.5d0) 0.14933621085538265636d0 +tol0+)

;; conicalp-1-e
(sf-deftest-e "conicalp-1-e" (0.0d0 0.5d0)
              0.14933621085538265636d0 +tol0+ gsl:+success+)

;; conicalp-sph-reg
(sf-deftest "conicalp-sph-reg" (2 1.0d0 -0.5d0)
            1.6406279287008789526d0 +tol+1+)

;; conicalp-sph-reg-e
(sf-deftest-e "conicalp-sph-reg-e" (2 1.0d0 -0.5d0)
              1.6406279287008789526d0 +tol+1+ gsl:+success+)

;; conicalp-cyl-reg
(sf-deftest "conicalp-cyl-reg" (2 1.0d0 -0.5d0)
            2.2048510472375258708d0 +tol+1+)

;; conicalp-cyl-reg-e
(sf-deftest-e "conicalp-cyl-reg-e" (2 1.0d0 -0.5d0)
              2.2048510472375258708d0 +tol+1+ gsl:+success+)

;; legendre-h3d-0
(sf-deftest "legendre-h3d-0" (1.0d0 1.0d0) 0.7160229153604338713d0 +tol+1+)

;; legendre-h3d-0-e
(sf-deftest-e "legendre-h3d-0-e" (1.0d0 1.0d0)
              0.7160229153604338713d0 +tol+1+ gsl:+success+)

;; legendre-h3d-1
(sf-deftest "legendre-h3d-1" (1.0d0 1.0d0) 0.3397013994799344639d0 +tol0+)

;; legendre-h3d-1-e
(sf-deftest-e "legendre-h3d-1-e" (1.0d0 1.0d0)
              0.3397013994799344639d0 +tol0+ gsl:+success+)

;; legendre-h3d
(sf-deftest "legendre-h3d" (5 1.0d0 1.0d0) 0.011498635037491577728d0 +tol0+)

;; legendre-h3d-e
(sf-deftest-e "legendre-h3d-e" (5 1.0d0 1.0d0)
              0.011498635037491577728d0 +tol0+ gsl:+success+)

;; legendre-h3d-array
;; log-e
;; log-abs
(sf-deftest "log-abs" (-1.1d0) 0.095310179804324860045d0 +tol0+)

;; log-abs-e
(sf-deftest-e "log-abs-e" (-1.1d0) 0.09531017980432486004d0 +tol0+ gsl:+success+)

;; log-1plusx
(sf-deftest "log-1plusx" (0.49d0) 0.3987761199573677730d0 +tol0+)

;; log-1plusx-e
(sf-deftest-e "log-1plusx-e" (0.49d0)
              0.3987761199573677730d0 +tol0+ gsl:+success+)

;; log-1plusx-mx
(sf-deftest "log-1plusx-mx" (0.49d0) -0.09122388004263222704d0 +tol0+)

;; log-1plusx-mx-e
(sf-deftest-e "log-1plusx-mx-e" (0.49d0)
              -0.09122388004263222704d0 +tol0+ gsl:+success+)

;; pow-int
;; pow-int-e
;; psi-int
(sf-deftest "psi-int" (1) -0.57721566490153286060d0 +tol0+)

;; psi-int-e
(sf-deftest-e "psi-int-e" (1)
              -0.57721566490153286060d0 +tol0+ gsl:+success+)

;; psi
(sf-deftest "psi" (5000.0d0) 8.517093188082904107d0 +tol0+)

;; psi-e
(sf-deftest-e "psi-e" (5000.0d0)
              8.517093188082904107d0 +tol0+ gsl:+success+)

;; psi-1piy
(sf-deftest "psi-1piy" (0.8d0) -0.07088340212750589223d0 +tol0+)

;; psi-1piy-e
(sf-deftest-e "psi-1piy-e" (0.8d0)
              -0.07088340212750589223d0 +tol0+ gsl:+success+)

;; psi-1-int
(sf-deftest "psi-1-int" (1) 1.6449340668482264364d0 +tol0+)

;; psi-1-int-e
(sf-deftest-e "psi-1-int-e" (1) 1.6449340668482264364d0 +tol0+ gsl:+success+)

;; psi-1
(sf-deftest "psi-1" (1.0d0) 1.6449340668482264365d0 +tol0+)

;; psi-1-e
(sf-deftest-e "psi-1-e" (1.0d0) 1.6449340668482264365d0 +tol0+ gsl:+success+)

;; psi-n
(sf-deftest "psi-n" (1 1.0d0) 1.6449340668482264364d0 +tol0+)

;; psi-n-e
(sf-deftest-e "psi-n-e" (1 1.0d0) 1.6449340668482264364d0 +tol0+ gsl:+success+)

;; synchrotron-1
(sf-deftest "synchrotron-1" (0.01d0) 0.444972504114210632d0 +tol0+)

;; synchrotron-1-e
(sf-deftest-e "synchrotron-1-e" (0.01d0)
              0.444972504114210632d0 +tol0+ gsl:+success+)

;; synchrotron-2
(sf-deftest "synchrotron-2" (0.01d0) 0.23098077342226277732d0 +tol+1+)

;; synchrotron-2-e
(sf-deftest-e "synchrotron-2-e" (0.01d0)
              0.23098077342226277732d0 +tol+1+ gsl:+success+)

;; transport-2
(sf-deftest "transport-2" (3.0d0) 2.41105004901695346199d0 +tol0+)

;; transport-2-e
(sf-deftest-e "transport-2-e" (3.0d0)
              2.41105004901695346199d0 +tol0+ gsl:+success+)

;; transport-3
(sf-deftest "transport-3" (1.0d0) 0.479841006572417499939d0 +tol0+)

;; transport-3-e
(sf-deftest-e "transport-3-e" (1.0d0)
              0.479841006572417499939d0 +tol0+ gsl:+success+)

;; transport-4
(sf-deftest "transport-4" (1.0d0) 0.31724404523442648241d0 +tol0+)

;; transport-4-e
(sf-deftest-e "transport-4-e" (1.0d0)
              0.31724404523442648241d0 +tol0+ gsl:+success+)

;; transport-5
(sf-deftest "transport-5" (1.0d0) 0.236615879239094789259153d0 +tol0+)

;; transport-5-e
(sf-deftest-e "transport-5-e" (1.0d0)
              0.236615879239094789259153d0 +tol0+ gsl:+success+)


;; sin-e
;; cos-e
;; hypot
;; hypot-e
;; sinc
;; sinc-e
;; lnsinh
;; lnsinh-e
;; lncosh
;; lncosh-e
;; angle-restrict-symm
;; angle-restrict-symm-e
;; angle-restrict-pos
;; angle-restrict-pos-e
;; sin-err-e
;; cos-err-e
;; zeta-int
(sf-deftest "zeta-int" (5) 1.0369277551433699263313655d0 +tol0+)

;; zeta-int-e
(sf-deftest-e "zeta-int-e" (5)
              1.0369277551433699263313655d0 +tol0+ gsl:+success+)

;; zeta
(sf-deftest "zeta" (-0.5d0) -0.207886224977354566017307d0 +tol+1+)

;; zeta-e
(sf-deftest-e "zeta-e" (-0.5d0)
              -0.207886224977354566017307d0 +tol+1+ gsl:+success+)

;; zetam1-int
(sf-deftest "zetam1-int" (-5) -1.003968253968253968253968d0 +tol+1+)

;; zetam1-int-e
(sf-deftest-e "zetam1-int-e" (-5)
              -1.003968253968253968253968d0 +tol+1+ gsl:+success+)

;; zetam1
(sf-deftest "zetam1" (0.5d0) -2.460354508809586812889499d0 +tol+1+)

;; zetam1-e
(sf-deftest-e "zetam1-e" (0.5d0)
              -2.460354508809586812889499d0 +tol+1+ gsl:+success+)

;; hzeta
(sf-deftest "hzeta" (2.0d0 1.0d0) 1.6449340668482264365d0 +tol0+)

;; hzeta-e
(sf-deftest-e "hzeta-e" (2.0d0 1.0d0)
              1.6449340668482264365d0 +tol0+ gsl:+success+)

;; eta-int
(sf-deftest "eta-int" (5) 0.9721197704469093059d0 +tol0+)

;; eta-int-e
(sf-deftest-e "eta-int-e" (5) 0.9721197704469093059d0 +tol0+ gsl:+success+)

;; eta
(sf-deftest "eta" (-5.0d0) 0.25d0 +tol0+)

;; eta-e
(sf-deftest-e "eta-e" (-5.0d0) 0.25d0 +tol0+ gsl:+success+)
