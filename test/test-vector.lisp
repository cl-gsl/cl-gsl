;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl-test)

(defconstant +vector+ "vector")

(deftest "make-vector-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((v (gsl-array:make-vector 5 :element-type 'double-float)))
               (prog1
                   (and (= (gsl-array::size v) 5)
                        (eq (gsl-array::element-type v) 'double-float))
                 (gsl-array:free v)))))


(deftest "make-vector-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((v (gsl-array:make-vector 5 :element-type 'single-float)))
               (prog1
                   (and (= (gsl-array::size v) 5)
                        (eq (gsl-array::element-type v) 'single-float))
                 (gsl-array:free v)))))

(deftest "make-vector-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((v (gsl-array:make-vector 5 :element-type 'integer)))
               (prog1
                   (and (= (gsl-array::size v) 5)
                        (eq (gsl-array::element-type v) 'integer))
                 (gsl-array:free v)))))

(deftest "make-vector-complex-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((v (gsl-array:make-vector 5 :element-type
                                              '(complex (double-float)))))
               (prog1
                   (and (= (gsl-array::size v) 5)
                        (equal (gsl-array::element-type v)
                               '(complex (double-float))))
                 (gsl-array:free v)))))

(deftest "make-vector-complex-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((v (gsl-array:make-vector 5 :element-type
                                              '(complex (single-float)))))
               (prog1
                   (and (= (gsl-array::size v) 5)
                        (equal (gsl-array::element-type v)
                               '(complex (single-float))))
                 (gsl-array:free v)))))

;; ----------------------------------------------------------------------

(deftest "make-vector-double-float-initial-element" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((v (gsl-array:make-vector 5 :element-type 'double-float
                                             :initial-element 1.0d0)))
               (prog1
                   (dotimes (i 5 t)
                     (unless (= (gsl-array:get-element v i) 1.0d0)
                       (return nil)))
                 (gsl-array:free v)))))


(deftest "make-vector-single-float-initial-element" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((v (gsl-array:make-vector 5 :element-type 'single-float
                                             :initial-element 1.0)))
               (prog1
                   (dotimes (i 5 t)
                     (unless (= (gsl-array:get-element v i) 1.0)
                       (return nil)))
                 (gsl-array:free v)))))


(deftest "make-vector-integer-initial-element" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((v (gsl-array:make-vector 5 :element-type 'integer
                                             :initial-element 1)))
               (prog1
                   (dotimes (i 5 t)
                     (unless (= (gsl-array:get-element v i) 1)
                       (return nil)))
                 (gsl-array:free v)))))

(deftest "make-vector-complex-double-float-initial-element" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((v (gsl-array:make-vector 5 :element-type
                                             '(complex (double-float))
                                             :initial-element
                                             (complex 1.0d0 1.0d0))))
               (prog1
                   (dotimes (i 5 t)
                     (unless (= (gsl-array:get-element v i)
                                (complex 1.0d0 1.0d0))
                       (return nil)))
                 (gsl-array:free v)))))


(deftest "make-vector-complex-single-float-initial-element" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((v (gsl-array:make-vector 5 :element-type
                                             '(complex (single-float))
                                             :initial-element
                                             (complex 1.0 1.0))))
               (prog1
                   (dotimes (i 5 t)
                     (unless (= (gsl-array:get-element v i) (complex 1.0 1.0))
                       (return nil)))
                 (gsl-array:free v)))))

;; ----------------------------------------------------------------------

(deftest "make-vector-double-float-initial-contents" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((v (gsl-array:make-vector
                       5
                       :element-type 'double-float
                       :initial-contents (list 1.0d0 2.0d0 3.0d0 4.0d0 5.0d0)))
                   (ret t)
                   (val 0.0d0))
               (dotimes (i 5 ret)
                 (unless (= (gsl-array:get-element v i) (incf val 1.0d0))
                   (setq ret nil)))
               (gsl-array:free v)
               ret)))


(deftest "make-vector-single-float-initial-contents" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((v (gsl-array:make-vector
                       5
                       :element-type 'single-float
                       :initial-contents (vector -1.0 -2.0 -3.0 -4.0 -5.0)))
                   (ret t)
                   (val 0.0))
               (dotimes (i 5 ret)
                 (unless (= (gsl-array:get-element v i) (decf val 1.0))
                   (setq ret nil)))
               (gsl-array:free v)
               ret)))


(deftest "make-vector-integer-initial-contents" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((v (gsl-array:make-vector
                       5
                       :element-type 'integer
                       :initial-contents (list 1 2 3 4 5)))
                   (ret t)
                   (val 0))
               (dotimes (i 5 ret)
                 (unless (= (gsl-array:get-element v i) (incf val))
                   (setq ret nil)))
               (gsl-array:free v)
               ret)))

(deftest "make-vector-complex-double-float-initial-contents" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((v (gsl-array:make-vector
                       5
                       :element-type '(complex (double-float))
                       :initial-contents
                       (vector (complex 1.0d0 1.0d0) (complex 2.0d0 2.0d0)
                               (complex 3.0d0 3.0d0) (complex 4.0d0 4.0d0)
                               (complex 5.0d0 5.0d0))))
                   (ret t)
                   (val (complex 0.0d0 0.0d0)))
               (dotimes (i 5 ret)
                 (unless (= (gsl-array:get-element v i)
                            (incf val (complex 1.0d0 1.0d0)))
                   (setq ret nil)))
               (gsl-array:free v)
               ret)))

(deftest "make-vector-complex-single-float-initial-contents" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((v (gsl-array:make-vector
                       5
                       :element-type '(complex (single-float))
                       :initial-contents
                       (list (complex -1.0 -1.0) (complex -2.0 -2.0)
                             (complex -3.0 -3.0) (complex -4.0 -4.0)
                             (complex -5.0 -5.0))))
                   (ret t)
                   (val (complex 0.0 0.0)))
               (dotimes (i 5 ret)
                 (unless (= (gsl-array:get-element v i)
                            (decf val (complex 1.0 1.0)))
                   (setq ret nil)))
               (gsl-array:free v)
               ret)))

;; ----------------------------------------------------------------------

(deftest "set-all-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector (v 5 :element-type 'double-float)
               (gsl-array:set-all v 5.0d0)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (unless (= (gsl-array:get-element v i) 5.0d0)
                     (setq ret nil)))))))


(deftest "set-all-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector (v 5 :element-type 'single-float)
               (gsl-array:set-all v 5.0)
               (dotimes (i 5 t)
                 (unless (= (gsl-array:get-element v i) 5.0)
                   (return nil))))))

(deftest "set-all-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector (v 5 :element-type 'integer)
               (gsl-array:set-all v 5)
               (dotimes (i 5 t)
                 (unless (= (gsl-array:get-element v i) 5)
                   (return nil))))))

(deftest "set-all-complex-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector
                 (v 5 :element-type '(complex (double-float)))
               (gsl-array:set-all v (complex 5.0d0 4.0d0))
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (unless (= (gsl-array:get-element v i)
                              (complex 5.0d0 4.0d0))
                     (setq ret nil)))))))

(deftest "set-all-complex-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector
                 (v 5 :element-type '(complex (single-float)))
               (gsl-array:set-all v (complex 5.0 4.0))
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (unless (= (gsl-array:get-element v i) (complex 5.0 4.0))
                     (setq ret nil)))))))

;; ----------------------------------------------------------------------

(deftest "set-zero-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector (v 5 :element-type 'double-float)
               (gsl-array:set-all v 5.0d0)
               (gsl-array:set-zero v)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (unless (= (gsl-array:get-element v i) 0.0d0)
                     (setq ret nil)))))))


(deftest "set-zero-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector (v 5 :element-type 'single-float)
               (gsl-array:set-all v 5.0)
               (gsl-array:set-zero v)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (unless (= (gsl-array:get-element v i) 0.0)
                     (setq ret nil)))))))

(deftest "set-zero-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector (v 5 :element-type 'integer)
               (gsl-array:set-all v 5)
               (gsl-array:set-zero v)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (unless (= (gsl-array:get-element v i) 0)
                     (setq ret nil)))))))

(deftest "set-zero-complex-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector
                 (v 5 :element-type '(complex (double-float)))
               (gsl-array:set-all v (complex 5.0d0 4.0d0))
               (gsl-array:set-zero v)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (unless (= (gsl-array:get-element v i)
                              (complex 0.0d0 0.0d0))
                     (setq ret nil)))))))

(deftest "set-zero-complex-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector
                 (v 5 :element-type '(complex (single-float)))
               (gsl-array:set-all v (complex 5.0 4.0))
               (gsl-array:set-zero v)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (unless (= (gsl-array:get-element v i) (complex 0.0 0.0))
                     (setq ret nil)))))))

;; ----------------------------------------------------------------------

(deftest "set-basis-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector (v 5 :element-type 'double-float)
               (gsl-array:set-basis v 3)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (if (= i 3)
                       (unless (= (gsl-array:get-element v i) 1.0d0)
                         (setq ret nil))
                       (unless (= (gsl-array:get-element v i) 0.0d0)
                         (setq ret nil))))))))

(deftest "set-basis-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector (v 5 :element-type 'single-float)
               (gsl-array:set-basis v 2)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (if (= i 2)
                       (unless (= (gsl-array:get-element v i) 1.0)
                         (setq ret nil))
                       (unless (= (gsl-array:get-element v i) 0.0)
                         (setq ret nil))))))))

(deftest "set-basis-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector (v 5 :element-type 'integer)
               (gsl-array:set-basis v 1)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (if (= i 1)
                       (unless (= (gsl-array:get-element v i) 1)
                         (setq ret nil))
                       (unless (= (gsl-array:get-element v i) 0)
                         (setq ret nil))))))))

(deftest "set-basis-complex-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector
                 (v 5 :element-type '(complex (double-float)))
               (gsl-array:set-basis v 4)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (if (= i 4)
                       (unless (= (gsl-array:get-element v i)
                                  (complex 1.0d0 0.0d0))
                         (setq ret nil))
                       (unless (= (gsl-array:get-element v i)
                                  (complex 0.0d0 0.0d0))
                         (setq ret nil))))))))

(deftest "set-basis-complex-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector
                 (v 5 :element-type '(complex (single-float)))
               (gsl-array:set-basis v 0)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (if (= i 0)
                       (unless (= (gsl-array:get-element v i)
                                  (complex 1.0 0.0))
                         (setq ret nil))
                       (unless (= (gsl-array:get-element v i)
                                  (complex 0.0 0.0))
                         (setq ret nil))))))))


;; ----------------------------------------------------------------------

(deftest "set-element-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector (v 5 :element-type 'double-float)
               (gsl-array:set-zero v)
               (gsl-array:set-element v 3 6.0d0)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (if (= i 3)
                       (unless (= (gsl-array:get-element v i) 6.0d0)
                         (setq ret nil))
                       (unless (= (gsl-array:get-element v i) 0.0d0)
                         (setq ret nil))))))))


(deftest "set-element-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector (v 5 :element-type 'single-float)
               (gsl-array:set-zero v)
               (gsl-array:set-element v 2 6.0)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (if (= i 2)
                       (unless (= (gsl-array:get-element v i) 6.0)
                         (setq ret nil))
                       (unless (= (gsl-array:get-element v i) 0.0)
                         (setq ret nil))))))))

(deftest "set-element-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector (v 5 :element-type 'integer)
               (gsl-array:set-zero v)
               (gsl-array:set-element v 1 6)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (if (= i 1)
                       (unless (= (gsl-array:get-element v i) 6)
                         (setq ret nil))
                       (unless (= (gsl-array:get-element v i) 0)
                         (setq ret nil))))))))

(deftest "set-element-complex-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector
                 (v 5 :element-type '(complex (double-float)))
               (gsl-array:set-zero v)
               (gsl-array:set-element v 4 (complex 6.0d0 7.0d0))
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (if (= i 4)
                       (unless (= (gsl-array:get-element v i)
                                  (complex 6.0d0 7.0d0))
                         (setq ret nil))
                       (unless (= (gsl-array:get-element v i)
                                  (complex 0.0d0 0.0d0))
                         (setq ret nil))))))))

(deftest "set-element-complex-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector
                 (v 5 :element-type '(complex (single-float)))
               (gsl-array:set-zero v)
               (gsl-array:set-element v 0 (complex 6.0 7.0))
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (if (= i 0)
                       (unless (= (gsl-array:get-element v i)
                                  (complex 6.0 7.0))
                         (setq ret nil))
                       (unless (= (gsl-array:get-element v i)
                                  (complex 0.0 0.0))
                         (setq ret nil))))))))

;; ----------------------------------------------------------------------

(deftest "isnull-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector (v 5 :element-type 'double-float)
               (gsl-array:set-zero v)
               (gsl-array:isnull v))))


(deftest "isnull-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector (v 5 :element-type 'single-float)
               (gsl-array:set-basis v 0)
               ;; check for failure of isnull
               (not (gsl-array:isnull v)))))


(deftest "isnull-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector (v 5 :element-type 'integer)
               (gsl-array:set-zero v)
               (gsl-array:isnull v))))

(deftest "isnull-complex-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector
                 (v 5 :element-type '(complex (double-float)))
               (gsl-array:set-basis v 1)
               (not (gsl-array:isnull v)))))

(deftest "isnull-complex-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (gsl-array:with-vector
                 (v 5 :element-type '(complex (single-float)))
               (gsl-array:set-zero v)
               (gsl-array:isnull v))))

;; ----------------------------------------------------------------------

(deftest "reverse-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector 1.0d0 2.0d0 3.0d0 4.0d0 5.0d0)))
               (gsl-array:with-vector
                   (v 5 :element-type 'double-float :initial-contents vec)
                 (equalp (reverse vec)
                         (gsl-array:gsl->lisp-vector
                          (gsl-array:reverse-vector v)))))))

(deftest "reverse-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector 1.0 2.0 3.0 4.0 5.0)))
               (gsl-array:with-vector
                   (v 5 :element-type 'single-float :initial-contents vec)
                 (equalp (reverse vec)
                         (gsl-array:gsl->lisp-vector
                          (gsl-array:reverse-vector v)))))))

(deftest "reverse-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector 1 2 3 4 5)))
               (gsl-array:with-vector
                   (v 5 :element-type 'integer :initial-contents vec)
                 (equalp (reverse vec)
                         (gsl-array:gsl->lisp-vector
                          (gsl-array:reverse-vector v)))))))


(deftest "reverse-complex-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector (complex 1.0d0 1.0d0) (complex 2.0d0 2.0d0)
                                (complex 3.0d0 3.0d0) (complex 4.0d0 4.0d0)
                                (complex 5.0d0 5.0d0))))
               (gsl-array:with-vector
                   (v 5 :element-type '(complex (double-float))
                      :initial-contents vec)
                 (equalp (reverse vec)
                         (gsl-array:gsl->lisp-vector
                          (gsl-array:reverse-vector v)))))))


(deftest "reverse-complex-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector (complex -1.0 -1.0) (complex -2.0 -2.0)
                                (complex -3.0 -3.0) (complex -4.0 -4.0)
                                (complex -5.0 -5.0))))
               (gsl-array:with-vector
                   (v 5 :element-type '(complex (single-float))
                      :initial-contents vec)
                 (equalp (reverse vec)
                         (gsl-array:gsl->lisp-vector
                          (gsl-array:reverse-vector v)))))))

;; ----------------------------------------------------------------------

(deftest "read-write-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector 1.0d0 2.0d0 3.0d0 4.0d0 5.0d0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float :initial-contents vec)
                 (gsl-array:write-to-file "/tmp/test.txt" v1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'double-float
                         :from-file "/tmp/test.txt")
                   (and (equalp vec (gsl-array:gsl->lisp-vector v1))
                        (equalp vec (gsl-array:gsl->lisp-vector v2))))))))

(deftest "read-write-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector 1.0 2.0 3.0 4.0 5.0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float :initial-contents vec)
                 (gsl-array:write-to-file "/tmp/test.txt" v1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'single-float
                         :from-file "/tmp/test.txt")
                   (and (equalp vec (gsl-array:gsl->lisp-vector v1))
                        (equalp vec (gsl-array:gsl->lisp-vector v2))))))))

(deftest "read-write-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector 1 2 3 4 5)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer :initial-contents vec)
                 (gsl-array:write-to-file "/tmp/test.txt" v1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'integer :from-file "/tmp/test.txt")
                   (and (equalp vec (gsl-array:gsl->lisp-vector v1))
                        (equalp vec (gsl-array:gsl->lisp-vector v2))))))))

(deftest "read-write-complex-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector (complex 1.0d0 1.0d0) (complex 2.0d0 2.0d0)
                                (complex 3.0d0 3.0d0) (complex 4.0d0 4.0d0)
                                (complex 5.0d0 5.0d0))))
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (double-float))
                       :initial-contents vec)
                 (gsl-array:write-to-file "/tmp/test.txt" v1)
                 (gsl-array:with-vector
                     (v2 5 :element-type '(complex (double-float))
                         :from-file "/tmp/test.txt")
                   (and (equalp vec (gsl-array:gsl->lisp-vector v1))
                        (equalp vec (gsl-array:gsl->lisp-vector v2))))))))

(deftest "read-write-complex-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector (complex 1.0 1.0) (complex 2.0 2.0)
                                (complex 3.0 3.0) (complex 4.0 4.0)
                                (complex 5.0 5.0))))
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (single-float))
                       :initial-contents vec)
                 (gsl-array:write-to-file "/tmp/test.txt" v1)
                 (gsl-array:with-vector
                     (v2 5 :element-type '(complex (single-float))
                         :from-file "/tmp/test.txt")
                   (and (equalp vec (gsl-array:gsl->lisp-vector v1))
                        (equalp vec (gsl-array:gsl->lisp-vector v2))))))))


;; ----------------------------------------------------------------------

(deftest "read-write-binary-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector 1.0000000000001d0 2.0d0 3.0d0 4.0d0 5.0d0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float :initial-contents vec)
                 (gsl-array:write-to-binary-file "/tmp/test.bin" v1)
                 (gsl-array:with-vector (v2 5 :element-type 'double-float
                                             :from-binary-file "/tmp/test.bin")
                   (and (equalp vec (gsl-array:gsl->lisp-vector v1))
                        (equalp vec (gsl-array:gsl->lisp-vector v2))))))))

(deftest "read-write-binary-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector 1.0 2.0 3.0 4.0 5.0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float :initial-contents vec)
                 (gsl-array:write-to-binary-file "/tmp/test.bin" v1)
                 (gsl-array:with-vector (v2 5 :element-type 'single-float
                                             :from-binary-file "/tmp/test.bin")
                   (and (equalp vec (gsl-array:gsl->lisp-vector v1))
                        (equalp vec (gsl-array:gsl->lisp-vector v2))))))))


(deftest "read-write-binary-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector 1 2 3 4 5)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer :initial-contents vec)
                 (gsl-array:write-to-binary-file "/tmp/test.bin" v1)
                 (gsl-array:with-vector (v2 5 :element-type 'integer
                                             :from-binary-file "/tmp/test.bin")
                   (and (equalp vec (gsl-array:gsl->lisp-vector v1))
                        (equalp vec (gsl-array:gsl->lisp-vector v2))))))))

(deftest "read-write-binary-complex-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector (complex 1.0d0 1.0d0) (complex 2.0d0 2.0d0)
                                (complex 3.0d0 3.0d0) (complex 4.0d0 4.0d0)
                                (complex 5.0d0 5.0d0))))
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (double-float))
                       :initial-contents vec)
                 (gsl-array:write-to-binary-file "/tmp/test.bin" v1)
                 (gsl-array:with-vector
                     (v2 5 :element-type '(complex (double-float))
                         :from-binary-file "/tmp/test.bin")
                   (and (equalp vec (gsl-array:gsl->lisp-vector v1))
                        (equalp vec (gsl-array:gsl->lisp-vector v2))))))))

(deftest "read-write-binary-complex-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector (complex 1.0 1.0) (complex 2.0 2.0)
                                (complex 3.0 3.0) (complex 4.0 4.0)
                                (complex 5.0 5.0))))
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (single-float))
                       :initial-contents vec)
                 (gsl-array:write-to-binary-file "/tmp/test.bin" v1)
                 (gsl-array:with-vector
                     (v2 5 :element-type '(complex (single-float))
                         :from-binary-file "/tmp/test.bin")
                   (and (equalp vec (gsl-array:gsl->lisp-vector v1))
                        (equalp vec (gsl-array:gsl->lisp-vector v2))))))))

;; ----------------------------------------------------------------------

(deftest "copy-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector 1.0d0 2.0d0 3.0d0 4.0d0 5.0d0))
                   (v2)
                   (res))
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float :initial-contents vec)
                 (setq v2 (gsl-array:copy v1))
                 (setq res (equalp (gsl-array:gsl->lisp-vector v1)
                                   (gsl-array:gsl->lisp-vector v2)))
                 (gsl-array:free v2))
               res)))

(deftest "copy-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector 1.0 2.0 3.0 4.0 5.0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float :initial-contents vec)
                 (gsl-array:with-vector-copy (v2 v1)
                   (and (equalp vec (gsl-array:gsl->lisp-vector v1))
                        (equalp vec (gsl-array:gsl->lisp-vector v2))))))))

(deftest "copy-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector 1 2 3 4 5)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer :initial-contents vec)
                 (gsl-array:with-vector-copy (v2 v1)
                   (and (equalp vec (gsl-array:gsl->lisp-vector v1))
                        (equalp vec (gsl-array:gsl->lisp-vector v2))))))))

(deftest "copy-complex-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector (complex 1.0d0 1.0d0) (complex 2.0d0 2.0d0)
                                (complex 3.0d0 3.0d0) (complex 4.0d0 4.0d0)
                                (complex 5.0d0 5.0d0))))
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (double-float))
                       :initial-contents vec)
                 (gsl-array:with-vector-copy (v2 v1)
                   (and (equalp vec (gsl-array:gsl->lisp-vector v1))
                        (equalp vec (gsl-array:gsl->lisp-vector v2))))))))

(deftest "copy-complex-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec (vector (complex 1.0 1.0) (complex 2.0 2.0)
                                (complex 3.0 3.0) (complex 4.0 4.0)
                                (complex 5.0 5.0))))
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (single-float))
                       :initial-contents vec)
                 (gsl-array:with-vector-copy (v2 v1)
                   (and (equalp vec (gsl-array:gsl->lisp-vector v1))
                        (equalp vec (gsl-array:gsl->lisp-vector v2))))))))


;; ----------------------------------------------------------------------

(deftest "swap-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let* ((vec-1 (vector 1.0d0 2.0d0 3.0d0 4.0d0 5.0d0))
                    (vec-2 (reverse vec-1)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float :initial-contents vec-1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'double-float :initial-contents vec-2)
                   (gsl-array:swap v1 v2)
                   (and (equalp vec-2 (gsl-array:gsl->lisp-vector v1))
                        (equalp vec-1 (gsl-array:gsl->lisp-vector v2))))))))

(deftest "swap-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let* ((vec-1 (vector 1.0 2.0 3.0 4.0 5.0))
                    (vec-2 (reverse vec-1)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float :initial-contents vec-1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'single-float :initial-contents vec-2)
                   (gsl-array:swap v1 v2)
                   (and (equalp vec-2 (gsl-array:gsl->lisp-vector v1))
                        (equalp vec-1 (gsl-array:gsl->lisp-vector v2))))))))

(deftest "swap-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let* ((vec-1 (vector 1 2 3 4 5))
                    (vec-2 (reverse vec-1)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer :initial-contents vec-1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'integer :initial-contents vec-2)
                   (gsl-array:swap v1 v2)
                   (and (equalp vec-2 (gsl-array:gsl->lisp-vector v1))
                        (equalp vec-1 (gsl-array:gsl->lisp-vector v2))))))))

(deftest "swap-complex-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let* ((vec-1 (vector (complex 1.0d0 1.0d0) (complex 2.0d0 2.0d0)
                                   (complex 3.0d0 3.0d0) (complex 4.0d0 4.0d0)
                                   (complex 5.0d0 5.0d0)))
                    (vec-2 (reverse vec-1)))
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (double-float))
                       :initial-contents vec-1)
                 (gsl-array:with-vector
                     (v2 5 :element-type '(complex (double-float))
                         :initial-contents vec-2)
                   (gsl-array:swap v1 v2)
                   (and (equalp vec-2 (gsl-array:gsl->lisp-vector v1))
                        (equalp vec-1 (gsl-array:gsl->lisp-vector v2))))))))

(deftest "swap-complex-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let* ((vec-1 (vector (complex 1.0 1.0) (complex 2.0 2.0)
                                   (complex 3.0 3.0) (complex 4.0 4.0)
                                   (complex 5.0 5.0)))
                    (vec-2 (reverse vec-1)))
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (single-float))
                       :initial-contents vec-1)
                 (gsl-array:with-vector
                     (v2 5 :element-type '(complex (single-float))
                         :initial-contents vec-2)
                   (gsl-array:swap v1 v2)
                   (and (equalp vec-2 (gsl-array:gsl->lisp-vector v1))
                        (equalp vec-1 (gsl-array:gsl->lisp-vector v2))))))))

;; ----------------------------------------------------------------------

(deftest "swap-elements-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1.0d0 2.0d0 3.0d0 4.0d0 5.0d0))
                   (vec-2 (vector 1.0d0 2.0d0 4.0d0 3.0d0 5.0d0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float :initial-contents vec-1)
                   (gsl-array:swap-elements v1 2 3)
                   (equalp vec-2 (gsl-array:gsl->lisp-vector v1))))))

(deftest "swap-elements-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1.0 2.0 3.0 4.0 5.0))
                   (vec-2 (vector 1.0 2.0 4.0 3.0 5.0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float :initial-contents vec-1)
                   (gsl-array:swap-elements v1 2 3)
                   (equalp vec-2 (gsl-array:gsl->lisp-vector v1))))))

(deftest "swap-elements-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1 2 3 4 5))
                   (vec-2 (vector 1 2 4 3 5)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer :initial-contents vec-1)
                   (gsl-array:swap-elements v1 2 3)
                   (equalp vec-2 (gsl-array:gsl->lisp-vector v1))))))

(deftest "swap-elements-complex-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector (complex 1.0d0 1.0d0) (complex 2.0d0 2.0d0)
                                  (complex 3.0d0 3.0d0) (complex 4.0d0 4.0d0)
                                  (complex 5.0d0 5.0d0)))
                   (vec-2 (vector (complex 1.0d0 1.0d0) (complex 2.0d0 2.0d0)
                                  (complex 4.0d0 4.0d0) (complex 3.0d0 3.0d0)
                                  (complex 5.0d0 5.0d0))))
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (double-float))
                       :initial-contents vec-1)
                   (gsl-array:swap-elements v1 2 3)
                   (equalp vec-2 (gsl-array:gsl->lisp-vector v1))))))

(deftest "swap-elements-complex-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector (complex 1.0 1.0) (complex 2.0 2.0)
                                  (complex 3.0 3.0) (complex 4.0 4.0)
                                  (complex 5.0 5.0)))
                   (vec-2 (vector (complex 1.0 1.0) (complex 2.0 2.0)
                                  (complex 4.0 4.0) (complex 3.0 3.0)
                                  (complex 5.0 5.0))))
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (single-float))
                       :initial-contents vec-1)
                   (equalp vec-2 (gsl-array:gsl->lisp-vector
                                  (gsl-array:swap-elements v1 2 3)))))))

;; ----------------------------------------------------------------------

(deftest "add-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1.0d0 2.0d0 3.0d0 4.0d0 5.0d0))
                   (vec-2 (vector 2.0d0 4.0d0 6.0d0 8.0d0 10.0d0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float :initial-contents vec-1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'double-float :initial-contents vec-1)
                   (gsl-array:add v1 v2)
                   (equalp vec-2 (gsl-array:gsl->lisp-vector v1)))))))

(deftest "add-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1.0 2.0 3.0 4.0 5.0))
                   (vec-2 (vector 2.0 4.0 6.0 8.0 10.0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float :initial-contents vec-1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'single-float :initial-contents vec-1)
                   (gsl-array:add v1 v2)
                   (equalp vec-2 (gsl-array:gsl->lisp-vector v1)))))))

(deftest "add-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1 2 3 4 5))
                   (vec-2 (vector 2 4 6 8 10)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer :initial-contents vec-1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'integer :initial-contents vec-1)
                   (gsl-array:add v1 v2)
                   (equalp vec-2 (gsl-array:gsl->lisp-vector v1)))))))


;; ----------------------------------------------------------------------

(deftest "sub-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1.0d0 2.0d0 3.0d0 4.0d0 5.0d0))
                   (vec-2 (vector 0.0d0 0.0d0 0.0d0 0.0d0 0.0d0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float :initial-contents vec-1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'double-float :initial-contents vec-1)
                   (gsl-array:sub v1 v2)
                   (equalp vec-2 (gsl-array:gsl->lisp-vector v1)))))))

(deftest "sub-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1.0 2.0 3.0 4.0 5.0))
                   (vec-2 (vector 0.0 0.0 0.0 0.0 0.0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float :initial-contents vec-1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'single-float :initial-contents vec-1)
                   (gsl-array:sub v1 v2)
                   (equalp vec-2 (gsl-array:gsl->lisp-vector v1)))))))

(deftest "sub-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1 2 3 4 5))
                   (vec-2 (vector 0 0 0 0 0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer :initial-contents vec-1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'integer :initial-contents vec-1)
                   (gsl-array:sub v1 v2)
                   (equalp vec-2 (gsl-array:gsl->lisp-vector v1)))))))

;; ----------------------------------------------------------------------

(deftest "mul-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1.0d0 2.0d0 3.0d0 4.0d0 5.0d0))
                   (vec-2 (vector 1.0d0 4.0d0 9.0d0 16.0d0 25.0d0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float :initial-contents vec-1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'double-float :initial-contents vec-1)
                   (gsl-array:mul v1 v2)
                   (equalp vec-2 (gsl-array:gsl->lisp-vector v1)))))))

(deftest "mul-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1.0 2.0 3.0 4.0 5.0))
                   (vec-2 (vector 1.0 4.0 9.0 16.0 25.0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float :initial-contents vec-1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'single-float :initial-contents vec-1)
                   (gsl-array:mul v1 v2)
                   (equalp vec-2 (gsl-array:gsl->lisp-vector v1)))))))

(deftest "mul-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1 2 3 4 5))
                   (vec-2 (vector 1 4 9 16 25)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer :initial-contents vec-1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'integer :initial-contents vec-1)
                   (gsl-array:mul v1 v2)
                   (equalp vec-2 (gsl-array:gsl->lisp-vector v1)))))))

;; ----------------------------------------------------------------------

(deftest "div-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1.0d0 2.0d0 3.0d0 4.0d0 5.0d0))
                   (vec-2 (vector 1.0d0 1.0d0 1.0d0 1.0d0 1.0d0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float :initial-contents vec-1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'double-float :initial-contents vec-1)
                   (gsl-array:div v1 v2)
                   (equalp vec-2 (gsl-array:gsl->lisp-vector v1)))))))

(deftest "div-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1.0 2.0 3.0 4.0 5.0))
                   (vec-2 (vector 1.0 1.0 1.0 1.0 1.0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float :initial-contents vec-1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'single-float :initial-contents vec-1)
                   (gsl-array:div v1 v2)
                   (equalp vec-2 (gsl-array:gsl->lisp-vector v1)))))))

(deftest "div-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1 2 3 4 5))
                   (vec-2 (vector 1 1 1 1 1)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer :initial-contents vec-1)
                 (gsl-array:with-vector
                     (v2 5 :element-type 'integer :initial-contents vec-1)
                   (gsl-array:div v1 v2)
                   (equalp vec-2 (gsl-array:gsl->lisp-vector v1)))))))


;; ----------------------------------------------------------------------

(deftest "scale-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1.0d0 2.0d0 3.0d0 4.0d0 5.0d0))
                   (vec-2 (vector 10.0d0 20.0d0 30.0d0 40.0d0 50.0d0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float :initial-contents vec-1)
                 (gsl-array:scale v1 10.0d0)
                 (equalp vec-2 (gsl-array:gsl->lisp-vector v1))))))

(deftest "scale-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1.0 2.0 3.0 4.0 5.0))
                   (vec-2 (vector 10.0 20.0 30.0 40.0 50.0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float :initial-contents vec-1)
                 (gsl-array:scale v1 10.0)
                 (equalp vec-2 (gsl-array:gsl->lisp-vector v1))))))

(deftest "scale-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1 2 3 4 5))
                   (vec-2 (vector 10 20 30 40 50)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer :initial-contents vec-1)
                 (gsl-array:scale v1 10)
                 (equalp vec-2 (gsl-array:gsl->lisp-vector v1))))))


;; ----------------------------------------------------------------------

(deftest "add-constant-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1.0d0 2.0d0 3.0d0 4.0d0 5.0d0))
                   (vec-2 (vector 11.0d0 12.0d0 13.0d0 14.0d0 15.0d0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float :initial-contents vec-1)
                 (gsl-array:add-constant v1 10.0d0)
                 (equalp vec-2 (gsl-array:gsl->lisp-vector v1))))))

(deftest "add-constant-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1.0 2.0 3.0 4.0 5.0))
                   (vec-2 (vector 11.0 12.0 13.0 14.0 15.0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float :initial-contents vec-1)
                 (gsl-array:add-constant v1 10.0)
                 (equalp vec-2 (gsl-array:gsl->lisp-vector v1))))))

(deftest "add-constant-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 1 2 3 4 5))
                   (vec-2 (vector 11 12 13 14 15)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer :initial-contents vec-1)
                 (gsl-array:add-constant v1 10)
                 (equalp vec-2 (gsl-array:gsl->lisp-vector v1))))))

;; ----------------------------------------------------------------------

(deftest "max-value-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4.0d0 5.0d0 1.0d0 2.0d0 3.0d0))
                   (max-val 5.0d0))
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float :initial-contents vec-1)
                 (= max-val (gsl-array:max-value v1))))))

(deftest "max-value-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4.0 5.0 1.0 2.0 3.0))
                   (max-val 5.0))
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float :initial-contents vec-1)
                 (= max-val (gsl-array:max-value v1))))))

(deftest "max-value-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4 5 1 2 3))
                   (max-val 5))
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer :initial-contents vec-1)
                 (= max-val (gsl-array:max-value v1))))))

;; ----------------------------------------------------------------------

(deftest "min-value-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4.0d0 5.0d0 1.0d0 2.0d0 3.0d0))
                   (min-val 1.0d0))
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float :initial-contents vec-1)
                 (= min-val (gsl-array:min-value v1))))))

(deftest "min-value-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4.0 5.0 1.0 2.0 3.0))
                   (min-val 1.0))
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float :initial-contents vec-1)
                 (= min-val (gsl-array:min-value v1))))))

(deftest "min-value-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4 5 1 2 3))
                   (min-val 1))
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer :initial-contents vec-1)
                 (= min-val (gsl-array:min-value v1))))))

;; ----------------------------------------------------------------------

(deftest "max-index-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4.0d0 5.0d0 1.0d0 2.0d0 3.0d0))
                   (max-idx 1))
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float :initial-contents vec-1)
                 (= max-idx (gsl-array:max-index v1))))))

(deftest "max-index-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4.0 5.0 1.0 2.0 3.0))
                   (max-idx 1))
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float :initial-contents vec-1)
                 (= max-idx (gsl-array:max-index v1))))))

(deftest "max-index-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4 5 1 2 3))
                   (max-idx 1))
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer :initial-contents vec-1)
                 (= max-idx (gsl-array:max-index v1))))))

;; ----------------------------------------------------------------------

(deftest "min-index-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4.0d0 5.0d0 1.0d0 2.0d0 3.0d0))
                   (min-idx 2))
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float :initial-contents vec-1)
                 (= min-idx (gsl-array:min-index v1))))))

(deftest "min-index-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4.0 5.0 1.0 2.0 3.0))
                   (min-idx 2))
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float :initial-contents vec-1)
                 (= min-idx (gsl-array:min-index v1))))))

(deftest "min-index-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4 5 1 2 3))
                   (min-idx 2))
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer :initial-contents vec-1)
                 (= min-idx (gsl-array:min-index v1))))))

;; ----------------------------------------------------------------------

(deftest "min-max-indicies-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4.0d0 5.0d0 1.0d0 2.0d0 3.0d0))
                   (min-max-idx '(2 1)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float :initial-contents vec-1)
                 (equal min-max-idx (gsl-array:min-max-indicies v1))))))

(deftest "min-max-indicies-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4.0 5.0 1.0 2.0 3.0))
                   (min-max-idx '(2 1)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float :initial-contents vec-1)
                 (equal min-max-idx (gsl-array:min-max-indicies v1))))))

(deftest "min-max-indicies-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4 5 1 2 3))
                   (min-max-idx '(2 1)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer :initial-contents vec-1)
                 (equal min-max-idx (gsl-array:min-max-indicies v1))))))

;; ----------------------------------------------------------------------

(deftest "min-max-values-double-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4.0d0 5.0d0 1.0d0 2.0d0 3.0d0))
                   (min-max-val '(1.0d0 5.0d0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'double-float :initial-contents vec-1)
                 (equal min-max-val (gsl-array:min-max-values v1))))))

(deftest "min-max-values-single-float" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4.0 5.0 1.0 2.0 3.0))
                   (min-max-val '(1.0 5.0)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'single-float :initial-contents vec-1)
                 (equal min-max-val (gsl-array:min-max-values v1))))))

(deftest "min-max-values-integer" :category +vector+
         :test-fn
         #'(lambda ()
             (let ((vec-1 (vector 4 5 1 2 3))
                   (min-max-val '(1 5)))
               (gsl-array:with-vector
                   (v1 5 :element-type 'integer :initial-contents vec-1)
                 (equal min-max-val (gsl-array:min-max-values v1))))))

