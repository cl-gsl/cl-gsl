;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-user)

(defpackage #:cl-gsl-test-system (:use #:cl #:asdf))

(in-package :cl-gsl-test-system)

(defsystem cl-gsl-test
    :name "cl-gsl-test"
    :version "0.2"
    :maintainer "Edgar Denny <edgardenny@comcast.net>"
    :licence "GPL"
    :description "unit tests for GSL interface"
    :depends-on (:clunit :cl-gsl)
    :components
    ((:file "package")
     (:file "tolerance" :depends-on ("package"))
     (:file "test-sf" :depends-on ("tolerance"))
     (:file "test-poly" :depends-on ("tolerance"))
     (:file "test-vector" :depends-on ("package"))
     (:file "test-matrix" :depends-on ("package"))
     (:file "test-permutation" :depends-on ("package"))
     (:file "test-sort" :depends-on ("package"))
     ))
