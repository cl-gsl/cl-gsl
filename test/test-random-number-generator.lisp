;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl-test)

(defconstant +rng+ "rng")

(defconstant +generator-list+
  (list "borosh13" "coveyou" "cmrg" "fishman18" "fishman20" "fishman2x" "gfsr4" "knuthran" "knuthran2" "lecuyer21" "minstd" "mrg" "mt19937" "mt19937-1999" "mt19937-1998" "r250" "ran0" "ran1" "ran2" "ran3" "rand" "rand48" "random128-bsd" "random128-glibc2" "random128-libc5" "random256-bsd" "random256-glibc2" "random256-libc5" "random32-bsd" "random32-glibc2" "random32-libc5" "random64-bsd" "random64-glibc2" "random64-libc5" "random8-bsd" "random8-glibc2" "random8-libc5" "random-bsd" "random-glibc2" "random-libc5" "randu" "ranf" "ranlux" "ranlux389" "ranlxd1" "ranlxd2" "ranlxs0" "ranlxs1" "ranlxs2" "ranmar" "slatec" "taus" "taus2" "taus113" "transputer" "tt800" "uni" "uni32" "vax" "waterman14" "zuf"))

(defun iterate (name seed num)
  (gsl-rng:with-generator (g name)
    (gsl-rng:seed g seed)
    (dotimes (i (1- num))
      (gsl-rng:get-integer g))
    (gsl-rng:get-integer g)))

(deftest "rand" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "rand" 1 10000) 1910041713)))

(deftest "randu" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "randu" 1 10000) 1623524161)))

(deftest "cmrg" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "cmrg" 1 10000) 719452880)))

(deftest "minstd" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "minstd" 1 10000) 1043618065)))

(deftest "mrg" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "mrg" 1 10000) 2064828650)))

(deftest "taus" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "taus" 1 10000) 2733957125)))

(deftest "taus113" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "taus113" 1 1000) 1925420673)))

(deftest "transputer" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "transputer" 1 10000) 1244127297)))

(deftest "vax" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "vax" 1 10000) 3051034865)))

(deftest "borosh13" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "borosh13" 1 10000) 2513433025)))

(deftest "fishman18" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "fishman18" 1 10000) 330402013)))

(deftest "fishman2x" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "fishman2x" 1 10000) 540133597)))

(deftest "knuthran2" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "knuthran2" 1 10000) 1084477620)))

(deftest "lecuyer21" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "lecuyer21" 1 10000) 2006618587)))

(deftest "waterman14" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "waterman14" 1 10000) 3776680385)))

(deftest "coveyou" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "coveyou" 6 10000) 1416754246)))

(deftest "fishman20" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "fishman20" 6 10000) 248127575)))

(deftest "ranlux" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "ranlux" 314159265 10000) 12077992)))

(deftest "ranlux389" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "ranlux389" 314159265 10000) 165942)))

(deftest "ranlxs0" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "ranlxs0" 1 10000) 11904320)))

(deftest "ranlxs1" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "ranlxs1" 1 10000) 8734328)))

(deftest "ranlxs2" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "ranlxs2" 1 10000) 6843140) ))

(deftest "ranlxd1" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "ranlxd1" 1 10000) 1998227290)))

(deftest "ranlxd2" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "ranlxd2" 1 10000) 3949287736)))

(deftest "slatec" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "slatec" 1 10000) 45776)))

(deftest "uni" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "uni" 1 10000) 9214)))

(deftest "uni32" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "uni32" 1 10000) 1155229825)))

(deftest "zuf" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "zuf" 1 10000) 3970)))

(deftest "r250" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "r250" 1 10000) 1100653588)))

(deftest "mt19937" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "mt19937" 4357 1000) 1186927261)))

(deftest "mt19937-1999" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "mt19937-1999" 4357 1000) 1030650439)))

(deftest "mt19937-1998" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "mt19937-1998" 4357 1000) 1309179303)))

(deftest "tt800" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "tt800" 0 10000) 2856609219)))

(deftest "ran0" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "ran0" 0 10000) 1115320064)))

(deftest "ran1" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "ran1" 0 10000) 1491066076)))

(deftest "ran2" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "ran2" 0 10000) 1701364455)))

(deftest "ran3" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "ran3" 0 10000) 186340785)))

(deftest "ranmar" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "ranmar" 1 10000) 14428370)))

(deftest "random-glibc2" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random-glibc2" 0 10000) 1908609430)))

(deftest "random8-glibc2" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random8-glibc2" 0 10000) 1910041713)))

(deftest "random32-glibc2" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random32-glibc2" 0 10000) 1587395585)))

(deftest "random64-glibc2" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random64-glibc2" 0 10000) 52848624)))

(deftest "random128-glibc2" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random128-glibc2" 0 10000) 1908609430)))

(deftest "random256-glibc2" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random256-glibc2" 0 10000) 179943260)))

(deftest "random-bsd" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random-bsd" 0 10000) 1457025928)))

(deftest "random8-bsd" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random8-bsd" 0 10000) 1910041713)))

(deftest "random32-bsd" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random32-bsd" 0 10000) 1663114331)))

(deftest "random64-bsd" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random64-bsd" 0 10000) 864469165)))

(deftest "random128-bsd" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random128-bsd" 0 10000) 1457025928)))

(deftest "random256-bsd" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random256-bsd" 0 10000) 1216357476)))

(deftest "random-libc5" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random-libc5" 0 10000) 428084942)))

(deftest "random8-libc5" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random8-libc5" 0 10000) 1910041713)))

(deftest "random32-libc5" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random32-libc5" 0 10000) 1967452027)))

(deftest "random64-libc5" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random64-libc5" 0 10000) 2106639801)))

(deftest "random128-libc5" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random128-libc5" 0 10000) 428084942)))

(deftest "random256-libc5" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "random256-libc5" 0 10000) 116367984)))

(deftest "ranf" :category +rng+
         :test-fn
         #'(lambda ()
             (= (iterate "ranf" 2 10000) 339327233)))
