;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl-test)

(defconstant +matrix+ "matrix")

(defconstant +array-double-float+
  (make-array '(5 5) :element-type 'double-float
                     :initial-contents '((1.0d0 2.0d0 3.0d0 4.0d0 5.0d0)
                                         (6.0d0 7.0d0 8.0d0 9.0d0 -1.0d0)
                                         (-2.0d0 -3.0d0 -4.0d0 -5.0d0 -6.0d0)
                                         (-7.0d0 -8.0d0 -9.0d0 1.0d0 2.0d0)
                                         (3.0d0 4.0d0 5.0d0 6.0d0 7.0d0))))

(defconstant +vector-row-2-double-float+
  (vector -2.0d0 -3.0d0 -4.0d0 -5.0d0 -6.0d0))

(defconstant +vector-col-2-double-float+
  (vector 3.0d0 8.0d0 -4.0d0 -9.0d0 5.0d0))

(defconstant +array-single-float+
  (make-array '(5 5) :element-type 'single-float
                     :initial-contents '((1.0 2.0 3.0 4.0 5.0)
                                         (6.0 7.0 8.0 9.0 -1.0)
                                         (-2.0 -3.0 -4.0 -5.0 -6.0)
                                         (-7.0 -8.0 -9.0 1.0 2.0)
                                         (3.0 4.0 5.0 6.0 7.0))))

(defconstant +vector-row-2-single-float+
  (vector -2.0 -3.0 -4.0 -5.0 -6.0))

(defconstant +vector-col-2-single-float+
  (vector 3.0 8.0 -4.0 -9.0 5.0))

(defconstant +array-integer+
  (make-array '(5 5) :element-type 'integer
                     :initial-contents '((1 2 3 4 5)
                                         (6 7 8 9 -1)
                                         (-2 -3 -4 -5 -6)
                                         (-7 -8 -9 1 2)
                                         (3 4 5 6 7))))

(defconstant +vector-row-2-integer+
  (vector -2 -3 -4 -5 -6))

(defconstant +vector-col-2-integer+
  (vector 3 8 -4 -9 5))

(defconstant +array-complex-double-float+
  (make-array '(5 5) :element-type '(complex (double-float))
                     :initial-contents
                     (list (list (complex 1.0d0 2.0d0) (complex 2.0d0 3.0d0)
                                 (complex 3.0d0 4.0d0) (complex 4.0d0 5.0d0)
                                 (complex 5.0d0 6.0d0))
                           (list (complex 6.0d0 7.0d0) (complex 7.0d0 8.0d0)
                                 (complex 9.0d0 9.0d0) (complex 9.0d0 -1.0d0)
                                 (complex -1.0d0 -2.0d0))
                           (list (complex -2.0d0 -3.0d0) (complex -3.0d0 -4.0d0)
                                 (complex -4.0d0 -5.0d0) (complex -5.0d0 -6.0d0)
                                 (complex -6.0d0 -7.0d0))
                           (list (complex -7.0d0 -8.0d0) (complex -8.0d0 -9.0d0)
                                 (complex -9.0d0 1.0d0) (complex 1.0d0 2.0d0)
                                 (complex 2.0d0 3.0d0))
                           (list (complex 3.0d0 4.0d0) (complex 4.0d0 5.0d0)
                                 (complex 5.0d0 6.0d0) (complex 6.0d0 7.0d0)
                                 (complex 7.0d0 8.0d0)))))

(defconstant +vector-row-2-complex-double-float+
  (vector (complex -2.0d0 -3.0d0) (complex -3.0d0 -4.0d0)
          (complex -4.0d0 -5.0d0) (complex -5.0d0 -6.0d0)
          (complex -6.0d0 -7.0d0)))

(defconstant +vector-col-2-complex-double-float+
  (vector (complex 3.0d0 4.0d0) (complex 9.0d0 9.0d0) (complex -4.0d0 -5.0d0)
          (complex -9.0d0 1.0d0) (complex 5.0d0 6.0d0)))

(defconstant +array-complex-single-float+
  (make-array '(5 5) :element-type '(complex (single-float))
                     :initial-contents
                     (list (list (complex 1.0 2.0) (complex 2.0 3.0)
                                 (complex 3.0 4.0) (complex 4.0 5.0)
                                 (complex 5.0 6.0))
                           (list (complex 6.0 7.0) (complex 7.0 8.0)
                                 (complex 9.0 9.0) (complex 9.0 -1.0)
                                 (complex -1.0 -2.0))
                           (list (complex -2.0 -3.0) (complex -3.0 -4.0)
                                 (complex -4.0 -5.0) (complex -5.0 -6.0)
                                 (complex -6.0 -7.0))
                           (list (complex -7.0 -8.0) (complex -8.0 -9.0)
                                 (complex -9.0 1.0) (complex 1.0 2.0)
                                 (complex 2.0 3.0))
                           (list (complex 3.0 4.0) (complex 4.0 5.0)
                                 (complex 5.0 6.0) (complex 6.0 7.0)
                                 (complex 7.0 8.0)))))

(defconstant +vector-row-2-complex-single-float+
  (vector (complex -2.0 -3.0) (complex -3.0 -4.0) (complex -4.0 -5.0)
          (complex -5.0 -6.0) (complex -6.0 -7.0)))

(defconstant +vector-col-2-complex-single-float+
  (vector (complex 3.0 4.0) (complex 9.0 9.0) (complex -4.0 -5.0)
          (complex -9.0 1.0) (complex 5.0 6.0)))

;; ----------------------------------------------------------------------

(deftest "make-matrix-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((m (gsl-array:make-matrix 5 4 :element-type 'double-float)))
               (prog1
                   (and (= (gsl-array::size-rows m) 5)
                        (= (gsl-array::size-cols m) 4)
                        (eq (gsl-array::element-type m) 'double-float))
                 (gsl-array:free m)))))


(deftest "make-matrix-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((m (gsl-array:make-matrix 5 4 :element-type 'single-float)))
               (prog1
                   (and (= (gsl-array::size-rows m) 5)
                        (= (gsl-array::size-cols m) 4)
                        (eq (gsl-array::element-type m) 'single-float))
                 (gsl-array:free m)))))

(deftest "make-matrix-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((m (gsl-array:make-matrix 5 4 :element-type 'integer)))
               (prog1
                   (and (= (gsl-array::size-rows m) 5)
                        (= (gsl-array::size-cols m) 4)
                        (eq (gsl-array::element-type m) 'integer))
                 (gsl-array:free m)))))

(deftest "make-matrix-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((m (gsl-array:make-matrix 5 4 :element-type
                                             '(complex (double-float)))))
               (prog1
                   (and (= (gsl-array::size-rows m) 5)
                        (= (gsl-array::size-cols m) 4)
                        (equal (gsl-array::element-type m)
                               '(complex (double-float))))
                 (gsl-array:free m)))))

(deftest "make-matrix-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((m (gsl-array:make-matrix 5 4 :element-type
                                             '(complex (single-float)))))
               (prog1
                   (and (= (gsl-array::size-rows m) 5)
                        (= (gsl-array::size-cols m) 4)
                        (equal (gsl-array::element-type m)
                               '(complex (single-float))))
                 (gsl-array:free m)))))

;; ----------------------------------------------------------------------

(deftest "make-matrix-double-float-initial-element" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((m (gsl-array:make-matrix 5 5 :element-type 'double-float
                                             :initial-element 1.0d0))
                   (ret t))
               (dotimes (i 5)
                 (dotimes (j 5)
                   (unless (= (gsl-array:get-element m i j) 1.0d0)
                     (setq ret nil))))
               (gsl-array:free m)
               ret)))


(deftest "make-matrix-single-float-initial-element" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((m (gsl-array:make-matrix 5 5 :element-type 'single-float
                                             :initial-element 1.0))
                   (ret t))
               (dotimes (i 5)
                 (dotimes (j 5)
                   (unless (= (gsl-array:get-element m i j) 1.0)
                     (setq ret nil))))
               (gsl-array:free m)
               ret)))


(deftest "make-matrix-integer-initial-element" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((m (gsl-array:make-matrix 5 5 :element-type 'integer
                                             :initial-element 1))
                   (ret t))
               (dotimes (i 5)
                 (dotimes (j 5)
                   (unless (= (gsl-array:get-element m i j) 1)
                     (setq ret nil))))
               (gsl-array:free m)
               ret)))

(deftest "make-matrix-complex-double-float-initial-element" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((m (gsl-array:make-matrix 5 5
                                             :element-type
                                             '(complex (double-float))
                                             :initial-element
                                             (complex 1.0d0 1.0d0)))
                   (ret t))
               (dotimes (i 5)
                 (dotimes (j 5)
                   (unless (= (gsl-array:get-element m i j)
                              (complex 1.0d0 1.0d0))
                     (setq ret nil))))
               (gsl-array:free m)
               ret)))

(deftest "make-matrix-complex-single-float-initial-element" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((m (gsl-array:make-matrix 5 5
                                             :element-type
                                             '(complex (single-float))
                                             :initial-element
                                             (complex 1.0 1.0)))
                   (ret t))
               (dotimes (i 5)
                 (dotimes (j 5)
                   (unless (= (gsl-array:get-element m i j) (complex 1.0 1.0))
                     (setq ret nil))))
               (gsl-array:free m)
               ret)))

;; ----------------------------------------------------------------------

(deftest "make-matrix-double-float-initial-contents" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((m (gsl-array:make-matrix 5 5
                         :element-type 'double-float
                         :initial-contents +array-double-float+))
                   (ret t))
               (dotimes (i 5)
                 (dotimes (j 5)
                   (unless (= (gsl-array:get-element m i j)
                              (aref +array-double-float+ i j))
                     (setq ret nil))))
               (gsl-array:free m)
               ret)))


(deftest "make-matrix-single-float-initial-contents" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((m (gsl-array:make-matrix 5 5
                         :element-type 'single-float
                         :initial-contents +array-single-float+))
                   (ret t))
               (dotimes (i 5)
                 (dotimes (j 5)
                 (unless (= (gsl-array:get-element m i j)
                            (aref +array-single-float+ i j))
                   (setq ret nil))))
               (gsl-array:free m)
               ret)))


(deftest "make-matrix-integer-initial-contents" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((m (gsl-array:make-matrix 5 5
                         :element-type 'integer
                         :initial-contents +array-integer+))
                   (ret t))
               (dotimes (i 5)
                 (dotimes (j 5)
                   (unless (= (gsl-array:get-element m i j)
                              (aref +array-integer+ i j))
                     (setq ret nil))))
               (gsl-array:free m)
               ret)))

(deftest "make-matrix-complex-double-float-initial-contents" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((m (gsl-array:make-matrix 5 5
                         :element-type '(complex (double-float))
                         :initial-contents +array-complex-double-float+))
                   (ret t))
               (dotimes (i 5)
                 (dotimes (j 5)
                   (unless (= (gsl-array:get-element m i j)
                              (aref +array-complex-double-float+ i j))
                     (setq ret nil))))
               (gsl-array:free m)
               ret)))


(deftest "make-matrix-complex-single-float-initial-contents" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((m (gsl-array:make-matrix 5 5
                         :element-type '(complex (single-float))
                         :initial-contents +array-complex-single-float+))
                   (ret t))
               (dotimes (i 5)
                 (dotimes (j 5)
                   (unless (= (gsl-array:get-element m i j)
                              (aref +array-complex-single-float+ i j))
                     (setq ret nil))))
               (gsl-array:free m)
               ret)))

;; ----------------------------------------------------------------------

(deftest "set-all-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m 5 5 :element-type 'double-float)
               (gsl-array:set-all m 5.0d0)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (unless (= (gsl-array:get-element m i j) 5.0d0)
                       (setq ret nil))))))))


(deftest "set-all-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m 5 5 :element-type 'single-float)
               (gsl-array:set-all m 5.0)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (unless (= (gsl-array:get-element m i j) 5.0)
                       (setq ret nil))))))))

(deftest "set-all-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m 5 5 :element-type 'integer)
               (gsl-array:set-all m 5)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (unless (= (gsl-array:get-element m i j) 5)
                       (setq ret nil))))))))

(deftest "set-all-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m 5 5 :element-type '(complex (double-float)))
               (gsl-array:set-all m (complex 5.0d0 4.0d0))
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (unless (= (gsl-array:get-element m i j)
                                (complex 5.0d0 4.0d0))
                     (setq ret nil))))))))

(deftest "set-all-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m 5 5 :element-type '(complex (single-float)))
               (gsl-array:set-all m (complex 5.0 4.0))
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (unless (= (gsl-array:get-element m i j) (complex 5.0 4.0))
                       (setq ret nil))))))))

;; ----------------------------------------------------------------------

(deftest "set-zero-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m 5 5 :element-type 'double-float)
               (gsl-array:set-all m 5.0d0)
               (gsl-array:set-zero m)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (unless (= (gsl-array:get-element m i j) 0.0d0)
                       (setq ret nil))))))))


(deftest "set-zero-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m 5 5 :element-type 'single-float)
               (gsl-array:set-all m 5.0)
               (gsl-array:set-zero m)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (unless (= (gsl-array:get-element m i j) 0.0)
                       (setq ret nil))))))))

(deftest "set-zero-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m 5 5 :element-type 'integer)
               (gsl-array:set-all m 5)
               (gsl-array:set-zero m)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (unless (= (gsl-array:get-element m i j) 0)
                       (setq ret nil))))))))

(deftest "set-zero-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m 5 5 :element-type '(complex (double-float)))
               (gsl-array:set-all m (complex 5.0d0 4.0d0))
               (gsl-array:set-zero m)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (unless (= (gsl-array:get-element m i j)
                                (complex 0.0d0 0.0d0))
                       (setq ret nil))))))))

(deftest "set-zero-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m 5 5 :element-type '(complex (single-float)))
               (gsl-array:set-all m (complex 5.0 4.0))
               (gsl-array:set-zero m)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (unless (= (gsl-array:get-element m i j) (complex 0.0 0.0))
                       (setq ret nil))))))))

;; ----------------------------------------------------------------------

(deftest "set-identity-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m 5 5 :element-type 'double-float)
               (gsl-array:set-identity m)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (if (= i j)
                         (unless (= (gsl-array:get-element m i j) 1.0d0)
                           (setq ret nil))
                         (unless (= (gsl-array:get-element m i j) 0.0d0)
                           (setq ret nil)))))))))

(deftest "set-identity-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m 5 5 :element-type 'single-float)
               (gsl-array:set-identity m)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (if (= i j)
                         (unless (= (gsl-array:get-element m i j) 1.0)
                           (setq ret nil))
                         (unless (= (gsl-array:get-element m i j) 0.0)
                           (setq ret nil)))))))))

(deftest "set-identity-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m 5 5 :element-type 'integer)
               (gsl-array:set-identity m)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (if (= i j)
                         (unless (= (gsl-array:get-element m i j) 1)
                           (setq ret nil))
                         (unless (= (gsl-array:get-element m i j) 0)
                           (setq ret nil)))))))))

(deftest "set-identity-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m 5 5 :element-type '(complex (double-float)))
               (gsl-array:set-identity m)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (if (= i j)
                         (unless (= (gsl-array:get-element m i j)
                                    (complex 1.0d0 0.0d0))
                           (setq ret nil))
                         (unless (= (gsl-array:get-element m i j)
                                    (complex 0.0d0 0.0d0))
                           (setq ret nil)))))))))

(deftest "set-identity-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m 5 5 :element-type '(complex (single-float)))
               (gsl-array:set-identity m)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (if (= i j)
                         (unless (= (gsl-array:get-element m i j)
                                    (complex 1.0 0.0))
                           (setq ret nil))
                         (unless (= (gsl-array:get-element m i j)
                                    (complex 0.0 0.0))
                           (setq ret nil)))))))))


;; ----------------------------------------------------------------------

(deftest "set-element-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m 5 5 :element-type 'double-float)
               (gsl-array:set-zero m)
               (gsl-array:set-element m 3 4 6.0d0)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (if (and (= i 3) (= j 4))
                         (unless (= (gsl-array:get-element m i j) 6.0d0)
                           (setq ret nil))
                         (unless (= (gsl-array:get-element m i j) 0.0d0)
                           (setq ret nil)))))))))


(deftest "set-element-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m 5 5 :element-type 'single-float)
               (gsl-array:set-zero m)
               (gsl-array:set-element m 2 1 6.0)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (if (and (= i 2) (= j 1))
                         (unless (= (gsl-array:get-element m i j) 6.0)
                           (setq ret nil))
                         (unless (= (gsl-array:get-element m i j) 0.0)
                           (setq ret nil)))))))))

(deftest "set-element-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m 5 5 :element-type 'integer)
               (gsl-array:set-zero m)
               (gsl-array:set-element m 1 1 6)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (if (and (= i 1) (= j 1))
                         (unless (= (gsl-array:get-element m i j) 6)
                           (setq ret nil))
                         (unless (= (gsl-array:get-element m i j) 0)
                           (setq ret nil)))))))))

(deftest "set-element-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m 5 5 :element-type '(complex (double-float)))
               (gsl-array:set-zero m)
               (gsl-array:set-element m 4 2 (complex 6.0d0 7.0d0))
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (if (and (= i 4) (= j 2))
                         (unless (= (gsl-array:get-element m i j)
                                    (complex 6.0d0 7.0d0))
                           (setq ret nil))
                         (unless (= (gsl-array:get-element m i j)
                                    (complex 0.0d0 0.0d0))
                           (setq ret nil)))))))))

(deftest "set-element-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m 5 5 :element-type '(complex (single-float)))
               (gsl-array:set-zero m)
               (gsl-array:set-element m 0 0 (complex 6.0 7.0))
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (if (and (= i 0) (= j 0))
                         (unless (= (gsl-array:get-element m i j)
                                    (complex 6.0 7.0))
                           (setq ret nil))
                         (unless (= (gsl-array:get-element m i j)
                                    (complex 0.0 0.0))
                           (setq ret nil)))))))))

;; ----------------------------------------------------------------------

(deftest "isnull-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m 5 5 :element-type 'double-float)
               (gsl-array:set-zero m)
               (gsl-array:isnull m))))


(deftest "isnull-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m 5 5 :element-type 'single-float)
               (gsl-array:set-identity m)
               ;; check for failure of isnull
               (not (gsl-array:isnull m)))))


(deftest "isnull-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m 5 5 :element-type 'integer)
               (gsl-array:set-identity m)
               (not (gsl-array:isnull m)))))

(deftest "isnull-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m 5 5 :element-type '(complex (double-float)))
               (gsl-array:set-zero m)
               (gsl-array:isnull m))))

(deftest "isnull-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m 5 5 :element-type '(complex (single-float)))
               (gsl-array:set-zero m)
               (gsl-array:isnull m))))

;; ----------------------------------------------------------------------

(deftest "read-write-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-double-float+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'double-float :initial-contents a1)
                 (gsl-array:write-to-file "/tmp/test.txt" m1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'double-float
                         :from-file "/tmp/test.txt")
                   (and (equalp a1 (gsl-array:gsl-matrix->lisp-array m1))
                        (equalp a1 (gsl-array:gsl-matrix->lisp-array m2))))))))

(deftest "read-write-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-single-float+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'single-float :initial-contents a1)
                 (gsl-array:write-to-file "/tmp/test.txt" m1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'single-float
                         :from-file "/tmp/test.txt")
                   (and (equalp a1 (gsl-array:gsl-matrix->lisp-array m1))
                        (equalp a1 (gsl-array:gsl-matrix->lisp-array m2))))))))

(deftest "read-write-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-integer+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'integer :initial-contents a1)
                 (gsl-array:write-to-file "/tmp/test.txt" m1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'integer
                         :from-file "/tmp/test.txt")
                   (and (equalp a1 (gsl-array:gsl-matrix->lisp-array m1))
                        (equalp a1 (gsl-array:gsl-matrix->lisp-array m2))))))))

(deftest "read-write-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-complex-double-float+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type '(complex (double-float))
                       :initial-contents a1)
                 (gsl-array:write-to-file "/tmp/test.txt" m1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type '(complex (double-float))
                         :from-file "/tmp/test.txt")
                   (and (equalp a1 (gsl-array:gsl-matrix->lisp-array m1))
                        (equalp a1 (gsl-array:gsl-matrix->lisp-array m2))))))))

(deftest "read-write-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-complex-single-float+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type '(complex (single-float))
                       :initial-contents a1)
                 (gsl-array:write-to-file "/tmp/test.txt" m1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type '(complex (single-float))
                         :from-file "/tmp/test.txt")
                   (and (equalp a1 (gsl-array:gsl-matrix->lisp-array m1))
                        (equalp a1 (gsl-array:gsl-matrix->lisp-array m2))))))))


;; ----------------------------------------------------------------------

(deftest "read-write-binary-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-double-float+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'double-float :initial-contents a1)
                 (gsl-array:write-to-binary-file "/tmp/test.bin" m1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'double-float
                         :from-binary-file "/tmp/test.bin")
                   (and (equalp a1 (gsl-array:gsl-matrix->lisp-array m1))
                        (equalp a1 (gsl-array:gsl-matrix->lisp-array m2))))))))


(deftest "read-write-binary-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-single-float+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'single-float :initial-contents a1)
                 (gsl-array:write-to-binary-file "/tmp/test.bin" m1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'single-float
                         :from-binary-file "/tmp/test.bin")
                   (and (equalp a1 (gsl-array:gsl-matrix->lisp-array m1))
                        (equalp a1 (gsl-array:gsl-matrix->lisp-array m2))))))))

(deftest "read-write-binary-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-integer+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'integer :initial-contents a1)
                 (gsl-array:write-to-binary-file "/tmp/test.bin" m1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'integer
                         :from-binary-file "/tmp/test.bin")
                   (and (equalp a1 (gsl-array:gsl-matrix->lisp-array m1))
                        (equalp a1 (gsl-array:gsl-matrix->lisp-array m2))))))))

(deftest "read-write-binary-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-complex-double-float+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type '(complex (double-float))
                       :initial-contents a1)
                 (gsl-array:write-to-binary-file "/tmp/test.bin" m1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type '(complex (double-float))
                         :from-binary-file "/tmp/test.bin")
                   (and (equalp a1 (gsl-array:gsl-matrix->lisp-array m1))
                        (equalp a1 (gsl-array:gsl-matrix->lisp-array m2))))))))

(deftest "read-write-binary-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-complex-single-float+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type '(complex (single-float))
                       :initial-contents a1)
                 (gsl-array:write-to-binary-file "/tmp/test.bin" m1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type '(complex (single-float))
                         :from-binary-file "/tmp/test.bin")
                   (and (equalp a1 (gsl-array:gsl-matrix->lisp-array m1))
                        (equalp a1 (gsl-array:gsl-matrix->lisp-array m2))))))))


;; ----------------------------------------------------------------------

(deftest "copy-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-double-float+)
                   (m2))
               (prog1
                   (gsl-array:with-matrix
                       (m1 5 5 :element-type 'double-float :initial-contents a1)
                     (setq m2 (gsl-array:copy m1))
                     (and (equalp (gsl-array:gsl-matrix->lisp-array m1) a1)
                          (equalp (gsl-array:gsl-matrix->lisp-array m2) a1)))
                 (gsl-array:free m2)))))

(deftest "copy-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-single-float+)
                   (m2))
               (prog1
                   (gsl-array:with-matrix
                       (m1 5 5 :element-type 'single-float :initial-contents a1)
                     (setq m2 (gsl-array:copy m1))
                     (and (equalp (gsl-array:gsl-matrix->lisp-array m1) a1)
                          (equalp (gsl-array:gsl-matrix->lisp-array m2) a1)))
                 (gsl-array:free m2)))))

(deftest "copy-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-integer+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'integer :initial-contents a1)
                 (gsl-array:with-matrix-copy (m2 m1)
                   (and (equalp (gsl-array:gsl-matrix->lisp-array m1) a1)
                        (equalp (gsl-array:gsl-matrix->lisp-array m2) a1)))))))

(deftest "copy-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-complex-double-float+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type '(complex (double-float))
                       :initial-contents a1)
                 (gsl-array:with-matrix-copy (m2 m1)
                   (and (equalp (gsl-array:gsl-matrix->lisp-array m1) a1)
                        (equalp (gsl-array:gsl-matrix->lisp-array m2) a1)))))))

(deftest "copy-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-complex-single-float+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type '(complex (single-float))
                       :initial-contents a1)
                 (gsl-array:with-matrix-copy (m2 m1)
                   (and (equalp (gsl-array:gsl-matrix->lisp-array m1) a1)
                        (equalp (gsl-array:gsl-matrix->lisp-array m2) a1)))))))


;; ----------------------------------------------------------------------

(deftest "swap-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-double-float+)
                   (a2 (make-array '(5 5) :element-type 'double-float
                                   :initial-element 1.0d0)))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'double-float :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'double-float :initial-element 1.0d0)
                   (gsl-array:swap m1 m2)
                   (and (equalp a1 (gsl-array:gsl-matrix->lisp-array m2))
                        (equalp a2 (gsl-array:gsl-matrix->lisp-array m1))))))))

(deftest "swap-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-single-float+)
                   (a2 (make-array '(5 5) :element-type 'single-float
                                   :initial-element 1.0)))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'single-float :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'single-float :initial-element 1.0)
                   (gsl-array:swap m1 m2)
                   (and (equalp a1 (gsl-array:gsl-matrix->lisp-array m2))
                        (equalp a2 (gsl-array:gsl-matrix->lisp-array m1))))))))

(deftest "swap-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-integer+)
                   (a2 (make-array '(5 5) :element-type 'integer
                                   :initial-element 1)))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'integer :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'integer :initial-element 1)
                   (gsl-array:swap m1 m2)
                   (and (equalp a1 (gsl-array:gsl-matrix->lisp-array m2))
                        (equalp a2 (gsl-array:gsl-matrix->lisp-array m1))))))))

(deftest "swap-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-complex-double-float+)
                   (a2 (make-array '(5 5)
                                   :element-type '(complex (double-float))
                                   :initial-element (complex 1.0d0 1.0d0))))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type '(complex (double-float))
                       :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type '(complex (double-float))
                         :initial-element (complex 1.0d0 1.0d0))
                   (gsl-array:swap m1 m2)
                   (and (equalp a1 (gsl-array:gsl-matrix->lisp-array m2))
                        (equalp a2 (gsl-array:gsl-matrix->lisp-array m1))))))))

(deftest "swap-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-complex-single-float+)
                   (a2 (make-array '(5 5)
                                   :element-type '(complex (single-float))
                                   :initial-element (complex 1.0 1.0))))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type '(complex (single-float))
                       :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type '(complex (single-float))
                         :initial-element (complex 1.0 1.0))
                   (gsl-array:swap m1 m2)
                   (and (equalp a1 (gsl-array:gsl-matrix->lisp-array m2))
                        (equalp a2 (gsl-array:gsl-matrix->lisp-array m1))))))))


;; ----------------------------------------------------------------------

(deftest "add-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-double-float+)
                   (ret t))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'double-float :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'double-float :initial-contents a1)
                   (gsl-array:add m1 m2)
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (when (/= (gsl-array:get-element m1 i j)
                                 (* 2.0d0 (aref a1 i j)))
                         (setq ret nil)))))))))


(deftest "add-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-single-float+)
                   (ret t))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'single-float :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'single-float :initial-contents a1)
                   (gsl-array:add m1 m2)
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (when (/= (gsl-array:get-element m1 i j)
                                 (* 2.0 (aref a1 i j)))
                         (setq ret nil)))))))))

(deftest "add-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-integer+)
                   (ret t))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'integer :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'integer :initial-contents a1)
                   (gsl-array:add m1 m2)
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (when (/= (gsl-array:get-element m1 i j)
                                 (* 2 (aref a1 i j)))
                         (setq ret nil)))))))))


;; ----------------------------------------------------------------------

(deftest "sub-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-double-float+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'double-float :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'double-float :initial-contents a1)
                   (gsl-array:sub m1 m2)
                   (gsl-array:isnull m1))))))

(deftest "sub-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-single-float+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'single-float :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'single-float :initial-contents a1)
                   (gsl-array:sub m1 m2)
                   (gsl-array:isnull m1))))))

(deftest "sub-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-integer+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'integer :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'integer :initial-contents a1)
                   (gsl-array:sub m1 m2)
                   (gsl-array:isnull m1))))))

;; ----------------------------------------------------------------------

(deftest "mul-elements-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-double-float+)
                   (ret t))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'double-float :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'double-float :initial-element 5.0d0)
                   (gsl-array:mul-elements m1 m2)
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (when (/= (gsl-array:get-element m1 i j)
                                 (* 5.0d0 (aref a1 i j)))
                         (setq ret nil)))))))))

(deftest "mul-elements-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-single-float+)
                   (ret t))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'single-float :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'single-float :initial-element 5.0)
                   (gsl-array:mul-elements m1 m2)
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (when (/= (gsl-array:get-element m1 i j)
                                 (* 5.0 (aref a1 i j)))
                         (setq ret nil)))))))))

(deftest "mul-elements-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-integer+)
                   (ret t))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'integer :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'integer :initial-element 5)
                   (gsl-array:mul-elements m1 m2)
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (when (/= (gsl-array:get-element m1 i j)
                                 (* 5 (aref a1 i j)))
                         (setq ret nil)))))))))

;; ----------------------------------------------------------------------

(deftest "div-elements-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-double-float+)
                   (a2 (make-array '(5 5) :element-type 'double-float
                                   :initial-element 1.0d0)))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'double-float :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'double-float :initial-contents a1)
                   (gsl-array:div-elements m1 m2)
                   (equalp (gsl-array:gsl-matrix->lisp-array m1) a2))))))


(deftest "div-elements-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-single-float+)
                   (a2 (make-array '(5 5) :element-type 'single-float
                                   :initial-element 1.0)))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'single-float :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'single-float :initial-contents a1)
                   (gsl-array:div-elements m1 m2)
                   (equalp (gsl-array:gsl-matrix->lisp-array m1) a2))))))

(deftest "div-elements-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-integer+)
                   (a2 (make-array '(5 5) :element-type 'integer
                                   :initial-element 1)))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'integer :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'integer :initial-contents a1)
                   (gsl-array:div-elements m1 m2)
                   (equalp (gsl-array:gsl-matrix->lisp-array m1) a2))))))

;; ----------------------------------------------------------------------

(deftest "scale-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-double-float+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'double-float :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'double-float :initial-element 10.0d0)
                   (gsl-array:mul-elements m2 m1)
                   (gsl-array:scale m1 10.0d0)
                 (equalp (gsl-array:gsl-matrix->lisp-array m1)
                         (gsl-array:gsl-matrix->lisp-array m2)))))))

(deftest "scale-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-single-float+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'single-float :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'single-float :initial-element 10.0)
                   (gsl-array:mul-elements m2 m1)
                   (gsl-array:scale m1 10.0)
                 (equalp (gsl-array:gsl-matrix->lisp-array m1)
                         (gsl-array:gsl-matrix->lisp-array m2)))))))

(deftest "scale-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-integer+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'integer :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'integer :initial-element 10)
                   (gsl-array:mul-elements m2 m1)
                   (gsl-array:scale m1 10)
                 (equalp (gsl-array:gsl-matrix->lisp-array m1)
                         (gsl-array:gsl-matrix->lisp-array m2)))))))

;; ----------------------------------------------------------------------

(deftest "add-constant-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-double-float+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'double-float :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'double-float :initial-element 3.0d0)
                   (gsl-array:add m2 m1)
                   (gsl-array:add-constant m1 3.0d0)
                 (equalp (gsl-array:gsl-matrix->lisp-array m1)
                         (gsl-array:gsl-matrix->lisp-array m2)))))))

(deftest "add-constant-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-single-float+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'single-float :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'single-float :initial-element 3.0)
                   (gsl-array:add m2 m1)
                   (gsl-array:add-constant m1 3.0)
                 (equalp (gsl-array:gsl-matrix->lisp-array m1)
                         (gsl-array:gsl-matrix->lisp-array m2)))))))

(deftest "add-constant-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (let ((a1 +array-integer+))
               (gsl-array:with-matrix
                   (m1 5 5 :element-type 'integer :initial-contents a1)
                 (gsl-array:with-matrix
                     (m2 5 5 :element-type 'integer :initial-element 3)
                   (gsl-array:add m2 m1)
                   (gsl-array:add-constant m1 3)
                 (equalp (gsl-array:gsl-matrix->lisp-array m1)
                         (gsl-array:gsl-matrix->lisp-array m2)))))))

;; ----------------------------------------------------------------------

(deftest "max-value-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'double-float
                                        :initial-contents +array-double-float+)
                 (= 9.0d0 (gsl-array:max-value m1)))))

(deftest "max-value-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'single-float
                                        :initial-contents +array-single-float+)
                 (= 9.0 (gsl-array:max-value m1)))))

(deftest "max-value-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'integer
                                        :initial-contents +array-integer+)
                 (= 9 (gsl-array:max-value m1)))))

;; ----------------------------------------------------------------------

(deftest "min-value-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'double-float
                                        :initial-contents +array-double-float+)
                 (= -9.0d0 (gsl-array:min-value m1)))))

(deftest "min-value-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'single-float
                                        :initial-contents +array-single-float+)
                 (= -9.0 (gsl-array:min-value m1)))))

(deftest "min-value-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'integer
                                        :initial-contents +array-integer+)
                 (= -9 (gsl-array:min-value m1)))))


;; ----------------------------------------------------------------------

(deftest "max-index-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'double-float
                                        :initial-contents +array-double-float+)
                 (equalp '(1 3) (gsl-array:max-index m1)))))

(deftest "max-index-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'single-float
                                        :initial-contents +array-single-float+)
                 (equalp '(1 3) (gsl-array:max-index m1)))))

(deftest "max-index-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'integer
                                        :initial-contents +array-integer+)
                 (equalp '(1 3) (gsl-array:max-index m1)))))

;; ----------------------------------------------------------------------

(deftest "min-index-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'double-float
                                        :initial-contents +array-double-float+)
                 (equalp '(3 2) (gsl-array:min-index m1)))))

(deftest "min-index-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'single-float
                                        :initial-contents +array-single-float+)
                 (equalp '(3 2) (gsl-array:min-index m1)))))

(deftest "min-index-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'integer
                                        :initial-contents +array-integer+)
                 (equalp '(3 2) (gsl-array:min-index m1)))))

;; ----------------------------------------------------------------------

(deftest "min-max-indicies-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'double-float
                                        :initial-contents +array-double-float+)
                 (equalp '((3 2) (1 3)) (gsl-array:min-max-indicies m1)))))

(deftest "min-max-indicies-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'single-float
                                        :initial-contents +array-single-float+)
                 (equalp '((3 2) (1 3)) (gsl-array:min-max-indicies m1)))))

(deftest "min-max-indicies-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'integer
                                        :initial-contents +array-integer+)
                 (equalp '((3 2) (1 3)) (gsl-array:min-max-indicies m1)))))

;; ----------------------------------------------------------------------

(deftest "min-max-values-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'double-float
                                        :initial-contents +array-double-float+)
                 (equalp '(-9.0d0 9.0d0) (gsl-array:min-max-values m1)))))

(deftest "min-max-values-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'single-float
                                        :initial-contents +array-single-float+)
                 (equalp '(-9.0 9.0) (gsl-array:min-max-values m1)))))

(deftest "min-max-values-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'integer
                                        :initial-contents +array-integer+)
                 (equalp '(-9 9) (gsl-array:min-max-values m1)))))

;; ----------------------------------------------------------------------

(deftest "get-row-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'double-float
                                        :initial-contents +array-double-float+)
               (let ((v1 (gsl-array:get-row m1 2)))
                 (prog1
                     (equalp (gsl-array:gsl->lisp-vector v1)
                             +vector-row-2-double-float+)
                   (gsl-array:free v1))))))

(deftest "get-row-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'single-float
                                        :initial-contents +array-single-float+)
               (let ((v1 (gsl-array:get-row m1 2)))
                 (prog1
                     (equalp (gsl-array:gsl->lisp-vector v1)
                             +vector-row-2-single-float+)
                   (gsl-array:free v1))))))

(deftest "get-row-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'integer
                                        :initial-contents +array-integer+)
               (gsl-array:with-matrix-row (v1 m1 2)
                 (equalp (gsl-array:gsl->lisp-vector v1)
                         +vector-row-2-integer+)))))


(deftest "get-row-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type '(complex (double-float))
                     :initial-contents +array-complex-double-float+)
               (let ((v1 (gsl-array:get-row m1 2)))
                 (prog1
                     (equalp (gsl-array:gsl->lisp-vector v1)
                             +vector-row-2-complex-double-float+)
                   (gsl-array:free v1))))))

(deftest "get-row-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type '(complex (single-float))
                     :initial-contents +array-complex-single-float+)
               (let ((v1 (gsl-array:get-row m1 2)))
                 (prog1
                     (equalp (gsl-array:gsl->lisp-vector v1)
                             +vector-row-2-complex-single-float+)
                   (gsl-array:free v1))))))

;; ----------------------------------------------------------------------

(deftest "get-col-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'double-float
                                        :initial-contents +array-double-float+)
               (let ((v1 (gsl-array:get-col m1 2)))
                 (prog1
                     (equalp (gsl-array:gsl->lisp-vector v1)
                             +vector-col-2-double-float+)
                   (gsl-array:free v1))))))

(deftest "get-col-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'single-float
                                        :initial-contents +array-single-float+)
               (let ((v1 (gsl-array:get-col m1 2)))
                 (prog1
                     (equalp (gsl-array:gsl->lisp-vector v1)
                             +vector-col-2-single-float+)
                   (gsl-array:free v1))))))

(deftest "get-col-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'integer
                                        :initial-contents +array-integer+)
               (gsl-array:with-matrix-col (v1 m1 2)
                 (equalp (gsl-array:gsl->lisp-vector v1)
                         +vector-col-2-integer+)))))


(deftest "get-col-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type '(complex (double-float))
                     :initial-contents +array-complex-double-float+)
               (let ((v1 (gsl-array:get-col m1 2)))
                 (prog1
                     (equalp (gsl-array:gsl->lisp-vector v1)
                             +vector-col-2-complex-double-float+)
                   (gsl-array:free v1))))))

(deftest "get-col-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type '(complex (single-float))
                     :initial-contents +array-complex-single-float+)
               (let ((v1 (gsl-array:get-col m1 2)))
                 (prog1
                     (equalp (gsl-array:gsl->lisp-vector v1)
                             +vector-col-2-complex-single-float+)
                   (gsl-array:free v1))))))

;; ----------------------------------------------------------------------

(deftest "set-row-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'double-float
                                        :initial-contents +array-double-float+)
               (gsl-array:with-vector (v1 5 :element-type 'double-float
                                          :initial-element 1.0d0)
                 (gsl-array:set-row m1 2 v1)
                 (let ((ret t))
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (if (= i 2)
                           (when (not (= (gsl-array:get-element m1 i j)
                                         1.0d0))
                             (setq ret nil))
                           (when (not (= (gsl-array:get-element m1 i j)
                                         (aref +array-double-float+ i j)))
                             (setq ret nil))))))))))

(deftest "set-row-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'single-float
                                        :initial-contents +array-single-float+)
               (gsl-array:with-vector (v1 5 :element-type 'single-float
                                          :initial-element 1.0)
                 (gsl-array:set-row m1 2 v1)
                 (let ((ret t))
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (if (= i 2)
                           (when (not (= (gsl-array:get-element m1 i j)
                                         1.0d0))
                             (setq ret nil))
                           (when (not (= (gsl-array:get-element m1 i j)
                                         (aref +array-single-float+ i j)))
                             (setq ret nil))))))))))

(deftest "set-row-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'integer
                                        :initial-contents +array-integer+)
               (gsl-array:with-vector (v1 5 :element-type 'integer
                                          :initial-element 1)
                 (gsl-array:set-row m1 2 v1)
                 (let ((ret t))
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (if (= i 2)
                           (when (not (= (gsl-array:get-element m1 i j)
                                         1.0d0))
                             (setq ret nil))
                           (when (not (= (gsl-array:get-element m1 i j)
                                         (aref +array-integer+ i j)))
                             (setq ret nil))))))))))

(deftest "set-row-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type '(complex (double-float))
                     :initial-contents +array-complex-double-float+)
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (double-float))
                       :initial-element (complex 1.0d0 1.0d0))
                 (gsl-array:set-row m1 2 v1)
                 (let ((ret t))
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (if (= i 2)
                           (unless (= (gsl-array:get-element m1 i j)
                                      (complex 1.0d0 1.0d0))
                             (setq ret nil))
                           (unless (= (gsl-array:get-element m1 i j)
                                      (aref +array-complex-double-float+ i j))
                             (setq ret nil))))))))))

(deftest "set-row-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type '(complex (single-float))
                     :initial-contents +array-complex-single-float+)
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (single-float))
                       :initial-element (complex 1.0 1.0))
                 (gsl-array:set-row m1 2 v1)
                 (let ((ret t))
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (if (= i 2)
                           (unless (= (gsl-array:get-element m1 i j)
                                      (complex 1.0 1.0))
                             (setq ret nil))
                           (unless (= (gsl-array:get-element m1 i j)
                                      (aref +array-complex-single-float+ i j))
                             (setq ret nil))))))))))

;; ----------------------------------------------------------------------

(deftest "set-col-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'double-float
                                        :initial-contents +array-double-float+)
               (gsl-array:with-vector (v1 5 :element-type 'double-float
                                          :initial-element 1.0d0)
                 (gsl-array:set-col m1 2 v1)
                 (let ((ret t))
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (if (= j 2)
                           (when (not (= (gsl-array:get-element m1 i j)
                                         1.0d0))
                             (setq ret nil))
                           (when (not (= (gsl-array:get-element m1 i j)
                                         (aref +array-double-float+ i j)))
                             (setq ret nil))))))))))

(deftest "set-col-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'single-float
                                        :initial-contents +array-single-float+)
               (gsl-array:with-vector (v1 5 :element-type 'single-float
                                          :initial-element 1.0)
                 (gsl-array:set-col m1 2 v1)
                 (let ((ret t))
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (if (= j 2)
                           (when (not (= (gsl-array:get-element m1 i j)
                                         1.0d0))
                             (setq ret nil))
                           (when (not (= (gsl-array:get-element m1 i j)
                                         (aref +array-single-float+ i j)))
                             (setq ret nil))))))))))

(deftest "set-col-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'integer
                                        :initial-contents +array-integer+)
               (gsl-array:with-vector (v1 5 :element-type 'integer
                                          :initial-element 1)
                 (gsl-array:set-col m1 2 v1)
                 (let ((ret t))
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (if (= j 2)
                           (when (not (= (gsl-array:get-element m1 i j)
                                         1.0d0))
                             (setq ret nil))
                           (when (not (= (gsl-array:get-element m1 i j)
                                         (aref +array-integer+ i j)))
                             (setq ret nil))))))))))

(deftest "set-col-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type '(complex (double-float))
                     :initial-contents +array-complex-double-float+)
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (double-float))
                       :initial-element (complex 1.0d0 1.0d0))
                 (gsl-array:set-col m1 2 v1)
                 (let ((ret t))
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (if (= j 2)
                           (unless (= (gsl-array:get-element m1 i j)
                                      (complex 1.0d0 1.0d0))
                             (setq ret nil))
                           (unless (= (gsl-array:get-element m1 i j)
                                      (aref +array-complex-double-float+ i j))
                             (setq ret nil))))))))))

(deftest "set-col-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type '(complex (single-float))
                     :initial-contents +array-complex-single-float+)
               (gsl-array:with-vector
                   (v1 5 :element-type '(complex (single-float))
                       :initial-element (complex 1.0 1.0))
                 (gsl-array:set-col m1 2 v1)
                 (let ((ret t))
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (if (= j 2)
                           (unless (= (gsl-array:get-element m1 i j)
                                      (complex 1.0 1.0))
                             (setq ret nil))
                           (unless (= (gsl-array:get-element m1 i j)
                                      (aref +array-complex-single-float+ i j))
                             (setq ret nil))))))))))

;; ----------------------------------------------------------------------

(deftest "swap-cols-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'double-float
                                        :initial-contents +array-double-float+)
               (gsl-array:swap-cols m1 1 3)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (cond
                       ((= j 1)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-double-float+ i 3))
                          (setq ret nil)))
                       ((= j 3)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-double-float+ i 1))
                          (setq ret nil)))
                       (t
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-double-float+ i j))
                          (setq ret nil))))))))))

(deftest "swap-cols-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'single-float
                                        :initial-contents +array-single-float+)
               (gsl-array:swap-cols m1 1 3)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (cond
                       ((= j 1)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-single-float+ i 3))
                          (setq ret nil)))
                       ((= j 3)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-single-float+ i 1))
                          (setq ret nil)))
                       (t
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-single-float+ i j))
                          (setq ret nil))))))))))

(deftest "swap-cols-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'integer
                                        :initial-contents +array-integer+)
               (gsl-array:swap-cols m1 1 3)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (cond
                       ((= j 1)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-integer+ i 3))
                          (setq ret nil)))
                       ((= j 3)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-integer+ i 1))
                          (setq ret nil)))
                       (t
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-integer+ i j))
                          (setq ret nil))))))))))

(deftest "swap-cols-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type '(complex (double-float))
                     :initial-contents +array-complex-double-float+)
               (gsl-array:swap-cols m1 1 3)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (cond
                       ((= j 1)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-complex-double-float+ i 3))
                          (setq ret nil)))
                       ((= j 3)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-complex-double-float+ i 1))
                          (setq ret nil)))
                       (t
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-complex-double-float+ i j))
                          (setq ret nil))))))))))

(deftest "swap-cols-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type '(complex (single-float))
                     :initial-contents +array-complex-single-float+)
               (gsl-array:swap-cols m1 1 3)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (cond
                       ((= j 1)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-complex-single-float+ i 3))
                          (setq ret nil)))
                       ((= j 3)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-complex-single-float+ i 1))
                          (setq ret nil)))
                       (t
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-complex-single-float+ i j))
                          (setq ret nil))))))))))

;; ----------------------------------------------------------------------

(deftest "swap-rows-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'double-float
                                        :initial-contents +array-double-float+)
               (gsl-array:swap-rows m1 1 3)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (cond
                       ((= i 1)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-double-float+ 3 j))
                          (setq ret nil)))
                       ((= i 3)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-double-float+ 1 j))
                          (setq ret nil)))
                       (t
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-double-float+ i j))
                          (setq ret nil))))))))))

(deftest "swap-rows-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'single-float
                                        :initial-contents +array-single-float+)
               (gsl-array:swap-rows m1 1 3)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (cond
                       ((= i 1)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-single-float+ 3 j))
                          (setq ret nil)))
                       ((= i 3)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-single-float+ 1 j))
                          (setq ret nil)))
                       (t
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-single-float+ i j))
                          (setq ret nil))))))))))

(deftest "swap-rows-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix (m1 5 5 :element-type 'integer
                                        :initial-contents +array-integer+)
               (gsl-array:swap-rows m1 1 3)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (cond
                       ((= i 1)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-integer+ 3 j))
                          (setq ret nil)))
                       ((= i 3)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-integer+ 1 j))
                          (setq ret nil)))
                       (t
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-integer+ i j))
                          (setq ret nil))))))))))

(deftest "swap-rows-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type '(complex (double-float))
                     :initial-contents +array-complex-double-float+)
               (gsl-array:swap-rows m1 1 3)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (cond
                       ((= i 1)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-complex-double-float+ 3 j))
                          (setq ret nil)))
                       ((= i 3)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-complex-double-float+ 1 j))
                          (setq ret nil)))
                       (t
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-complex-double-float+ i j))
                          (setq ret nil))))))))))

(deftest "swap-rows-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type '(complex (single-float))
                     :initial-contents +array-complex-single-float+)
               (gsl-array:swap-rows m1 1 3)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (cond
                       ((= i 1)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-complex-single-float+ 3 j))
                          (setq ret nil)))
                       ((= i 3)
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-complex-single-float+ 1 j))
                          (setq ret nil)))
                       (t
                        (unless (= (gsl-array:get-element m1 i j)
                                   (aref +array-complex-single-float+ i j))
                          (setq ret nil))))))))))

;; ----------------------------------------------------------------------

;;  TODO: find out what this function is supposed to do.
;;
;; (deftest "swap-rowcol-double-float" :category +matrix+
;;          :test-fn
;;          #'(lambda ()
;;              (gsl-array:with-matrix
;;                  (m1 5 5 :element-type 'double-float
;;                      :initial-contents +array-double-float+)
;;                (gsl-array:swap-rowcol m1 1 3)
;;                (let ((ret t))
;;                  (dotimes (i 5 ret)
;;                    (dotimes (j 5)
;;                      (if (or (= i 1) (= j 3))
;;                          (unless (= (gsl-array:get-element m1 i j)
;;                                    (aref +array-double-float+ j i))
;;                           (setq ret nil))
;;                          (unless (= (gsl-array:get-element m1 i j)
;;                                     (aref +array-double-float+ i j))
;;                            (setq ret nil)))))))))

;; ----------------------------------------------------------------------

(deftest "transpose-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type 'double-float
                     :initial-contents +array-double-float+)
               (gsl-array:transpose m1)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (unless (= (gsl-array:get-element m1 i j)
                                (aref +array-double-float+ j i))
                       (setq ret nil))))))))

(deftest "transpose-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type 'single-float
                     :initial-contents +array-single-float+)
               (gsl-array:transpose m1)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (unless (= (gsl-array:get-element m1 i j)
                                (aref +array-single-float+ j i))
                       (setq ret nil))))))))

(deftest "transpose-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type 'integer
                     :initial-contents +array-integer+)
               (gsl-array:transpose m1)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (unless (= (gsl-array:get-element m1 i j)
                                (aref +array-integer+ j i))
                       (setq ret nil))))))))

(deftest "transpose-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type '(complex (double-float))
                     :initial-contents +array-complex-double-float+)
               (gsl-array:transpose m1)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (unless (= (gsl-array:get-element m1 i j)
                                (aref +array-complex-double-float+ j i))
                       (setq ret nil))))))))

(deftest "transpose-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type '(complex (single-float))
                     :initial-contents +array-complex-single-float+)
               (gsl-array:transpose m1)
               (let ((ret t))
                 (dotimes (i 5 ret)
                   (dotimes (j 5)
                     (unless (= (gsl-array:get-element m1 i j)
                                (aref +array-complex-single-float+ j i))
                       (setq ret nil))))))))

;; ----------------------------------------------------------------------

(deftest "copy-transpose-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type 'double-float
                     :initial-contents +array-double-float+)
               (gsl-array:with-matrix (m2 5 5 :element-type 'double-float)
                 (gsl-array:copy-transpose m2 m1)
                 (let ((ret t))
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (unless (= (gsl-array:get-element m1 i j)
                                  (gsl-array:get-element m2 j i))
                         (setq ret nil)))))))))

(deftest "copy-transpose-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type 'single-float
                     :initial-contents +array-single-float+)
               (gsl-array:with-copy-transpose (m2 m1)
                 (let ((ret t))
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (unless (= (gsl-array:get-element m1 i j)
                                  (gsl-array:get-element m2 j i))
                         (setq ret nil)))))))))

(deftest "copy-transpose-integer" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type 'integer
                     :initial-contents +array-integer+)
               (gsl-array:with-copy-transpose (m2 m1)
                 (let ((ret t))
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (unless (= (gsl-array:get-element m1 i j)
                                  (gsl-array:get-element m2 j i))
                         (setq ret nil)))))))))

(deftest "copy-transpose-complex-double-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type '(complex (double-float))
                     :initial-contents +array-complex-double-float+)
               (gsl-array:with-copy-transpose (m2 m1)
                 (let ((ret t))
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (unless (= (gsl-array:get-element m1 i j)
                                  (gsl-array:get-element m2 j i))
                         (setq ret nil)))))))))

(deftest "copy-transpose-complex-single-float" :category +matrix+
         :test-fn
         #'(lambda ()
             (gsl-array:with-matrix
                 (m1 5 5 :element-type '(complex (single-float))
                     :initial-contents +array-complex-single-float+)
               (gsl-array:with-copy-transpose (m2 m1)
                 (let ((ret t))
                   (dotimes (i 5 ret)
                     (dotimes (j 5)
                       (unless (= (gsl-array:get-element m1 i j)
                                  (gsl-array:get-element m2 j i))
                         (setq ret nil)))))))))
