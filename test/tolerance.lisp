;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl-test)

(defconstant +dbl-epsilon+ 2.2204460492503131d-16)

(defconstant +tol0+  (/ +dbl-epsilon+ 2.0d0))
(defconstant +tol+1+  (* +tol0+ 10.0d0))
(defconstant +tol-1+  (/ +tol0+ 10.0d0))
(defconstant +tol+2+  (* +tol0+ 100.0d0))
(defconstant +tol-2+  (/ +tol0+ 100.0d0))
(defconstant +tol+3+  (* +tol0+ 1000.0d0))
(defconstant +tol-3+  (/ +tol0+ 1000.0d0))

(defun tol< (x y tol)
  (< (abs (- x y)) tol))
