;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl-array)


(defclass gsl-vector ()
  ((ptr :accessor ptr :initarg :ptr)
   (size :accessor size :initarg :size)
   (element-type :accessor element-type :initarg :element-type)))

;; TODO: have a (defmethod initialize-instance : after) that calls alloc?

(defclass gsl-vector-double-float (gsl-vector) ())
(defclass gsl-vector-single-float (gsl-vector) ())
(defclass gsl-vector-integer (gsl-vector) ())
(defclass gsl-vector-complex-double-float (gsl-vector) ())
(defclass gsl-vector-complex-single-float (gsl-vector) ())


(defmacro def-vector-type-funcs% (typ)
  (let ((type-ptr)
        (type-val)
        (type-val-ptr)
        (type-string)
        (is-real (or (eq typ 'double-float)
                     (eq typ 'single-float)
                     (eq typ 'integer))))
    (cond
      ((eq typ 'double-float)
       (setq type-ptr 'gsl-vector-ptr)
       (setq type-val :double)
       (setq type-val-ptr '(* :double))
       (setq type-string "vector"))
      ((eq typ 'single-float)
       (setq type-ptr 'gsl-vector-float-ptr)
       (setq type-val :float)
       (setq type-val-ptr '(* :float))
       (setq type-string "vector_float"))
      ((eq typ 'integer)
       (setq type-ptr 'gsl-vector-int-ptr)
       (setq type-val :int)
       (setq type-val-ptr '(* :int))
       (setq type-string "vector_int"))
      ((equal typ '(complex (double-float)))
       (setq type-ptr 'gsl-vector-complex-ptr)
       (setq type-val 'gsl-complex)
       (setq type-val-ptr '(* gsl-complex))
       (setq type-string "vector_complex"))
      ((equal typ '(complex (single-float)))
       (setq type-ptr 'gsl-vector-complex-float-ptr)
       (setq type-val 'gsl-complex-float)
       (setq type-val-ptr '(* gsl-complex-float))
       (setq type-string "vector_complex_float"))
      (t
       (error "no matching type.")))

    `(progn
       (defun-foreign ,(concatenate 'string "gsl_" type-string "_alloc")
           ((size size-t))
         ,type-ptr)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_free")
           ((v ,type-ptr))
         :void)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_get")
           ((v ,type-ptr)
            (i size-t))
         ,type-val)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_set")
           ((v ,type-ptr)
            (i size-t)
            (x ,type-val))
         :void)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_set_all")
           ((v ,type-ptr)
            (x ,type-val))
         :void)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_set_zero")
           ((v ,type-ptr))
         :void)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_set_basis")
           ((v ,type-ptr)
            (i size-t))
         :void)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_memcpy")
           ((v1 ,type-ptr)
            (v2 ,type-ptr))
         :int)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_swap")
           ((v1 ,type-ptr)
            (v2 ,type-ptr))
         :int)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_swap_elements")
           ((v1 ,type-ptr)
            (i size-t)
            (j size-t))
         :int)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_reverse")
           ((v1 ,type-ptr))
         :int)

       (defun-foreign ,(concatenate 'string "gsl_" type-string "_isnull")
           ((vec ,type-ptr))
         :int)

       (defun-foreign ,(concatenate 'string "wrap_gsl_" type-string "_fwrite")
           ((fn :cstring)
            (v ,type-ptr))
         :int)

       (defun-foreign ,(concatenate 'string "wrap_gsl_" type-string "_fread")
           ((fn :cstring)
            (v ,type-ptr))
         :int)

       (defun-foreign ,(concatenate 'string "wrap_gsl_" type-string "_fprintf")
           ((fn :cstring)
            (v ,type-ptr))
         :int)

       (defun-foreign ,(concatenate 'string "wrap_gsl_" type-string "_fscanf")
           ((fn :cstring)
            (v ,type-ptr))
         :int)

       (defun-foreign ,(concatenate 'string "wrap_gsl_" type-string
                                    "_subvector")
           ((v ,type-ptr)
            (offset size-t)
            (n size-t))
         ,type-ptr)

       (defun-foreign ,(concatenate 'string "wrap_gsl_" type-string
                                    "_subvector_with_stride")
           ((v ,type-ptr)
            (offset size-t)
            (stride size-t)
            (n size-t))
         ,type-ptr)

       ,(when is-real
          `(progn
             (defun-foreign ,(concatenate 'string "gsl_" type-string "_add")
                 ((va ,type-ptr)
                  (vb ,type-ptr))
               :int)

             (defun-foreign ,(concatenate 'string "gsl_" type-string "_sub")
                 ((va ,type-ptr)
                  (vb ,type-ptr))
               :int)

             (defun-foreign ,(concatenate 'string "gsl_" type-string "_mul")
                 ((va ,type-ptr)
                  (vb ,type-ptr))
               :int)

             (defun-foreign ,(concatenate 'string "gsl_" type-string "_div")
                 ((va ,type-ptr)
                  (vb ,type-ptr))
               :int)

             (defun-foreign ,(concatenate 'string "gsl_" type-string "_scale")
                 ((vec ,type-ptr)
                  ;; seems odd that this is :double for all types
                  (x :double))
               :int)

             (defun-foreign ,(concatenate 'string
                                          "gsl_" type-string "_add_constant")
                 ((vec ,type-ptr)
                  ;; and again, :double for all types
                  (x :double))
               :int)

             (defun-foreign ,(concatenate 'string "gsl_" type-string "_max")
                 ((vec ,type-ptr))
               ,type-val)

             (defun-foreign ,(concatenate 'string "gsl_" type-string "_min")
                 ((vec ,type-ptr))
               ,type-val)

             (defun-foreign ,(concatenate 'string "gsl_" type-string "_minmax")
                 ((vec ,type-ptr)
                  (min ,type-val-ptr)
                  (max ,type-val-ptr))
               :void)

             (defun-foreign ,(concatenate 'string
                                          "gsl_" type-string "_max_index")
                 ((vec ,type-ptr))
               size-t)

             (defun-foreign ,(concatenate 'string
                                          "gsl_" type-string "_min_index")
                 ((vec ,type-ptr))
               size-t)

             (defun-foreign ,(concatenate 'string
                                          "gsl_" type-string "_minmax_index")
                 ((vec ,type-ptr)
                  (min size-t-ptr)
                  (max size-t-ptr))
               :void)
             ))

       ,(when (not is-real)
          `(progn
             (defun-foreign ,(concatenate 'string "gsl_" type-string "_ptr")
                 ((v ,type-ptr)
                  (i size-t))
               (* ,type-val))

             (defun-foreign ,(concatenate 'string "wrap_gsl_" type-string "_set")
                 ((v ,type-ptr)
                  (i size-t)
                  (z (* ,type-val)))
               :void)

             (defun-foreign ,(concatenate 'string
                                          "wrap_gsl_" type-string "_set_all")
                 ((v ,type-ptr)
                  (z (* ,type-val)))
               :void)))
       )))


(def-vector-type-funcs% double-float)
(def-vector-type-funcs% single-float)
(def-vector-type-funcs% integer)
(def-vector-type-funcs% (complex (double-float)))
(def-vector-type-funcs% (complex (single-float)))


(defmacro def-vector-methods% (class-string func-string)
  (let ((class-object (kmrcl:concat-symbol "gsl-vector-" class-string))
        (is-real (or (string= class-string "integer")
                     (string= class-string "single-float")
                     (string= class-string "double-float"))))
    `(progn

       (defmethod alloc ((o ,class-object))
         (setf (ptr o) (,(kmrcl:concat-symbol "gsl-vector-" func-string "alloc")
                        (size o)))
         o)

       (defmethod free ((o ,class-object))
         (,(kmrcl:concat-symbol "gsl-vector-" func-string "free") (ptr o))
         (setf (ptr o) nil)
         (setf (size o) nil)
         (setf (element-type o) nil))


       (defmethod get-element ((o ,class-object) i &optional j)
         (assert (and (typep i 'integer) (>= i 0) (< i (size o))))
         ,(if is-real
              `(,(kmrcl:concat-symbol "gsl-vector-" func-string "get")
                 (ptr o) i)
              `(,(kmrcl:concat-symbol "gsl-" func-string ">complex")
                 (,(kmrcl:concat-symbol "gsl-vector-" func-string "ptr")
                   (ptr o) i))))

       (defmethod set-element ((o ,class-object) i &optional x dummy)
         (assert (typep x (element-type o)))
         (assert (and (typep i 'integer) (>= i 0) (< i (size o))))
         ,(if is-real
              `(,(kmrcl:concat-symbol "gsl-vector-" func-string "set")
                 (ptr o) i x)
              `(,(kmrcl:concat-symbol "with-" class-string "->gsl-" func-string
                                      "ptr") (c-ptr x)
                 (,(kmrcl:concat-symbol "wrap-gsl-vector-" func-string "set")
                   (ptr o) i c-ptr)))
         x)

       (defmethod set-all ((o ,class-object) x)
         (assert (typep x (element-type o)))
         ,(if is-real
              `(,(kmrcl:concat-symbol "gsl-vector-" func-string "set-all")
                 (ptr o) x)
              `(,(kmrcl:concat-symbol "with-" class-string "->gsl-" func-string
                                      "ptr") (c-ptr x)
                 (,(kmrcl:concat-symbol "wrap-gsl-vector-" func-string "set-all")
                   (ptr o) c-ptr)))
         o)

       (defmethod set-zero ((o ,class-object))
         (,(kmrcl:concat-symbol "gsl-vector-" func-string "set-zero") (ptr o))
         o)


       (defmethod set-basis ((o ,class-object) i)
         (assert (typep i 'integer))
         (assert (and (>= i 0) (< i (size o))))
         (,(kmrcl:concat-symbol "gsl-vector-" func-string "set-basis")
           (ptr o) i)
         o)


       (defmethod read-from-binary-file ((o ,class-object) file-name)
         (let ((status))
           (uffi:with-cstring (c-file-name file-name)
             (setq status
                   (,(kmrcl:concat-symbol "wrap-gsl-vector-" func-string
                                          "fread") c-file-name (ptr o))))
           (values o status)))

       (defmethod read-from-file ((o ,class-object) file-name)
         (let ((status))
           (uffi:with-cstring (c-file-name file-name)
             (setq status
                   (,(kmrcl:concat-symbol "wrap-gsl-vector-" func-string
                                          "fscanf") c-file-name (ptr o))))
           (values o status)))

       (defmethod write-to-binary-file (file-name (o ,class-object))
         (let ((status))
           ;; TODO: check if uffi:with-string returns a result, docs unclear.
           (uffi:with-cstring (c-file-name file-name)
             (setq status
                   (,(kmrcl:concat-symbol "wrap-gsl-vector-" func-string
                                          "fwrite") c-file-name (ptr o))))
           status))

       (defmethod write-to-file (file-name (o ,class-object))
         (let ((status))
           (uffi:with-cstring (c-file-name file-name)
             (setq status
                   (,(kmrcl:concat-symbol "wrap-gsl-vector-" func-string
                                          "fprintf") c-file-name (ptr o))))
           status))

       (defmethod swap ((o1 ,class-object) (o2 ,class-object))
         (assert (= (size o1) (size o2)))
         (let ((status (,(kmrcl:concat-symbol "gsl-vector-" func-string
                                              "swap") (ptr o1) (ptr o2))))
           (values o1 status)))

       (defmethod swap-elements ((o ,class-object) i j)
         (assert (and (typep i 'integer) (>= i 0) (< i (size o))))
         (assert (and (typep j 'integer) (>= j 0) (< j (size o))))
         (let ((status (,(kmrcl:concat-symbol "gsl-vector-" func-string
                                              "swap-elements") (ptr o) i j)))
           (values o status)))

       (defmethod reverse-vector ((o ,class-object))
         (let ((status (,(kmrcl:concat-symbol "gsl-vector-" func-string
                                              "reverse") (ptr o))))
           (values o status)))


       (defmethod isnull ((o ,class-object))
         (1/0->t/nil (,(kmrcl:concat-symbol "gsl-vector-" func-string
                                            "isnull") (ptr o))))

       ,(when is-real
          `(progn
             (defmethod add ((o1 ,class-object) (o2 ,class-object))
               (assert (= (size o1) (size o2)))
               (let ((status (,(kmrcl:concat-symbol "gsl-vector-" func-string
                                                    "add") (ptr o1) (ptr o2))))
                 (values o1 status)))

             (defmethod sub ((o1 ,class-object) (o2 ,class-object))
               (assert (= (size o1) (size o2)))
               (let ((status (,(kmrcl:concat-symbol "gsl-vector-" func-string
                                                    "sub") (ptr o1) (ptr o2))))
                 (values o1 status)))

             (defmethod mul ((o1 ,class-object) (o2 ,class-object))
               (assert (= (size o1) (size o2)))
               (let ((status (,(kmrcl:concat-symbol "gsl-vector-" func-string
                                                    "mul") (ptr o1) (ptr o2))))
                 (values o1 status)))

             (defmethod div ((o1 ,class-object) (o2 ,class-object))
               (assert (= (size o1) (size o2)))
               (let ((status (,(kmrcl:concat-symbol "gsl-vector-" func-string
                                                    "div") (ptr o1) (ptr o2))))
                 (values o1 status)))

             (defmethod scale ((o ,class-object) x)
               (assert (typep x (element-type o)))
               ;; coerce to double-float looks wrong, but isn't.
               (,(kmrcl:concat-symbol "gsl-vector-" func-string "scale")
                 (ptr o) (coerce x 'double-float)))

             (defmethod add-constant ((o ,class-object) x)
               (assert (typep x (element-type o)))
               ;; coerce to double-float looks wrong, but isn't.
               (,(kmrcl:concat-symbol "gsl-vector-" func-string "add-constant")
                 (ptr o) (coerce x 'double-float)))

             (defmethod max-value ((o ,class-object))
               (,(kmrcl:concat-symbol "gsl-vector-" func-string "max") (ptr o)))

             (defmethod min-value ((o ,class-object))
               (,(kmrcl:concat-symbol "gsl-vector-" func-string "min") (ptr o)))

             (defmethod max-index ((o ,class-object))
               (,(kmrcl:concat-symbol "gsl-vector-" func-string "max-index")
                 (ptr o)))

             (defmethod min-index ((o ,class-object))
               (,(kmrcl:concat-symbol "gsl-vector-" func-string "min-index")
                 (ptr o)))

             (defmethod min-max-indicies ((o ,class-object))
               (let ((min-ptr (uffi:allocate-foreign-object 'size-t))
                     (max-ptr (uffi:allocate-foreign-object 'size-t)))
                 (,(kmrcl:concat-symbol "gsl-vector-" func-string
                                        "minmax-index")
                   (ptr o) min-ptr max-ptr)
                 (prog1
                     (list (uffi:deref-pointer min-ptr 'size-t)
                           (uffi:deref-pointer max-ptr 'size-t))
                   (uffi:free-foreign-object min-ptr)
                   (uffi:free-foreign-object max-ptr))))

             (defmethod min-max-values ((o ,class-object))
               (destructuring-bind (min-index max-index)
                   (min-max-indicies o)
                 (list (get-element o min-index)
                       (get-element o max-index))))

             )))))


(def-vector-methods% "integer" "int-")
(def-vector-methods% "single-float" "float-")
(def-vector-methods% "double-float" "")
(def-vector-methods% "complex-single-float" "complex-float-")
(def-vector-methods% "complex-double-float" "complex-")


(defun make-vector (size &key (element-type 'double-float) initial-element
                    initial-contents from-file from-binary-file)
  (assert (and (typep size 'integer) (> size 0) ))
  (assert (find element-type '(integer single-float double-float
                               (complex (single-float))
                               (complex (double-float))) :test #'equal))
  (let ((v (cond
             ((eq element-type 'integer)
              (make-instance 'gsl-vector-integer
                             :size size :element-type element-type))
             ((eq element-type 'double-float)
              (make-instance 'gsl-vector-double-float
                             :size size :element-type element-type))
             ((eq element-type 'single-float)
              (make-instance 'gsl-vector-single-float
                             :size size :element-type element-type))
             ((equal element-type '(complex (double-float)))
              (make-instance 'gsl-vector-complex-double-float
                             :size size :element-type element-type))
             ((equal element-type '(complex (single-float)))
              (make-instance 'gsl-vector-complex-single-float
                             :size size :element-type element-type))
             (t
              (error "should never get here.")))))
    (alloc v)
    (cond
      ((and initial-element initial-contents from-file from-binary-file)
       (error "can only define one of the keys: initial-element, initial-contents, from-file, from-binary-file."))
      (initial-element
       (set-all v initial-element))
      (initial-contents
       (cond
         ((listp initial-contents)
          (do ((x initial-contents (cdr x))
               (i 0 (1+ i)))
              ((= i size))
            (set-element v i (car x))))
         ((vectorp initial-contents)
          (do ((i 0 (1+ i)))
              ((= i size))
            (set-element v i (aref initial-contents i))))
         (t
          (error "initial-contents must be either a list or a vector."))))
      (from-file
       (read-from-file v from-file))
      (from-binary-file
       (read-from-binary-file v from-binary-file)))
    v))


(defmacro with-vector
    ((vec size &key element-type initial-element initial-contents from-file
          from-binary-file) &body body)
  `(let ((,vec (make-vector ,size
                            :element-type (or ,element-type 'double-float)
                            :initial-element ,initial-element
                            :initial-contents ,initial-contents
                            :from-file ,from-file
                            :from-binary-file ,from-binary-file)))
     (unwind-protect
          (progn ,@body)
       (free ,vec))))


(defmacro def-vector-copy-method% (class-string func-string)
  (let ((class-object (kmrcl:concat-symbol "gsl-vector-" class-string)))
    `(defmethod copy ((o ,class-object))
       (let* ((o-copy (make-vector (size o) :element-type (element-type o)))
              (status (,(kmrcl:concat-symbol "gsl-vector-" func-string
                                             "memcpy") (ptr o-copy) (ptr o))))
         (values o-copy status)))))

(def-vector-copy-method% "integer" "int-")
(def-vector-copy-method% "single-float" "float-")
(def-vector-copy-method% "double-float" "")
(def-vector-copy-method% "complex-single-float" "complex-float-")
(def-vector-copy-method% "complex-double-float" "complex-")


(defmacro with-vector-copy ((vec-dest vec-src) &body body)
  `(let ((,vec-dest (copy ,vec-src)))
     (unwind-protect
          ,@body
       (free ,vec-dest))))


(defun gsl->lisp-vector (v)
  (let ((a (make-array (size v) :element-type (element-type v))))
    (dotimes (i (size v) a)
      (setf (aref a i) (get-element v i)))))

;; Function: gsl_vector_view gsl_vector_complex_real (gsl_vector_complex *v)
;; Function: gsl_vector_view gsl_vector_complex_imag (gsl_vector_complex *v)

