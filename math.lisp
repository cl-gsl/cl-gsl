;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl-math)


(register-constants ((+e+        2.71828182845904523536028747135d0)
                     (+log2e+    1.44269504088896340735992468100d0)
                     (+log10e+   0.43429448190325182765112891892d0)
                     (+sqrt2+    1.41421356237309504880168872421d0)
                     (+sqrt1/2+  0.70710678118654752440084436210d0)
                     (+sqrt3+    1.73205080756887729352744634151d0)
                     (+pi+       3.14159265358979323846264338328d0)
                     (+pi/2+     1.57079632679489661923132169164d0)
                     (+pi/4+     0.78539816339744830966156608458d0)
                     (+sqrtpi+   1.77245385090551602729816748334d0)
                     (+2/sqrtpi+ 1.12837916709551257389615890312d0)
                     (+1/pi+     0.31830988618379067153776752675d0)
                     (+2/pi+     0.63661977236758134307553505349d0)
                     (+ln10+     2.30258509299404568401799145468d0)
                     (+ln2+      0.69314718055994530941723212146d0)
                     (+lnpi+     1.14472988584940017414342735135d0)
                     (+euler+    0.57721566490153286060651209008d0)))
