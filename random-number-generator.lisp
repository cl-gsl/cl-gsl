;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl-rng)

(defstruct rng
  ptr)

;; ----------------------------------------------------------------------

(defun-foreign "wrap_gsl_rng_set_type"
    ((tn :cstring))
  gsl-rng-ptr)

(defun make-generator (name)
  "Returns a newly created instance of a random number generator. NAME is
a string specifying the random number generator algorithm. Instance must
be destroyed with a call to FREE-GENERATOR."
  (uffi:with-cstring (c-name name)
    (make-rng :ptr (wrap-gsl-rng-set-type c-name))))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_rng_free"
    ((r gsl-rng-ptr))
  :void)

(defun free-generator (generator)
  "Destroys the random number generator instance GENERATOR."
  (assert (typep generator 'rng))
  (gsl-rng-free (rng-ptr generator))
  (setf (rng-ptr generator) nil))

;; ----------------------------------------------------------------------

(defmacro with-generator ((generator name) &body body)
  "Creates a new random number generator of type specified by the string
NAME, and binds it to GENERATOR. The generator is destroyed when the form is
exited."
  `(let ((,generator (make-generator ,name)))
     (unwind-protect
          (progn ,@body)
       (free-generator ,generator))))

;; ----------------------------------------------------------------------
(defun-foreign "gsl_rng_set"
    ((r gsl-rng-ptr)
     (s size-t))
  :void)

(defun seed (generator num)
  "Initialize (or seed) the random number generator GENERATOR with the
integer NUM. Returns GENERATOR."
  (assert (typep generator 'rng))
  (assert (typep num 'integer))
  (gsl-rng-set (rng-ptr generator) num)
  generator)

;; ----------------------------------------------------------------------

(defun-foreign "gsl_rng_get"
    ((r gsl-rng-ptr))
  size-t)

(defun get-integer (generator)
  "Returns an integer unifromly distributed in the range [min,max].
The minimum and maximum values of the range depend upon the GENERATOR used."
  (assert (typep generator 'rng))
  (gsl-rng-get (rng-ptr generator)))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_rng_uniform"
    ((r gsl-rng-ptr))
  :double)

(defun get-double-float (generator)
  "Returns a double-float uniformly distributed in the range [0,1].
The range includes 0.0 but excludes 1.0."
  (assert (typep generator 'rng))
  (gsl-rng-uniform (rng-ptr generator)))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_rng_uniform_pos"
    ((r gsl-rng-ptr))
  :double)

(defun get-double-float-pos (generator)
  "Returns a positive double-float uniformly distributed in the range [0,1].
The range excludes both 0.0 and 1.0."
  (assert (typep generator 'rng))
  (gsl-rng-uniform-pos (rng-ptr generator)))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_rng_uniform_int"
    ((r gsl-rng-ptr)
     (n size-t))
  size-t)

(defun get-integer-in-range (generator num)
  "Returns an integer unifromly distributed in the range [0,(1- NUM)].
The minimum and maximum values of the range depend upon the GENERATOR used."
  (assert (typep generator 'rng))
  (assert (typep num 'integer))
  (gsl-rng-uniform-int (rng-ptr generator) num))


;; ----------------------------------------------------------------------

(defun-foreign "gsl_rng_name"
    ((r gsl-rng-ptr))
  :cstring)

(defun generator-name (generator)
  "Returns a string giving the name of the random number generator GENERATOR."
  (assert (typep generator 'rng))
  (uffi:convert-from-cstring (gsl-rng-name (rng-ptr generator))))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_rng_max"
    ((r gsl-rng-ptr))
  size-t)

(defun generator-max (generator)
  "Returns the largest value that (GET-INTEGER GENERATOR) can return."
  (assert (typep generator 'rng))
  (gsl-rng-max (rng-ptr generator)))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_rng_min"
    ((r gsl-rng-ptr))
  size-t)

(defun generator-min (generator)
  "Returns the smallest value that (GET-INTEGER GENERATOR) can return."
  (assert (typep generator 'rng))
  (gsl-rng-min (rng-ptr generator)))

;; ----------------------------------------------------------------------

(defun-foreign "gsl_rng_clone"
    ((r gsl-rng-ptr))
  gsl-rng-ptr)

(defun clone (generator)
  "Returns a newly created copy of the random number generator GENERATOR."
  (assert (typep generator 'rng))
  (make-rng :ptr (gsl-rng-clone (rng-ptr generator))))

(defmacro with-clone ((generator-clone generator) &body body)
  "Create a newly created random number generator which is an extact copy of
GENERATOR, and bind it to GENERATOR-CLONE. The generator is destroyed when
the form is exited."
  `(let ((,generator-clone (clone ,generator)))
     (unwind-protect
          (progn ,@body)
       (free-generator ,generator-clone))))

;; ----------------------------------------------------------------------

(defun-foreign "wrap_gsl_rng_fwrite"
    ((fn :cstring)
     (r gsl-rng-ptr))
  :int)

(defun write-state-to-file (file-name generator)
  "Writes the random number state of the random number generator GENERATOR
to the file given by the string FILE-NAME. Returns 0 for success and
+EFAILED+ for failure."
  (assert (typep generator 'rng))
  (let ((status))
    (uffi:with-cstring (c-file-name file-name)
      (setq status (wrap-gsl-rng-fwrite c-file-name (rng-ptr generator))))
    status))

;; ----------------------------------------------------------------------

(defun-foreign "wrap_gsl_rng_fread"
    ((fn :cstring)
     (r gsl-rng-ptr))
  :int)

(defun read-state-from-file (file-name generator)
  "Reads the random number state of the random number generator GENERATOR
from the file given by the string FILE-NAME. Return 0 for success and
+EFAILED+ for failure. GENERATOR must be created with the correct random
number generator."
  (assert (typep generator 'rng))
  (let ((status))
    (uffi:with-cstring (c-file-name file-name)
      (setq status (wrap-gsl-rng-fread c-file-name (rng-ptr generator))))
    status))
