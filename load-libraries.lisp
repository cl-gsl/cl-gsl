;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl)

(defun load-library (library-name)
  (if (uffi:load-foreign-library
       (uffi:find-foreign-library
        library-name
        '("/usr/local/lib/" "/usr/lib/" "/lib/" "/usr/local/lib/cl-gsl/")
        :types '("so" "a"))
       :module library-name
       :supporting-libraries '("c"))
      t
      (progn
        (warn (concatenate 'string "Unable to load: " library-name))
        nil)))

(defun gsl-config-lib-string ()
  (multiple-value-bind (output error-output error-status)
      (kmrcl:command-output "gsl-config --libs")
    (declare (ignore error-output))
    (if (not (= error-status 0))
        (error "Could not run 'gsl-config --libs'")
        output)))

(defun get-libs-list ()
  (cons
   "gsl_cwrapper"
   (remove-if #'(lambda (elm) (not (cl-ppcre:scan "^lib" elm)))
              (mapcar #'(lambda (elm) (cl-ppcre:regex-replace "^-l" elm "lib"))
                      (butlast
                       (cl-ppcre:split "[ ]+" (gsl-config-lib-string)))))))

(defun load-the-libraries ()
  (mapcar #'load-library (reverse (get-libs-list))))

(load-the-libraries)
