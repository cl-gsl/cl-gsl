;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-gsl-sf)

(def-foreign-struct gsl-sf-result
    (val :double)
    (err :double))

(def-foreign-struct gsl-sf-result-e10
    (val :double)
    (err :double)
    (e10 :int))

(define-foreign-type gsl-sf-result-ptr (* gsl-sf-result))
(define-foreign-type gsl-sf-result-e10-ptr (* gsl-sf-result-e10))

(defmacro defun-wrapper% (str+symb params+types ret-type)
  (let ((c-func-str+symb)
        (c-func-symb)
        (lisp-func-symb)
        (let-params)
        (c-func-params)
        (lisp-func-params)
        (cleanup)
        (declare-params)
        (ret))

    (if (consp str+symb)
        (progn
          (setq c-func-symb (kmrcl:concat-symbol "gsl-sf-" (cadr str+symb)))
          (setq c-func-str+symb
                (list (concatenate 'string "gsl_sf_" (car str+symb))
                      c-func-symb))
          (setq lisp-func-symb (cadr str+symb)))
        (progn
          (setq c-func-symb (kmrcl:concat-symbol
                             "gsl-sf-" (string_->string- str+symb)))
          (setq c-func-str+symb (concatenate 'string "gsl_sf_" str+symb))
          (setq lisp-func-symb (kmrcl:concat-symbol
                                (string_->string- str+symb)))))

    (dolist (elm params+types)
      (let ((p (car elm))
            (p-type (cadr elm)))
        (cond
          ((eq p-type 'gsl-sf-result-ptr)
           (let ((val (gensym)))
             (push `(,val (uffi:allocate-foreign-object 'gsl-sf-result))
                   let-params)
             (push val c-func-params)
             (push `(uffi:get-slot-value ,val :double 'val) ret)
             (push `(uffi:get-slot-value ,val :double 'err) ret)
             (push `(uffi:free-foreign-object ,val) cleanup)))
          ((eq p-type 'gsl-sf-result-e10-ptr)
           (let ((val (gensym)))
             (push `(,val (uffi:allocate-foreign-object 'gsl-sf-result-e10))
                   let-params)
             (push val c-func-params)
             (push `(uffi:get-slot-value ,val :double 'val) ret)
             (push `(uffi:get-slot-value ,val :double 'err) ret)
             (push `(uffi:get-slot-value ,val :int 'e10) ret)
             (push `(uffi:free-foreign-object ,val) cleanup)))
          (t
           (push p lisp-func-params)
           (push (ecase p-type
                   (:double `(double-float ,p))
                   (:int `(integer ,p))
                   (:unsigned-int `(unsigned-byte ,p))
                   (gsl-mode-t `(unsigned-byte ,p)))
                 declare-params)
           (push p c-func-params)))))

    (push '(ret-val) let-params)

    ;; no need to reverse cleanup or let-params
    (setq c-func-params (nreverse c-func-params))
    (setq lisp-func-params (nreverse lisp-func-params))
    (setq ret (nreverse ret))

    `(progn
       (defun-foreign ,c-func-str+symb
           ,params+types
         ,ret-type)

       (defun ,lisp-func-symb ,lisp-func-params
         (declare ,@declare-params)
         ,(if ret
              `(let ,let-params
                 (setq ret-val (,c-func-symb ,@c-func-params))
                 (multiple-value-prog1
                     (values ,@ret ret-val)
                   ,@cleanup))
              `(,c-func-symb ,@c-func-params)))

       (export '(,lisp-func-symb)))))


;;; Airy Functions

(defun-wrapper% "airy_Ai"
    ((x :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "airy_Ai_e"
    ((x :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "airy_Bi"
    ((x :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "airy_Bi_e"
    ((x :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "airy_Ai_scaled"
    ((x :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "airy_Ai_scaled_e"
    ((x :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "airy_Bi_scaled"
    ((x :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "airy_Bi_scaled_e"
    ((x :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "airy_Ai_deriv"
    ((x :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "airy_Ai_deriv_e"
    ((x :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "airy_Bi_deriv"
    ((x :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "airy_Bi_deriv_e"
    ((x :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "airy_Ai_deriv_scaled"
    ((x :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "airy_Ai_deriv_scaled_e"
    ((x :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "airy_Bi_deriv_scaled"
    ((x :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "airy_Bi_deriv_scaled_e"
    ((x :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "airy_zero_Ai"
    ((x :unsigned-int))
  :double)

(defun-wrapper% "airy_zero_Ai_e"
    ((x :unsigned-int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "airy_zero_Bi"
    ((x :unsigned-int))
  :double)

(defun-wrapper% "airy_zero_Bi_e"
    ((x :unsigned-int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "airy_zero_Ai_deriv"
    ((x :unsigned-int))
  :double)

(defun-wrapper% "airy_zero_Ai_deriv_e"
    ((x :unsigned-int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "airy_zero_Bi_deriv"
    ((x :unsigned-int))
  :double)

(defun-wrapper% "airy_zero_Bi_deriv_e"
    ((x :unsigned-int)
     (result gsl-sf-result-ptr))
  :int)

;;; Bessel Functions

(defun-wrapper% ("bessel_J0" bessel-c-j0)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_J0_e" bessel-c-j0-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_J1" bessel-c-j1)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_J1_e" bessel-c-j1-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_Jn" bessel-c-jn)
    ((n :int)
     (x :double))
  :double)

(defun-wrapper% ("bessel_Jn_e" bessel-c-jn-e)
    ((n :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

;; ----------------------------------------------------------------------

(defun-foreign "gsl_sf_bessel_Jn_array"
    ((nmin :int)
     (nmax :int)
     (x :double)
     (result double-ptr))
  :int)

(defun bessel-c-jn-array (nmin nmax x)
  (declare (integer nmin) (integer nmax) (double-float x))
  (assert (> nmax nmin))
  (let* ((array-ptr (uffi:allocate-foreign-object :double (1+ (- nmax nmin))))
         (status (gsl-sf-bessel-jn-array nmin nmax x array-ptr)))
    (multiple-value-prog1
        (values (c-array->lisp-vec array-ptr (1+ (- nmax nmin))) status)
      (uffi:free-foreign-object array-ptr))))

;; ----------------------------------------------------------------------

(defun-wrapper% ("bessel_Y0" bessel-c-y0)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_Y0_e" bessel-c-y0-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_Y1" bessel-c-y1)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_Y1_e" bessel-c-y1-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_Yn" bessel-c-yn)
    ((n :int)
     (x :double))
  :double)

(defun-wrapper% ("bessel_Yn_e" bessel-c-yn-e)
    ((n :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

;; ----------------------------------------------------------------------

(defun-foreign "gsl_sf_bessel_Yn_array"
    ((nmin :int)
     (nmax :int)
     (x :double)
     (result double-ptr))
  :int)

(defun bessel-c-yn-array (nmin nmax x)
  (declare (integer nmin) (integer nmax) (double-float x))
  (assert (> nmax nmin))
  (let* ((array-ptr (uffi:allocate-foreign-object :double (1+ (- nmax nmin))))
         (status (gsl-sf-bessel-yn-array nmin nmax x array-ptr)))
    (multiple-value-prog1
        (values (c-array->lisp-vec array-ptr (1+ (- nmax nmin))) status)
      (uffi:free-foreign-object array-ptr))))

;; ----------------------------------------------------------------------

(defun-wrapper% ("bessel_I0" bessel-c-i0)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_I0_e" bessel-c-i0-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_I1" bessel-c-i1)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_I1_e" bessel-c-i1-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_In" bessel-c-in)
    ((n :int)
     (x :double))
  :double)

(defun-wrapper% ("bessel_In_e" bessel-c-in-e)
    ((n :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

;; ----------------------------------------------------------------------

(defun-foreign "gsl_sf_bessel_In_array"
    ((nmin :int)
     (nmax :int)
     (x :double)
     (result double-ptr))
  :int)

(defun bessel-c-in-array (nmin nmax x)
  (declare (integer nmin) (integer nmax) (double-float x))
  (assert (> nmax nmin))
  (let* ((array-ptr (uffi:allocate-foreign-object :double (1+ (- nmax nmin))))
         (status (gsl-sf-bessel-in-array nmin nmax x array-ptr)))
    (multiple-value-prog1
        (values (c-array->lisp-vec array-ptr (1+ (- nmax nmin))) status)
      (uffi:free-foreign-object array-ptr))))

;; ----------------------------------------------------------------------

(defun-wrapper% ("bessel_I0_scaled" bessel-c-i0-scaled)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_I0_scaled_e" bessel-c-i0-scaled-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_I1_scaled" bessel-c-i1-scaled)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_I1_scaled_e" bessel-c-i1-scaled-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_In_scaled" bessel-c-in-scaled)
    ((n :int)
     (x :double))
  :double)

(defun-wrapper% ("bessel_In_scaled_e" bessel-c-in-scaled-e)
    ((n :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)


;; ----------------------------------------------------------------------

(defun-foreign "gsl_sf_bessel_In_scaled_array"
    ((nmin :int)
     (nmax :int)
     (x :double)
     (result double-ptr))
  :int)

(defun bessel-c-in-scaled-array (nmin nmax x)
  (declare (integer nmin) (integer nmax) (double-float x))
  (assert (> nmax nmin))
  (let* ((array-ptr (uffi:allocate-foreign-object :double (1+ (- nmax nmin))))
         (status (gsl-sf-bessel-in-scaled-array nmin nmax x array-ptr)))
    (multiple-value-prog1
        (values (c-array->lisp-vec array-ptr (1+ (- nmax nmin))) status)
      (uffi:free-foreign-object array-ptr))))

;; ----------------------------------------------------------------------

(defun-wrapper% ("bessel_K0" bessel-c-k0)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_K0_e" bessel-c-k0-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_K1" bessel-c-k1)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_K1_e" bessel-c-k1-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_Kn" bessel-c-kn)
    ((n :int)
     (x :double))
  :double)

(defun-wrapper% ("bessel_Kn_e" bessel-c-kn-e)
    ((n :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

;; ----------------------------------------------------------------------

(defun-foreign "gsl_sf_bessel_Kn_array"
    ((nmin :int)
     (nmax :int)
     (x :double)
     (result double-ptr))
  :int)

(defun bessel-c-kn-array (nmin nmax x)
  (declare (integer nmin) (integer nmax) (double-float x))
  (assert (> nmax nmin))
  (let* ((array-ptr (uffi:allocate-foreign-object :double (1+ (- nmax nmin))))
         (status (gsl-sf-bessel-kn-array nmin nmax x array-ptr)))
    (multiple-value-prog1
        (values (c-array->lisp-vec array-ptr (1+ (- nmax nmin))) status)
      (uffi:free-foreign-object array-ptr))))

;; ----------------------------------------------------------------------

(defun-wrapper% ("bessel_K0_scaled" bessel-c-k0-scaled)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_K0_scaled_e" bessel-c-k0-scaled-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_K1_scaled" bessel-c-k1-scaled)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_K1_scaled_e" bessel-c-k1-scaled-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_Kn_scaled" bessel-c-kn-scaled)
    ((n :int)
     (x :double))
  :double)

(defun-wrapper% ("bessel_Kn_scaled_e" bessel-c-kn-scaled-e)
    ((n :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

;; ----------------------------------------------------------------------

(defun-foreign "gsl_sf_bessel_Kn_scaled_array"
    ((nmin :int)
     (nmax :int)
     (x :double)
     (result double-ptr))
  :int)

(defun bessel-c-kn-scaled-array (nmin nmax x)
  (declare (integer nmin) (integer nmax) (double-float x))
  (assert (> nmax nmin))
  (let* ((array-ptr (uffi:allocate-foreign-object :double (1+ (- nmax nmin))))
         (status (gsl-sf-bessel-kn-scaled-array nmin nmax x array-ptr)))
    (multiple-value-prog1
        (values (c-array->lisp-vec array-ptr (1+ (- nmax nmin))) status)
      (uffi:free-foreign-object array-ptr))))

;; ----------------------------------------------------------------------

(defun-wrapper% ("bessel_j0" bessel-s-j0)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_j0_e" bessel-s-j0-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_j1" bessel-s-j1)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_j1_e" bessel-s-j1-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_j2" bessel-s-j2)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_j2_e" bessel-s-j2-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_jl" bessel-s-jl)
    ((l :int)
     (x :double))
  :double)

(defun-wrapper% ("bessel_jl_e" bessel-s-jl-e)
    ((l :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

;; ----------------------------------------------------------------------

(defun-foreign "gsl_sf_bessel_jl_array"
    ((lmax :int)
     (x :double)
     (result double-ptr))
  :int)

(defun bessel-s-jl-array (lmax x)
  (declare (integer lmax) (double-float x))
  (assert (>= lmax 0))
  (let* ((array-ptr (uffi:allocate-foreign-object :double (1+ lmax)))
         (status (gsl-sf-bessel-jl-array lmax x array-ptr)))
    (multiple-value-prog1
        (values (c-array->lisp-vec array-ptr (1+ lmax)) status)
      (uffi:free-foreign-object array-ptr))))

;; ----------------------------------------------------------------------



;; Function: int gsl_sf_bessel_jl_steed_array (int lmax, double x, double * jl_x_array)

(defun-wrapper% ("bessel_y0" bessel-s-y0)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_y0_e" bessel-s-y0-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_y1" bessel-s-y1)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_y1_e" bessel-s-y1-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_y2" bessel-s-y2)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_y2_e" bessel-s-y2-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_yl" bessel-s-yl)
    ((l :int)
     (x :double))
  :double)

(defun-wrapper% ("bessel_yl_e" bessel-s-yl-e)
    ((l :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

;; ----------------------------------------------------------------------

(defun-foreign "gsl_sf_bessel_yl_array"
    ((lmax :int)
     (x :double)
     (result double-ptr))
  :int)

(defun bessel-s-yl-array (lmax x)
  (declare (integer lmax) (double-float x))
  (assert (>= lmax 0))
  (let* ((array-ptr (uffi:allocate-foreign-object :double (1+ lmax)))
         (status (gsl-sf-bessel-yl-array lmax x array-ptr)))
    (multiple-value-prog1
        (values (c-array->lisp-vec array-ptr (1+ lmax)) status)
      (uffi:free-foreign-object array-ptr))))

;; ----------------------------------------------------------------------

(defun-wrapper% ("bessel_i0_scaled" bessel-s-i0-scaled)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_i0_scaled_e" bessel-s-i0-scaled-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_i1_scaled" bessel-s-i1-scaled)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_i1_scaled_e" bessel-s-i1-scaled-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_i2_scaled" bessel-s-i2-scaled)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_i2_scaled_e" bessel-s-i2-scaled-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_il_scaled" bessel-s-il-scaled)
    ((l :int)
     (x :double))
  :double)

(defun-wrapper% ("bessel_il_scaled_e" bessel-s-il-scaled-e)
    ((l :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

;; ----------------------------------------------------------------------

(defun-foreign "gsl_sf_bessel_il_scaled_array"
    ((lmax :int)
     (x :double)
     (result double-ptr))
  :int)

(defun bessel-s-il-scaled-array (lmax x)
  (declare (integer lmax) (double-float x))
  (assert (>= lmax 0))
  (let* ((array-ptr (uffi:allocate-foreign-object :double (1+ lmax)))
         (status (gsl-sf-bessel-il-scaled-array lmax x array-ptr)))
    (multiple-value-prog1
        (values (c-array->lisp-vec array-ptr (1+ lmax)) status)
      (uffi:free-foreign-object array-ptr))))

;; ----------------------------------------------------------------------

(defun-wrapper% ("bessel_k0_scaled" bessel-s-k0-scaled)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_k0_scaled_e" bessel-s-k0-scaled-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_k1_scaled" bessel-s-k1-scaled)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_k1_scaled_e" bessel-s-k1-scaled-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_k2_scaled" bessel-s-k2-scaled)
    ((x :double))
  :double)

(defun-wrapper% ("bessel_k2_scaled_e" bessel-s-k2-scaled-e)
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_kl_scaled" bessel-s-kl-scaled)
    ((l :int)
     (x :double))
  :double)

(defun-wrapper% ("bessel_kl_scaled_e" bessel-s-kl-scaled-e)
    ((l :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

;; ----------------------------------------------------------------------

(defun-foreign "gsl_sf_bessel_kl_scaled_array"
    ((lmax :int)
     (x :double)
     (result double-ptr))
  :int)

(defun bessel-s-kl-scaled-array (lmax x)
  (declare (integer lmax) (double-float x))
  (assert (>= lmax 0))
  (let* ((array-ptr (uffi:allocate-foreign-object :double (1+ lmax)))
         (status (gsl-sf-bessel-kl-scaled-array lmax x array-ptr)))
    (multiple-value-prog1
        (values (c-array->lisp-vec array-ptr (1+ lmax)) status)
      (uffi:free-foreign-object array-ptr))))

;; ----------------------------------------------------------------------

(defun-wrapper% ("bessel_Jnu" bessel-c-jnu)
    ((nu :double)
     (x :double))
  :double)

(defun-wrapper% ("bessel_Jnu_e" bessel-c-jnu-e)
    ((nu :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

;; Function: int gsl_sf_bessel_sequence_Jnu_e (double nu, gsl_mode_t mode, size_t size, double v[])

(defun-wrapper% ("bessel_Ynu" bessel-c-ynu)
    ((nu :double)
     (x :double))
  :double)

(defun-wrapper% ("bessel_Ynu_e" bessel-c-ynu-e)
    ((nu :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_Inu" bessel-c-inu)
    ((nu :double)
     (x :double))
  :double)

(defun-wrapper% ("bessel_Inu_e" bessel-c-inu-e)
    ((nu :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_Inu_scaled" bessel-c-inu-scaled)
    ((nu :double)
     (x :double))
  :double)

(defun-wrapper% ("bessel_Inu_scaled_e" bessel-c-inu-scaled-e)
    ((nu :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_Knu" bessel-c-knu)
    ((nu :double)
     (x :double))
  :double)

(defun-wrapper% ("bessel_Knu_e" bessel-c-knu-e)
    ((nu :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_lnKnu" bessel-c-lnknu)
    ((nu :double)
     (x :double))
  :double)

(defun-wrapper% ("bessel_lnKnu_e" bessel-c-lnknu-e)
    ((nu :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_Knu_scaled" bessel-c-knu-scaled)
    ((nu :double)
     (x :double))
  :double)

(defun-wrapper% ("bessel_Knu_scaled_e" bessel-c-knu-scaled-e)
    ((nu :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_zero_J0" bessel-c-zero-j0)
    ((s :unsigned-int))
  :double)

(defun-wrapper% ("bessel_zero_J0_e" bessel-c-zero-j0-e)
    ((s :unsigned-int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_zero_J1" bessel-c-zero-j1)
    ((s :unsigned-int))
  :double)

(defun-wrapper% ("bessel_zero_J1_e" bessel-c-zero-j1-e)
    ((s :unsigned-int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("bessel_zero_Jnu" bessel-c-zero-jnu)
    ((nu :double)
     (s :unsigned-int))
  :double)

(defun-wrapper% ("bessel_zero_Jnu_e" bessel-c-zero-jnu-e)
    ((nu :double)
     (s :unsigned-int)
     (result gsl-sf-result-ptr))
  :int)

;;; Clausen Functions

(defun-wrapper% "clausen"
    ((x :double))
  :double)

(defun-wrapper% "clausen_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

;;; Coulomb Functions

(defun-wrapper% ("hydrogenicR_1" hydrogenic-r-1)
    ((z :double)
     (r :double))
  :double)

(defun-wrapper% ("hydrogenicR_1_e" hydrogenic-r-1-e)
    ((z :double)
     (r :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% ("hydrogenicR" hydrogenic-r)
    ((n :int)
     (l :int)
     (z :double)
     (r :double))
  :double)

(defun-wrapper% ("hydrogenicR_e" hydrogenic-r-e)
    ((n :int)
     (l :int)
     (z :double)
     (r :double)
     (result gsl-sf-result-ptr))
  :int)

;; (defun-wrapper% "coulomb_wave_FG_e"
;;     ((eta :double)
;;      (x :double)
;;      (l-f :double)
;;      (k :int)
;;      (f gsl-sf-result-ptr)
;;      (fp gsl-sf-result-ptr)
;;      (g gsl-sf-result-ptr)
;;      (gp gsl-sf-result-ptr)
;;      (exp-f double-ptr)
;;      (exp-g double-ptr))
;;   :int)

;; Function: int gsl_sf_coulomb_wave_F_array (double L_min, int kmax, double eta, double x, double fc_array[], double * F_exponent)

;; Function: int gsl_sf_coulomb_wave_FG_array (double L_min, int kmax, double eta, double x, double fc_array[], double gc_array[], double * F_exponent, double *

;; Function: int gsl_sf_coulomb_wave_FGp_array (double L_min, int kmax, double eta, double x, double fc_array[], double fcp_array[], double gc_array[], double

;; Function: int gsl_sf_coulomb_wave_sphF_array (double L_min, int kmax, double eta, double x, double fc_array[], double F_exponent[])

(defun-wrapper% "coulomb_CL_e"
    ((l :double)
     (eta :double)
     (result gsl-sf-result-ptr))
  :int)

;; Function: int gsl_sf_coulomb_CL_array (double Lmin, int kmax, double eta, double cl[])

;;; Coupling Coefficients

(defun-wrapper% "coupling_3j"
    ((two-ja :int)
     (two-jb :int)
     (two-jc :int)
     (two-ma :int)
     (two-mb :int)
     (two-mc :int))
  :double)

(defun-wrapper% "coupling_3j_e"
    ((two-ja :int)
     (two-jb :int)
     (two-jc :int)
     (two-ma :int)
     (two-mb :int)
     (two-mc :int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "coupling_6j"
    ((two-ja :int)
     (two-jb :int)
     (two-jc :int)
     (two-jd :int)
     (two-je :int)
     (two-jf :int))
  :double)

(defun-wrapper% "coupling_6j_e"
    ((two-ja :int)
     (two-jb :int)
     (two-jc :int)
     (two-jd :int)
     (two-je :int)
     (two-jf :int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "coupling_9j"
    ((two-ja :int)
     (two-jb :int)
     (two-jc :int)
     (two-jd :int)
     (two-je :int)
     (two-jf :int)
     (two-jg :int)
     (two-jh :int)
     (two-ji :int))
  :double)

(defun-wrapper% "coupling_9j_e"
    ((two-ja :int)
     (two-jb :int)
     (two-jc :int)
     (two-jd :int)
     (two-je :int)
     (two-jf :int)
     (two-jg :int)
     (two-jh :int)
     (two-ji :int)
     (result gsl-sf-result-ptr))
  :int)

;;; Dawson Function

(defun-wrapper% "dawson"
    ((x :double))
  :double)

(defun-wrapper% "dawson_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

;;; Debye Function

(defun-wrapper% "debye_1"
    ((x :double))
  :double)

(defun-wrapper% "debye_1_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "debye_2"
    ((x :double))
  :double)

(defun-wrapper% "debye_2_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "debye_3"
    ((x :double))
  :double)

(defun-wrapper% "debye_3_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "debye_4"
    ((x :double))
  :double)

(defun-wrapper% "debye_4_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "dilog"
    ((x :double))
  :double)

(defun-wrapper% "dilog_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)


;; Function: int gsl_sf_complex_dilog_e (double r, double theta, gsl_sf_result * result_re, gsl_sf_result * result_im)

(defun-wrapper% "multiply_e"
    ((x :double)
     (y :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "multiply_err_e"
    ((x :double)
     (dx :double)
     (y :double)
     (dy :double)
     (result gsl-sf-result-ptr))
  :int)

;;; Elliptic Integrals

(defun-wrapper% "ellint_Kcomp"
    ((k :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "ellint_Kcomp_e"
    ((k :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "ellint_Ecomp"
    ((k :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "ellint_Ecomp_e"
    ((k :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "ellint_F"
    ((phi :double)
     (k :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "ellint_F_e"
    ((phi :double)
     (k :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "ellint_E"
    ((phi :double)
     (k :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "ellint_E_e"
    ((phi :double)
     (k :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "ellint_P"
    ((phi :double)
     (k :double)
     (n :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "ellint_P_e"
    ((phi :double)
     (k :double)
     (n :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "ellint_D"
    ((phi :double)
     (k :double)
     (n :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "ellint_D_e"
    ((phi :double)
     (k :double)
     (n :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "ellint_RC"
    ((x :double)
     (y :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "ellint_RC_e"
    ((x :double)
     (y :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "ellint_RD"
    ((x :double)
     (y :double)
     (z :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "ellint_RD_e"
    ((x :double)
     (y :double)
     (z :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "ellint_RF"
    ((x :double)
     (y :double)
     (z :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "ellint_RF_e"
    ((x :double)
     (y :double)
     (z :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "ellint_RJ"
    ((x :double)
     (y :double)
     (z :double)
     (p :double)
     (mode gsl-mode-t))
  :double)

(defun-wrapper% "ellint_RJ_e"
    ((x :double)
     (y :double)
     (z :double)
     (p :double)
     (mode gsl-mode-t)
     (result gsl-sf-result-ptr))
  :int)

;; Function: int gsl_sf_elljac_e (double u, double m, double * sn, double * cn, double * dn)

(defun-wrapper% "erf"
    ((x :double))
  :double)

(defun-wrapper% "erf_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "erfc"
    ((x :double))
  :double)

(defun-wrapper% "erfc_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "log_erfc"
    ((x :double))
  :double)

(defun-wrapper% "log_erfc_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "erf_Z"
    ((x :double))
  :double)

(defun-wrapper% "erf_Z_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "erf_Q"
    ((x :double))
  :double)

(defun-wrapper% "erf_Q_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "hazard"
    ((x :double))
  :double)

(defun-wrapper% "hazard_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)


;; Function: double gsl_sf_exp (double x)

(defun-wrapper% "exp_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

;; Function: int gsl_sf_exp_e10_e (double x, gsl_sf_result_e10 * result)

(defun-wrapper% "exp_mult"
    ((x :double)
     (y :double))
  :double)

(defun-wrapper% "exp_mult_e"
    ((x :double)
     (y :double)
     (result gsl-sf-result-ptr))
  :int)

;; Function: int gsl_sf_exp_mult_e10_e (const double x, const double y, gsl_sf_result_e10 * result)

(defun-wrapper% "expm1"
    ((x :double))
  :double)

(defun-wrapper% "expm1_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "exprel"
    ((x :double))
  :double)

(defun-wrapper% "exprel_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "exprel_2"
    ((x :double))
  :double)

(defun-wrapper% "exprel_2_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "exprel_n"
    ((n :int)
     (x :double))
  :double)

(defun-wrapper% "exprel_n_e"
    ((n :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "exp_err_e"
    ((x :double)
     (dx :double)
     (result gsl-sf-result-ptr))
  :int)


;; Function: int gsl_sf_exp_err_e10_e (double x, double dx, gsl_sf_result_e10 * result)

(defun-wrapper% "exp_mult_err_e"
    ((x :double)
     (dx :double)
     (y :double)
     (dy :double)
     (result gsl-sf-result-ptr))
  :int)

;; Function: int gsl_sf_exp_mult_err_e10_e (double x, double dx, double y, double dy, gsl_sf_result_e10 * result)

(defun-wrapper% "expint_E1"
    ((x :double))
  :double)

(defun-wrapper% "expint_E1_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "expint_E2"
    ((x :double))
  :double)

(defun-wrapper% "expint_E2_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "expint_Ei"
    ((x :double))
  :double)

(defun-wrapper% "expint_Ei_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "Shi"
    ((x :double))
  :double)

(defun-wrapper% "Shi_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "Chi"
    ((x :double))
  :double)

(defun-wrapper% "Chi_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "expint_3"
    ((x :double))
  :double)

(defun-wrapper% "expint_3_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "Si"
    ((x :double))
  :double)

(defun-wrapper% "Si_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "Ci"
    ((x :double))
  :double)

(defun-wrapper% "Ci_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "atanint"
    ((x :double))
  :double)

(defun-wrapper% "atanint_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

;;; Fermi Dirac Functions

(defun-wrapper% "fermi_dirac_m1"
    ((x :double))
  :double)

(defun-wrapper% "fermi_dirac_m1_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "fermi_dirac_0"
    ((x :double))
  :double)

(defun-wrapper% "fermi_dirac_0_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "fermi_dirac_1"
    ((x :double))
  :double)

(defun-wrapper% "fermi_dirac_1_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "fermi_dirac_2"
    ((x :double))
  :double)

(defun-wrapper% "fermi_dirac_2_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "fermi_dirac_int"
    ((j :int)
     (x :double))
  :double)

(defun-wrapper% "fermi_dirac_int_e"
    ((j :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "fermi_dirac_mhalf"
    ((x :double))
  :double)

(defun-wrapper% "fermi_dirac_mhalf_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "fermi_dirac_half"
    ((x :double))
  :double)

(defun-wrapper% "fermi_dirac_half_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "fermi_dirac_3half"
    ((x :double))
  :double)

(defun-wrapper% "fermi_dirac_3half_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "fermi_dirac_inc_0"
    ((x :double)
     (b :double))
  :double)

(defun-wrapper% "fermi_dirac_inc_0_e"
    ((x :double)
     (b :double)
     (result gsl-sf-result-ptr))
  :int)

;;; Gamma Function

(defun-wrapper% "gamma"
    ((x :double))
  :double)

(defun-wrapper% "gamma_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "lngamma"
    ((x :double))
  :double)

(defun-wrapper% "lngamma_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

;; Function: int gsl_sf_lngamma_sgn_e (double x, gsl_sf_result * result_lg, double * sgn)

(defun-wrapper% "gammastar"
    ((x :double))
  :double)

(defun-wrapper% "gammastar_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "gammainv"
    ((x :double))
  :double)

(defun-wrapper% "gammainv_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)


;; Function: int gsl_sf_lngamma_complex_e (double zr, double zi, gsl_sf_result * lnr, gsl_sf_result * arg)


(defun-wrapper% "taylorcoeff"
    ((n :int)
     (x :double))
  :double)

(defun-wrapper% "taylorcoeff_e"
    ((n :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "fact"
    ((n :unsigned-int))
  :double)

(defun-wrapper% "fact_e"
    ((n :unsigned-int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "doublefact"
    ((n :unsigned-int))
  :double)

(defun-wrapper% "doublefact_e"
    ((n :unsigned-int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "lnfact"
    ((n :unsigned-int))
  :double)

(defun-wrapper% "lnfact_e"
    ((n :unsigned-int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "lndoublefact"
    ((n :unsigned-int))
  :double)

(defun-wrapper% "lndoublefact_e"
    ((n :unsigned-int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "choose"
    ((n :unsigned-int)
     (m :unsigned-int))
  :double)

(defun-wrapper% "choose_e"
    ((n :unsigned-int)
     (m :unsigned-int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "lnchoose"
    ((n :unsigned-int)
     (m :unsigned-int))
  :double)

(defun-wrapper% "lnchoose_e"
    ((n :unsigned-int)
     (m :unsigned-int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "poch"
    ((a :double)
     (x :double))
  :double)

(defun-wrapper% "poch_e"
    ((a :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "lnpoch"
    ((a :double)
     (x :double))
  :double)

(defun-wrapper% "lnpoch_e"
    ((a :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

;; Function: int gsl_sf_lnpoch_sgn_e (double a, double x, gsl_sf_result * result, double * sgn)

(defun-wrapper% "pochrel"
    ((a :double)
     (x :double))
  :double)

(defun-wrapper% "pochrel_e"
    ((a :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "gamma_inc_Q"
    ((a :double)
     (x :double))
  :double)

(defun-wrapper% "gamma_inc_Q_e"
    ((a :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "gamma_inc_P"
    ((a :double)
     (x :double))
  :double)

(defun-wrapper% "gamma_inc_P_e"
    ((a :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "gamma_inc"
    ((a :double)
     (x :double))
  :double)

(defun-wrapper% "gamma_inc_e"
    ((a :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "beta"
    ((a :double)
     (b :double))
  :double)

(defun-wrapper% "beta_e"
    ((a :double)
     (b :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "lnbeta"
    ((a :double)
     (b :double))
  :double)

(defun-wrapper% "lnbeta_e"
    ((a :double)
     (b :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "beta_inc"
    ((a :double)
     (b :double)
     (x :double))
  :double)

(defun-wrapper% "beta_inc_e"
    ((a :double)
     (b :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

;;; Gegenbauer Functions

(defun-wrapper% "gegenpoly_1"
    ((l :double)
     (x :double))
  :double)

(defun-wrapper% "gegenpoly_2"
    ((l :double)
     (x :double))
  :double)

(defun-wrapper% "gegenpoly_3"
    ((l :double)
     (x :double))
  :double)

(defun-wrapper% "gegenpoly_1_e"
    ((l :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "gegenpoly_2_e"
    ((l :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "gegenpoly_3_e"
    ((l :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "gegenpoly_n"
    ((n :int)
     (l :double)
     (x :double))
  :double)

(defun-wrapper% "gegenpoly_n_e"
    ((n :int)
     (l :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

;; ----------------------------------------------------------------------

(defun-foreign "gsl_sf_gegenpoly_array"
    ((nmax :int)
     (l :double)
     (x :double)
     (result double-ptr))
  :int)

(defun gegenpoly-array (nmax l x)
  (declare (integer nmax) (double-float l) (double-float x))
  (assert (and (>= nmax 0) (> l -0.5d0)))
  (let* ((array-ptr (uffi:allocate-foreign-object :double (1+ nmax)))
         (status (gsl-sf-gegenpoly-array nmax l x array-ptr)))
    (multiple-value-prog1
        (values (c-array->lisp-vec array-ptr (1+ nmax)) status)
      (uffi:free-foreign-object array-ptr))))

;; ----------------------------------------------------------------------

;;; Hypergeometric Functions

(defun-wrapper% "hyperg_0F1"
    ((c :double)
     (x :double))
  :double)

(defun-wrapper% "hyperg_0F1_e"
    ((c :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "hyperg_1F1_int"
    ((m :int)
     (n :int)
     (x :double))
  :double)

(defun-wrapper% "hyperg_1F1_int_e"
    ((m :int)
     (n :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "hyperg_1F1"
    ((a :double)
     (b :double)
     (x :double))
  :double)

(defun-wrapper% "hyperg_1F1_e"
    ((a :double)
     (b :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "hyperg_U_int"
    ((m :int)
     (n :int)
     (x :double))
  :double)

(defun-wrapper% "hyperg_U_int_e"
    ((m :int)
     (n :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "hyperg_U_int_e10_e"
    ((m :int)
     (n :int)
     (x :double)
     (result gsl-sf-result-e10-ptr))
  :int)

(defun-wrapper% "hyperg_U"
    ((a :double)
     (b :double)
     (x :double))
  :double)

(defun-wrapper% "hyperg_U_e"
    ((a :double)
     (b :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "hyperg_U_e10_e"
    ((a :double)
     (b :double)
     (x :double)
     (result gsl-sf-result-e10-ptr))
  :int)

(defun-wrapper% "hyperg_2F1"
    ((a :double)
     (b :double)
     (c :double)
     (x :double))
  :double)

(defun-wrapper% "hyperg_2F1_e"
    ((a :double)
     (b :double)
     (c :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "hyperg_2F1_conj"
    ((ar :double)
     (ai :double)
     (c :double)
     (x :double))
  :double)

(defun-wrapper% "hyperg_2F1_conj_e"
    ((ar :double)
     (ai :double)
     (c :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "hyperg_2F1_renorm"
    ((a :double)
     (b :double)
     (c :double)
     (x :double))
  :double)

(defun-wrapper% "hyperg_2F1_renorm_e"
    ((a :double)
     (b :double)
     (c :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "hyperg_2F1_conj_renorm"
    ((ar :double)
     (ai :double)
     (c :double)
     (x :double))
  :double)

(defun-wrapper% "hyperg_2F1_conj_renorm_e"
    ((ar :double)
     (ai :double)
     (c :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "hyperg_2F0"
    ((a :double)
     (b :double)
     (x :double))
  :double)

(defun-wrapper% "hyperg_2F0_e"
    ((a :double)
     (b :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

;;; Laguerre Functions

(defun-wrapper% "laguerre_1"
    ((a :double)
     (x :double))
  :double)

(defun-wrapper% "laguerre_2"
    ((a :double)
     (x :double))
  :double)

(defun-wrapper% "laguerre_3"
    ((a :double)
     (x :double))
  :double)

(defun-wrapper% "laguerre_1_e"
    ((a :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "laguerre_2_e"
    ((a :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "laguerre_3_e"
    ((a :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "laguerre_n"
    ((n :int)
     (a :double)
     (x :double))
  :double)

(defun-wrapper% "laguerre_n_e"
    ((n :int)
     (a :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

;;; Lambert W Functions

(defun-wrapper% "lambert_W0"
    ((x :double))
  :double)

(defun-wrapper% "lambert_W0_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "lambert_Wm1"
    ((x :double))
  :double)

(defun-wrapper% "lambert_Wm1_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

;;; Legendre Functions

(defun-wrapper% "legendre_P1"
    ((x :double))
  :double)

(defun-wrapper% "legendre_P2"
    ((x :double))
  :double)

(defun-wrapper% "legendre_P3"
    ((x :double))
  :double)

(defun-wrapper% "legendre_P1_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "legendre_P2_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "legendre_P3_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "legendre_Pl"
    ((l :int)
     (x :double))
  :double)

(defun-wrapper% "legendre_Pl_e"
    ((l :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

;; ----------------------------------------------------------------------

(defun-foreign "gsl_sf_legendre_Pl_array"
    ((lmax :int)
     (x :double)
     (result double-ptr))
  :int)

(defun legendre-pl-array (lmax x)
  (declare (integer lmax) (double-float x))
  (assert (>= lmax 0))
  (let* ((array-ptr (uffi:allocate-foreign-object :double (1+ lmax)))
         (status (gsl-sf-legendre-pl-array lmax x array-ptr)))
    (multiple-value-prog1
        (values (c-array->lisp-vec array-ptr (1+ lmax)) status)
      (uffi:free-foreign-object array-ptr))))

;; ----------------------------------------------------------------------

(defun-wrapper% "legendre_Q0"
    ((x :double))
  :double)

(defun-wrapper% "legendre_Q0_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "legendre_Q1"
    ((x :double))
  :double)

(defun-wrapper% "legendre_Q1_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "legendre_Ql"
    ((l :int)
     (x :double))
  :double)

(defun-wrapper% "legendre_Ql_e"
    ((l :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "legendre_Plm"
    ((l :int)
     (m :int)
     (x :double))
  :double)

(defun-wrapper% "legendre_Plm_e"
    ((l :int)
     (m :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)


;; Function: int gsl_sf_legendre_Plm_array (int lmax, int m, double x, double result_array[])

(defun-wrapper% "legendre_sphPlm"
    ((l :int)
     (m :int)
     (x :double))
  :double)

(defun-wrapper% "legendre_sphPlm_e"
    ((l :int)
     (m :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

;; Function: int gsl_sf_legendre_sphPlm_array (int lmax, int m, double x, double result_array[])

(defun-wrapper% "legendre_array_size"
    ((l :int)
     (m :int))
  :int)

(defun-wrapper% "conicalP_half"
    ((l :double)
     (x :double))
  :double)

(defun-wrapper% "conicalP_half_e"
    ((l :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "conicalP_mhalf"
    ((l :double)
     (x :double))
  :double)

(defun-wrapper% "conicalP_mhalf_e"
    ((l :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "conicalP_0"
    ((l :double)
     (x :double))
  :double)

(defun-wrapper% "conicalP_0_e"
    ((l :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "conicalP_1"
    ((l :double)
     (x :double))
  :double)

(defun-wrapper% "conicalP_1_e"
    ((l :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "conicalP_sph_reg"
    ((l :int)
     (lmbd :double)
     (x :double))
  :double)

(defun-wrapper% "conicalP_sph_reg_e"
    ((l :int)
     (lmbd :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "conicalP_cyl_reg"
    ((m :int)
     (lmbd :double)
     (x :double))
  :double)

(defun-wrapper% "conicalP_cyl_reg_e"
    ((m :int)
     (lmbd :double)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "legendre_H3d_0"
    ((l :double)
     (eta :double))
  :double)

(defun-wrapper% "legendre_H3d_0_e"
    ((l :double)
     (eta :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "legendre_H3d_1"
    ((l :double)
     (eta :double))
  :double)

(defun-wrapper% "legendre_H3d_1_e"
    ((l :double)
     (eta :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "legendre_H3d"
    ((l :int)
     (lmbd :double)
     (eta :double))
  :double)

(defun-wrapper% "legendre_H3d_e"
    ((l :int)
     (lmbd :double)
     (eta :double)
     (result gsl-sf-result-ptr))
  :int)

;; ----------------------------------------------------------------------

(defun-foreign "gsl_sf_legendre_H3d_array"
    ((lmax :int)
     (l :double)
     (eta :double)
     (result double-ptr))
  :int)

(defun legendre-h3d-array (lmax l eta)
  (declare (integer lmax) (double-float l) (double-float eta))
  (assert (>= lmax 0))
  (let* ((array-ptr (uffi:allocate-foreign-object :double (1+ lmax)))
         (status (gsl-sf-legendre-h3d-array lmax l eta array-ptr)))
    (multiple-value-prog1
        (values (c-array->lisp-vec array-ptr (1+ lmax)) status)
      (uffi:free-foreign-object array-ptr))))

;; ----------------------------------------------------------------------

;; Function: double gsl_sf_log (double x)

(defun-wrapper% "log_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "log_abs"
    ((x :double))
  :double)

(defun-wrapper% "log_abs_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

;; Function: int gsl_sf_complex_log_e (double zr, double zi, gsl_sf_result * lnr, gsl_sf_result * theta)

(defun-wrapper% "log_1plusx"
    ((x :double))
  :double)

(defun-wrapper% "log_1plusx_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "log_1plusx_mx"
    ((x :double))
  :double)

(defun-wrapper% "log_1plusx_mx_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "pow_int"
    ((x :double)
     (n :int))
  :double)

(defun-wrapper% "pow_int_e"
    ((x :double)
     (n :int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "psi_int"
    ((n :int))
  :double)

(defun-wrapper% "psi_int_e"
    ((n :int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "psi"
    ((x :double))
  :double)

(defun-wrapper% "psi_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "psi_1piy"
    ((y :double))
  :double)

(defun-wrapper% "psi_1piy_e"
    ((y :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "psi_1_int"
    ((n :int))
  :double)

(defun-wrapper% "psi_1_int_e"
    ((n :int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "psi_1"
    ((x :double))
  :double)

(defun-wrapper% "psi_1_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "psi_n"
    ((m :int)
     (x :double))
  :double)

(defun-wrapper% "psi_n_e"
    ((m :int)
     (x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "synchrotron_1"
    ((x :double))
  :double)

(defun-wrapper% "synchrotron_1_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "synchrotron_2"
    ((x :double))
  :double)

(defun-wrapper% "synchrotron_2_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "transport_2"
    ((x :double))
  :double)

(defun-wrapper% "transport_2_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "transport_3"
    ((x :double))
  :double)

(defun-wrapper% "transport_3_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "transport_4"
    ((x :double))
  :double)

(defun-wrapper% "transport_4_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "transport_5"
    ((x :double))
  :double)

(defun-wrapper% "transport_5_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

;; Function: double gsl_sf_sin (double x)

(defun-wrapper% "sin_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

;; Function: double gsl_sf_cos (double x)

(defun-wrapper% "cos_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "hypot"
    ((x :double)
     (y :double))
  :double)

(defun-wrapper% "hypot_e"
    ((x :double)
     (y :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "sinc"
    ((x :double))
  :double)

(defun-wrapper% "sinc_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

;; Function: int gsl_sf_complex_sin_e (double zr, double zi, gsl_sf_result * szr, gsl_sf_result * szi)

;; Function: int gsl_sf_complex_cos_e (double zr, double zi, gsl_sf_result * czr, gsl_sf_result * czi)

;; Function: int gsl_sf_complex_logsin_e (double zr, double zi, gsl_sf_result * lszr, gsl_sf_result * lszi)

(defun-wrapper% "lnsinh"
    ((x :double))
  :double)

(defun-wrapper% "lnsinh_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "lncosh"
    ((x :double))
  :double)

(defun-wrapper% "lncosh_e"
    ((x :double)
     (result gsl-sf-result-ptr))
  :int)

;; Function: int gsl_sf_polar_to_rect (double r, double theta, gsl_sf_result * x, gsl_sf_result * y);

;; Function: int gsl_sf_rect_to_polar (double x, double y, gsl_sf_result * r, gsl_sf_result * theta)

(defun-wrapper% "angle_restrict_symm"
    ((theta :double))
  :double)

(defun-wrapper% "angle_restrict_symm_e"
    ((theta :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "angle_restrict_pos"
    ((theta :double))
  :double)

(defun-wrapper% "angle_restrict_pos_e"
    ((theta :double)
     (result gsl-sf-result-ptr))
  :int)

;; (defun-wrapper% "sin_err"
;;     ((x :double)
;;      (dx :double))
;;   :double)

(defun-wrapper% "sin_err_e"
    ((x :double)
     (dx :double)
     (result gsl-sf-result-ptr))
  :int)

;; (defun-wrapper% "cos_err"
;;     ((x :double)
;;      (dx :double))
;;   :double)

(defun-wrapper% "cos_err_e"
    ((x :double)
     (dx :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "zeta_int"
    ((n :int))
  :double)

(defun-wrapper% "zeta_int_e"
    ((n :int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "zeta"
    ((s :double))
  :double)

(defun-wrapper% "zeta_e"
    ((s :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "zetam1_int"
    ((n :int))
  :double)

(defun-wrapper% "zetam1_int_e"
    ((n :int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "zetam1"
    ((s :double))
  :double)

(defun-wrapper% "zetam1_e"
    ((s :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "hzeta"
    ((s :double)
     (q :double))
  :double)

(defun-wrapper% "hzeta_e"
    ((s :double)
     (q :double)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "eta_int"
    ((n :int))
  :double)

(defun-wrapper% "eta_int_e"
    ((n :int)
     (result gsl-sf-result-ptr))
  :int)

(defun-wrapper% "eta"
    ((s :double))
  :double)

(defun-wrapper% "eta_e"
    ((s :double)
     (result gsl-sf-result-ptr))
  :int)
