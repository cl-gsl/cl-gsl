;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Copyright (C) 2005 Edgar Denny <edgardenny@comcast.net>
;;;; This file is part of CL-GSL.
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(in-package #:cl-user)

(defpackage #:cl-gsl
  (:nicknames #:gsl)
  (:use #:cl)
  (:export
   #:load-library
   #:gsl-config-lib-string
   #:get-libs-list
   #:load-the-libraries
   #:t/nil->1/0
   #:1/0->t/nil
   #:string_->string-
   #:+null-void-pointer+

   #:min-light-travel-from-earth
   #:defun-foreign
   #:define-foreign-type
   #:def-foreign-struct
   #:gsl-complex->complex
   #:gsl-complex-float->complex
   #:with-lisp-vec->c-array
   #:complex-packed-array->lisp-vec
   #:with-complex-double-float->gsl-complex-ptr
   #:with-complex-single-float->gsl-complex-float-ptr
   #:c-array->lisp-vec
   #:defconstant-export
   #:register-constants
   ))

(defpackage #:cl-gsl-const
  (:nicknames #:gsl-const)
  (:use #:cl #:cl-gsl)
  (:export
   ))

(defpackage #:cl-gsl-math
  (:nicknames #:gsl-math)
  (:use #:cl #:cl-gsl)
  (:export
   ))

(defpackage #:cl-gsl-poly
  (:nicknames #:gsl-poly)
  (:use #:cl #:cl-gsl)
  (:export
   #:poly-eval
   #:solve-quadratic
   #:solve-cubic
   #:complex-solve-quadratic
   #:complex-solve-cubic
   #:complex-solve
   #:dd-init
   #:dd-eval
   #:dd-taylor
   ))

(defpackage #:cl-gsl-sf
  (:nicknames #:gsl-sf)
  (:use #:cl #:cl-gsl)
  (:export
   #:bessel-c-jn-array
   #:bessel-c-yn-array
   #:bessel-c-in-array
   #:bessel-c-in-scaled-array
   #:bessel-c-kn-array
   #:bessel-c-kn-scaled-array
   #:bessel-s-jl-array
   #:bessel-s-yl-array
   #:bessel-s-il-scaled-array
   #:bessel-s-kl-scaled-array
   #:gegenpoly-array
   #:legendre-pl-array
   #:legendre-hd3-array
   ))

(defpackage #:cl-gsl-array
  (:nicknames #:gsl-array)
  (:use #:cl #:cl-gsl)
  (:export

   ;; from vector
   #:size
   #:element-type
   #:ptr

   #:free
   #:make-vector
   #:with-vector
   #:get-element
   #:set-element
   #:set-all
   #:set-zero
   #:set-basis
   #:write-to-binary-file
   #:write-to-file
   #:read-from-binary-file
   #:read-from-file
   #:subvector
   #:subvector-with-stride
   #:copy
   #:with-vector-copy
   #:swap
   #:swap-elements
   #:reverse-vector
   #:add
   #:sub
   #:mul
   #:div
   #:scale
   #:add-constant
   #:max-value
   #:min-value
   #:max-index
   #:min-index
   #:min-max-indicies
   #:min-max-values
   #:isnull
   #:gsl->lisp-vector

   ;; from matrix
   #:make-matrix
   #:size-rows
   #:size-cols
   #:set-identity
   #:mul-elements
   #:div-elements
   #:with-matrix
   #:with-matrix-copy
   #:gsl-matrix->lisp-array
   #:get-row
   #:with-matrix-row
   #:get-col
   #:with-matrix-col
   #:set-row
   #:set-col
   #:swap-rows
   #:swap-cols
   #:swap-rowcol
   #:transpose
   #:copy-transpose
   #:with-copy-transpose

   ;; from permutation
   #:permutation-init
   #:isvalid
   #:reverse-permutation
   #:next
   #:prev
   #:inverse
   #:with-permutation-inverse
   #:permute-vector
   #:permute-vector-inverse
   #:make-permutation
   #:with-permutation
   #:with-permutation-copy
   #:with-permutation-mul
   #:linear->canonical
   #:with-permutation-linear->canonical
   #:canonical->linear
   #:with-permutation-canonical->linear
   #:inversions
   #:linear-cycles
   #:canonical-cycles

   ;; from sort
   #:sort-vector
   #:sort-vector-index
   #:with-sort-vector-index
   ))

(defpackage #:cl-gsl-rng
  (:nicknames #:gsl-rng)
  (:use #:cl #:cl-gsl)
  (:export
   #:make-generator
   #:free-generator
   #:with-generator
   #:seed
   #:get-integer
   #:get-double-float
   #:get-double-float-pos
   #:get-integer-in-range
   #:generator-name
   #:generator-max
   #:generator-min
   #:clone
   #:with-clone
   #:write-state-to-file
   #:read-state-from-file
   ))
